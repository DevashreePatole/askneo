import React, { FC, useEffect, useState } from 'react';

import { RootNavigation } from './src/container/navigators/rootNavigation';

import SplashScreen from 'react-native-splash-screen';


const App: FC = () => {

  useEffect(()=>{SplashScreen.hide()},[])

  return (
   <RootNavigation />
  //<Dashboard/>
  );
};

export default App;
