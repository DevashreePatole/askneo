import ImgToBase64 from 'react-native-image-base64';
import Share from 'react-native-share';

export const convertToBase64 = (image) => {
    let base64Data = '';
    return new Promise((resolve, reject) => {
        ImgToBase64.getBase64String(`${image}`)
            .then(async (base64String) => {
                base64Data = 'data:image/jpeg;base64,' + base64String;
                resolve(base64Data);
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            });
    });
};

export const shareData = async (url = '', message) => {
    console.log('message', message);
    const shareOptions = {
        url: url,
        message: message,
    };

    try {
        const shareRes = await Share.open(shareOptions);
        console.log(shareRes);
    } catch (e) {
        console.log(e);
    }
};
