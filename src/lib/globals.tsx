import { Dimensions } from 'react-native';

export const WINDOW = Dimensions.get('window');

export const percent = (percentage: number) => {
	return (percentage / 100) * WINDOW.width - 20;
};

const { width: screenWidth, height: screenHeight } = WINDOW;
const realWidth = screenHeight > screenWidth ? screenWidth : screenHeight;

export const deviceWidth = screenWidth;
export const deviceHeight = screenHeight;

const compareScreenWidth = 375;
const compareScreenHeight = 667;

export const normalize = (size: number): number => {
	return Math.round((size * realWidth) / compareScreenWidth);
};

export const textSizes = {
	h1: normalize(32),
	h2: normalize(28),
	h3: normalize(26),
	h4: normalize(24),
	h5: normalize(22),
	h6: normalize(20),
	h7: normalize(18),
	h8: normalize(16),
	h9: normalize(14),
	h10: normalize(13),
	h11: normalize(12),
	h12: normalize(10),
	h13: normalize(8),
};
