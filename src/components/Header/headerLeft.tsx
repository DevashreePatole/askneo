import React from 'react';
import { normalize } from '../../lib/globals';
import { Icon } from '../Icon';
import { TouchItem } from '../TouchItem';

export const HeaderLeft = ({ onPress }) => {
    return (
        <TouchItem
            onPress={() => onPress()}
            style={{ left: normalize(10) }}>
            <Icon
                type="Feather"
                name="arrow-left"
                size={normalize(22)}
                color={'white'}
            />
        </TouchItem>
    );
};
