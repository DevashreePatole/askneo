import React, { FC } from 'react';

import { Dimensions, Image, Platform, StatusBar } from 'react-native';
import styled from 'styled-components/native';
import { prop } from 'styled-tools';

import { Colors } from '../../assets/styles/color';
import { normalize, textSizes } from '../../lib/globals';
import { Icon } from '../Icon';
import { Text } from '../Text';
import { TouchItem } from '../TouchItem';

interface Props {
  navigation: any;
  title: string;
}

export const Header: FC<Props> = ({ navigation, title }) => {
  return (
    <HeaderContainer>
      <StatusBar barStyle="light-content" backgroundColor={Colors.black} />
      <Row>
        <TouchItem onPress={() => navigation.openDrawer()}>
          <Icon
            name={'bars'}
            size={normalize(18)}
            color={'white'}
            type="FontAwesome5"
          />
        </TouchItem>
        <Title size={textSizes.h8} fontWeight="500">
          {title}
        </Title>
      </Row>
      <Icon
        name="search"
        size={normalize(18)}
        style={{ marginRight: 5 }}
        color={'white'}
        type="FontAwesome5"
      />
    </HeaderContainer>
  );
};

const Title = styled(Text)`
  margin-left: ${normalize(10)}px;
  color: ${Colors.white};
`;

const HeaderContainer = styled.View`
  width: ${Platform.OS == 'ios'
    ? Dimensions.get('window').width
    : Dimensions.get('window').width - 30}px;
  height: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: ${Platform.OS == 'ios' ? normalize(18) : 0}px;
  padding-right: ${Platform.OS == 'ios' ? normalize(18) : 0}px;
`;

const ProfileImg = styled(Image) <{ size: number }>`
  width: ${prop('size')}px;
  height: ${prop('size')}px;
`;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
