import React, { FC, memo, useState } from 'react';
import { Image, ImageProps, ImageStyle } from 'react-native';
import { ImageSource } from 'react-native-image-viewing/dist/@types';

export interface ImageProp extends ImageProps {
    style?: ImageStyle;
    source: ImageSource;
}

export const ImageComponent: FC<ImageProp> = ({ style, source, ...props }) => {
    const [loading, setLoading] = useState(true);

    return (
        <Image
            source={loading ? require('../../assets/images/Default.png') : source}
            onLoadEnd={() => setLoading(false)}
            {...props}
            style={style}
        />
    );
};
