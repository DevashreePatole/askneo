import React, { FC, useRef } from 'react';

import { View, ViewStyle } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';

import { Colors } from '../../assets/styles/color';
import { TouchItem, AddIcon } from '../index';
import { normalize } from '../../lib/globals';
import { Icon } from '../Icon';

interface Props {
  navigation: any;
  styles?: ViewStyle;
  onPress?: () => void;
}

export const AddItem: FC<Props> = ({ navigation, onPress, styles }) => {
  const refRBSheet = useRef<RBSheet>(null);

  return (
    <View>
      <IconContainer
        style={styles}
        onPress={() => {
          if (onPress == undefined) {
            refRBSheet.current?.open();
          } else {
            onPress();
          }
        }}>
        <Icon
          name="plus"
          size={normalize(18)}
          color={Colors.black}
          type="FontAwesome5"
        />
      </IconContainer>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: Colors.black,
          },
          container: {
            borderTopRightRadius: normalize(18),
            borderTopLeftRadius: normalize(18),
            borderColor: '#d6d6d6',
            borderWidth: normalize(1),
            height: normalize(330)
          },
        }}>
        <AddIcon navigation={navigation} refRBSheet={refRBSheet} />
      </RBSheet>
    </View>
  );
};

const IconContainer = styled(TouchItem)`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.primaryColor};
  position: absolute;
  bottom: ${normalize(8)}px;
  right: ${normalize(8)}px;
  border-radius: ${normalize(50)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
