import React, { FC } from 'react';

import { ViewStyle } from 'react-native';
import styled from 'styled-components/native';

import { Text } from '../Text';
import { normalize } from '../../lib/globals';
import { Icon } from '../Icon';

interface BackProps {
    onPress: () => void;
    style?: ViewStyle;
}

export const Back: FC<BackProps> = ({ onPress, style }) => {
    return (
        <BackContainer onPress={onPress} style={style}>
            <Icon type="FontAwesome" name={'chevron-left'} size={normalize(20)} />
            <Text style={{ marginLeft: 5 }} fontWeight="500" size={normalize(14)}>
                Back
            </Text>
        </BackContainer>
    );
};

const BackContainer = styled.TouchableOpacity`
  width: ${normalize(75)}px;
  flex-direction: row;
  margin-bottom: ${normalize(15)}px;
  margin-left: ${normalize(10)}px;
`;
