import React, { FC, useState } from 'react';

import styled from 'styled-components/native';

import { Colors } from '../../assets/styles/color';
import { Text } from '..';
import { TouchItem } from '../TouchItem';
import { ComingSoon } from '../ComingSoon';
import { normalize } from '../../lib/globals';

import { Icon } from '../Icon';
interface Props {
  navigation: any;
  refRBSheet: any;
}

export const AddIcon: FC<Props> = ({ navigation, refRBSheet }) => {
  const [modal, setModal] = useState(false);
  return (
    <MainContainer>
      <Row>
        <Column>
          <ItemContainer
            onPress={() => {
              setModal(true);
            }}>
            <IconContainer>
              <Icon
                name="blog"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Blog
            </Text>
          </ItemContainer>
        </Column>
        <Column>
          <ItemContainer
            onPress={() => {
              refRBSheet.current?.close();
              navigation.navigate('AddQuestions');
            }}>
            <IconContainer>
              <Icon
                name="question-answer"
                size={normalize(22)}
                color={Colors.black}
                type="MaterialIcons"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Questions
            </Text>
          </ItemContainer>
        </Column>
        <Column>
          <ItemContainer
            onPress={() => {
              refRBSheet.current?.close();
              navigation.navigate('CreateCommunity');
            }}>
            <IconContainer>
              <Icon
                name="users"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Community
            </Text>
          </ItemContainer>
        </Column>
      </Row>
      <Row>
        <Column>
          <ItemContainer
            onPress={() => {
              setModal(true);
            }}>
            <IconContainer>
              <Icon
                name="link"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Session Links
            </Text>
          </ItemContainer>
        </Column>
        <Column>
          <ItemContainer
            onPress={() => {
              refRBSheet.current?.close();
              navigation.navigate('AddDoubts');
            }}>
            <IconContainer>
              <Icon
                name="chalkboard-teacher"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Ask Me
            </Text>
          </ItemContainer>
        </Column>
        <Column>
          <ItemContainer
            onPress={() => {
              refRBSheet.current?.close();
              navigation.navigate('SelectImage');
            }}>
            <IconContainer>
              <Icon
                name="smile-wink"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Memes
            </Text>
          </ItemContainer>
        </Column>
      </Row>
      <Row style={{ justifyContent: 'center' }}>
        <Column>
          <ItemContainer
            onPress={() => {
              refRBSheet.current?.close();
              navigation.navigate('CreatePoll');
            }}>
            <IconContainer>
              <Icon
                name="poll"
                size={normalize(22)}
                color={Colors.black}
                type="FontAwesome5"
              />
            </IconContainer>
            <Text fontWeight="500" style={{ marginTop: normalize(10) }}>
              Poll
            </Text>
          </ItemContainer>
        </Column>
      </Row>
      <ComingSoon
        visibility={modal}
        handleModalVisibility={() => {
          setModal(false);
        }}
      />
    </MainContainer>
  );
};

const MainContainer = styled.View`
  padding-top: ${normalize(18)}px;
`;

const Column = styled.View`
  width: 33%;
  flex-direction: row;
  justify-content: center;
`;

const Row = styled.View`
  flex-direction: row;
  margin-top: 0px;
  margin-bottom: ${normalize(25)}px;
`;
const IconContainer = styled.View`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  border-radius: ${normalize(50)}px;
  background-color: ${Colors.primaryColor};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  align-self: center;
`;

const ItemContainer = styled(TouchItem)`
  justify-content: flex-start;
  align-items: flex-start;
`;
