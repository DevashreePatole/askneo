import React, { FC } from 'react';

import styled from 'styled-components/native';
import {
    TextInputProps,
    TouchableOpacityProps,
    ViewStyle,
    Platform,
    View,
    ScrollView,
} from 'react-native';
import { ifProp } from 'styled-tools';

import { Text } from '../Text';
import { Colors } from '../../assets/styles/color';
import { normalize, textSizes } from '../../lib/globals';
import { Icon } from '../Icon';

interface IDropdown extends TextInputProps {
    list: string[];
    label: string;
    selectedItem: number | null;
    handleSelection: (index: number) => void;
    zIndexVal: number;
    containerStyle?: ViewStyle;
    isTransparent?: boolean;
    onClick: (status: boolean) => void;
}

interface IDropdownItem extends TouchableOpacityProps {
    item: string;
    selected: boolean;
    index: number;
    array: any;
}

const DropdownItem = ({
    item,
    selected,
    index,
    array,
    ...props
}: IDropdownItem) => {
    return (
        <ItemContainer
            selected={selected}
            {...props}
            style={{ borderBottomWidth: array.length == index + 1 ? 0 : 1 }}>
            <Text style={{ color: selected ? Colors.lightBlue : Colors.black }}>
                {item}
            </Text>
        </ItemContainer>
    );
};
export const Dropdown: FC<IDropdown> = ({
    list,
    label,
    selectedItem,
    handleSelection,
    zIndexVal,
    containerStyle,
    isTransparent,
    onClick,
    ...props
}) => {
    const dropdownRef = React.useRef();
    const [showDropdownItem, setShowDropdownItem] = React.useState(false);

    const handleItemPress = React.useCallback((index: number) => {
        handleSelection?.(index);
    }, []);

    const renderItem = (item, index, array) => {
        return (
            <DropdownItem
                key={index}
                item={item}
                index={index}
                array={array}
                selected={selectedItem === index}
                onPress={() => {
                    handleItemPress(index);
                    setShowDropdownItem(false);
                    onClick(false);
                }}
            />
        );
    };
    const handleDropdownPress = React.useCallback(() => {
        setShowDropdownItem(true);
        onClick(true);
    }, [list]);

    return (
        <View style={Platform.OS == 'ios' && { zIndex: zIndexVal }}>
            <Container style={containerStyle}>
                <Label size={textSizes.h10} fontWeight="500">
                    {label}
                </Label>

                <DropdownContainer
                    ref={dropdownRef}
                    onPress={handleDropdownPress}
                    style={isTransparent && { backgroundColor: 'transparent' }}
                    disabled={!list?.length}>
                    <Input
                        {...props}
                        value={selectedItem !== null ? list[selectedItem] : ''}
                        editable={false}
                    />
                    <Icon type="Entypo" name="chevron-down" size={normalize(25)} />
                </DropdownContainer>
            </Container>
            {showDropdownItem && (
                <DropdownOverlay
                    style={{ zIndex: 100 + zIndexVal }}
                    onPress={() => {
                        setShowDropdownItem(false);
                        onClick(false);
                    }}>
                    <DropdownBodyContainer>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {list.map((item, index, array) => {
                                return renderItem(item, index, array);
                            })}
                        </ScrollView>
                    </DropdownBodyContainer>
                </DropdownOverlay>
            )}
        </View>
    );
};

const Container = styled.View`
  position: relative;
  padding: ${normalize(14)}px 0;
`;

const Label = styled(Text)`
  align-self: flex-start;
`;

const Input = styled.TextInput`
  color: ${Colors.black};
`;

const DropdownContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-bottom-color: #6f6f6f;
  width: 100%;
  padding: ${Platform.OS === 'android' && 0} ${normalize(8)}px;
  padding-left: ${normalize(4)}px;
  opacity: ${ifProp({ disabled: true }, 0.5, 1)};
`;

const DropdownOverlay = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  bottom: 0;
  height: 400%;
  left: 0;
  right: 0;
`;

const DropdownBodyContainer = styled.View`
  position: absolute;
  top: ${normalize(68)}px;
  align-self: center;
  max-height: ${normalize(200)}px;
  width: 100%;
  border: 1px solid #6f6f6f;
  border-radius: ${normalize(10)}px;
  background-color: ${Colors.white};
  overflow: hidden;
`;

const ItemContainer = styled.TouchableOpacity<{ selected: boolean }>`
  background-color: ${ifProp('selected', Colors.lightGray, Colors.white)};
  width: 100%;
  padding: ${normalize(15)}px;
  border-bottom-width: 1px;
  border-bottom-color: ${Colors.darkGray};
`;
