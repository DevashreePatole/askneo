import React, { memo } from 'react';

import {
  TextStyle,
  TextProps as TextProperties,
  FlexAlignType,
} from 'react-native';
import styled from 'styled-components/native';
import { textSizes } from '../../lib/globals';

import { FontWeight, TextWeight } from '../../types';

export interface TextProps extends TextProperties {
  children?: string | string[] | number | boolean;
  style?: TextStyle;
  weight?: TextWeight;
  size?: number;
  height?: number;
  fontWeight?: FontWeight;
  italic?: boolean;
  align?: FlexAlignType;
  withMargins?: boolean;
  numberOfLines?: number;
}

const fontWeightTypes = {
  regular: {
    style: '-Book',
  },
  500: {
    style: '-Medium',
  },
  bold: {
    style: '-Bold',
  },
  light: {
    style: '-Light',
  },
};

export const Text: React.FC<TextProps> = memo(
  ({
    children,
    style,
    weight,
    size,
    fontWeight,
    italic,
    align = 'center',
    ...props
  }) => (
    <TextComponent
      weight={weight}
      size={size}
      fontWeight={fontWeight}
      style={style}
      italic={italic}
      align={align}
      {...props}>
      {children}
    </TextComponent>
  ),
);

const TextComponent = styled.Text`
  font-size: ${({ size }: TextProps) => size || textSizes.h10}px;
  font-weight: ${({ fontWeight }: TextProps) => fontWeight || 400};
  line-height: ${({ height, size }: TextProps) => height || size || 14}px;
  font-family: Gotham${({ fontWeight }: TextProps) =>
    fontWeightTypes[fontWeight || 'regular']?.style};
  font-style: ${({ italic }: TextProps) => (italic ? 'italic' : 'normal')};
  align-self: ${({ align }: TextProps) => align};
`;
