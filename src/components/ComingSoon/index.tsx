import React, { FC } from 'react';

import { Modal, TouchableWithoutFeedback } from 'react-native';
import styled from 'styled-components/native';

import { Text } from '../Text';
import { Colors } from '../../assets/styles/color';
import { normalize } from '../../lib/globals';
import { Icon } from '../Icon';

interface Props {
  visibility: boolean;
  handleModalVisibility: () => void;
}

export const ComingSoon: FC<Props> = ({
  visibility,
  handleModalVisibility,
  ...props
}) => {
  return (
    <Modal visible={visibility} animationType={'fade'} transparent>
      <TouchableWithoutFeedback onPress={handleModalVisibility}>
        <OuterContainer>
          <InnerContainer>
            <Iconcontainer>
              <Icon
                type="Octicons"
                name="primitive-dot"
                size={normalize(30)}
                color={Colors.white}
              />
            </Iconcontainer>
            <Text fontWeight="600">Coming Soon</Text>
          </InnerContainer>
        </OuterContainer>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const OuterContainer = styled.View`
  flex: 1;
  background-color: ${Colors.transparent};
  justify-content: center;
  align-items: center;
`;
const InnerContainer = styled.View`
  width: ${normalize(180)}px;
  height: ${normalize(180)}px;
  background-color: ${Colors.white};
  justify-content: center;
  align-items: center;
  border-radius: ${normalize(5)}px;
`;
const Iconcontainer = styled.View`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  border-radius: ${normalize(50)}px;
  background-color: ${Colors.primaryColor};
  margin-bottom: ${normalize(20)}px;
  padding-left: ${normalize(16)}px;
`;
