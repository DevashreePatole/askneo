import React, { FC } from 'react';

import { TouchableOpacity, View, ViewStyle } from 'react-native';

interface Props {
    onPress: () => void;
    disabled?: boolean;
    style?: ViewStyle;
}

export const TouchItem: FC<Props> = ({ style, children, ...props }) => {
    return (
        <TouchableOpacity
            {...props}
            onPress={() => {
                props.onPress();
            }}
            activeOpacity={props.disabled ? 1 : 0.8}
            style={style}>
            <View>{children}</View>
        </TouchableOpacity>
    );
};
