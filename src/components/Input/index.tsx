import React, { FC, useState } from 'react';

import {
    View,
    TextInputProps,
    KeyboardTypeOptions,
    ViewStyle,
} from 'react-native';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { prop, ifProp } from 'styled-tools';

import { Text } from '../Text';
import { Colors } from '../../assets/styles/color';
import { normalize, textSizes } from '../../lib/globals';

interface Props extends TextInputProps {
    placeholder?: string;
    style?: ViewStyle;
    value: string;
    keyboardType?: KeyboardTypeOptions;
    secureTextEntry?: boolean;
    editable?: boolean;
    onFocus?: () => void;
    onBlur?: (val: any) => void;
    onChangeText: (val: any) => void;
    maxLength?: number;
    autoCorrect?: boolean;
    icon?: string;
    label?: string;
    iconAction?: () => void;
    iconLib?: string;
    height?: number;
    errorText?: string | boolean;
    validationType?: string;
    inputColor?: string;
    iconStyle?: ViewStyle;
    iconSize?: number;
    multiline?: boolean
}

export const Input: FC<Props> = ({
    placeholder,
    style,
    value,
    keyboardType = 'default',
    secureTextEntry = false,
    editable,
    onFocus,
    onBlur,
    onChangeText,
    maxLength,
    autoCorrect,
    icon,
    label,
    iconAction,
    height = 40,
    errorText,
    validationType,
    iconStyle,
    inputColor = 'black',
    iconSize=20,
    multiline=false
}) => {
    // useEffect(()=>{
    //     validationSwitch(validationType)
    // },[validationType])
    const [sPassword, setSPassword] = React.useState(secureTextEntry);
    const [error, setError] = useState('');

    const validationSwitch = (validationType) => {
        switch (validationType) {
            case 'req':
                if (value.length < 1) {
                    setError('Required Field');
                } else {
                    setError('');
                }
                break;
            case 'email':
                var mail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!mail.test(value)) {
                    setError('Invalid Email Id');
                } else {
                    setError('');
                }
                break;
            case 'phone':
                if (value.length < 10 || value.length > 10) {
                    setError('Invalid phone number');
                } else {
                    setError('');
                }
            default:
                break;
        }
    };

    return (
        <View>
            <InputContainer>
                <View style={{ flex: 1 }}>
                    {label && (
                        <Text
                            align={'flex-start'}
                            style={{ paddingLeft: 2, color: inputColor }}
                            fontWeight="500">
                            {label}
                        </Text>
                    )}
                    <InputBox
                        style={style}
                        error={errorText ? true : false}
                        height={style?.height || height}
                        placeholder={placeholder}
                        value={value}
                        keyboardType={keyboardType}
                        secureTextEntry={sPassword}
                        editable={editable}
                        multiline={multiline}
                        autoCapitalize={'none'}
                        onFocus={onFocus}
                        onBlur={onBlur}
                        onChangeText={onChangeText}
                        maxLength={maxLength}
                        autoCorrect={autoCorrect}
                        color={inputColor}
                        placeholderTextColor={inputColor == 'black' ? '#a9a9a9' : '#989898'}
                    />
                </View>
                {icon == 'eye' || icon == 'eye-slash' ? (
                    <InputIcon
                        top={style?.height / 2 || height / 2}
                        name={sPassword ? 'eye-slash' : 'eye'}
                        onPress={() => setSPassword((prevVal) => !prevVal)}
                        size={iconSize}
                        color={inputColor == 'black' ? 'gray' : '#989898'}
                    />
                ) : (
                    <InputIcon
                        style={iconStyle}
                        top={style?.height / 2 || height / 2}
                        onPress={iconAction}
                        name={icon}
                        size={iconSize}
                        color={inputColor == 'black' ? 'gray' : '#989898'}
                    />
                )}
            </InputContainer>
            {errorText ? (
                <ErrorText
                    color={inputColor == 'black' ? Colors.darkRed : Colors.primaryColor}>
                    {errorText}
                </ErrorText>
            ) : null}
        </View>
    );
};

const ErrorText = styled(Text) <{ color }>`
  align-self: flex-start;
  font-size: ${textSizes.h12}px;
  margin-left: ${normalize(3)}px;
  margin-top: ${normalize(3)}px;
  color: ${prop('color')};
`;

const InputContainer = styled.View`
  margin-top: ${normalize(30)}px;
  width: 100%;
  flex-direction: row;
`;

const InputBox = styled.TextInput<{ height; error; color }>`
  border-bottom-width: ${normalize(1)}px;
  width: 100%;
  height: ${prop('height')}px;
  padding-left: ${normalize(3)}px;
  border-color: ${ifProp({ error: true }, Colors.darkRed, 'gray')};
  font-size: ${textSizes.h9}px;
  color: ${prop('color')};
`;

const InputIcon = styled(Icon) <{ top }>`
  position: absolute;
  right: ${normalize(5)}px;
  top: ${prop('top')}px;
`;
