import React, { FC } from 'react';

import { Animated, StyleSheet, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

interface Props {
    size: number;
    style?: ViewStyle;
    name: string;
    color: string;
    currentValue: object;
}

export const AnimateIcon = (currentValue, callBack) => {
    Animated.spring(currentValue, {
        toValue: 2,
        friction: 2,
        useNativeDriver: true,
    }).start(() => {
        Animated.timing(currentValue, {
            toValue: 0,
            duration: 500,
            useNativeDriver: true,
        }).start(() => {
            callBack();
        });
    });
};
export const AnimatedIcon: FC<Props> = ({
    size,
    name,
    color,
    style,
    currentValue,
    ...props
}) => {
    const AnimatedIcon = Animated.createAnimatedComponent(Icon);
    return (
        <AnimatedIcon
            name={name}
            size={size}
            color={color}
            style={{
                ...styles.animatedStyle,
                ...style,
                transform: [{ scale: currentValue }],
            }}
        />
    );
};

const styles = StyleSheet.create({
    animatedStyle: {
        position: 'absolute',
        left: '45%',
        top: '45%',
    },
});
