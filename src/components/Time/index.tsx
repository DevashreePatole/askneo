import React, { FC } from 'react';

import { Text, TextStyle } from 'react-native';
import moment from 'moment';

interface Props {
  time: string;
  style?: TextStyle;
}

export const TimeAgo: FC<Props> = ({ time, style, ...props }) => {
  const date = moment(time, 'YYYYMMDD').fromNow();
  var days = date.match(/\d/g);
  if (days == null) {
    const newDate = moment(time).format('LL');
    return <Text style={style}>{newDate}</Text>;
  }
  var numOfDays = days.join('');
  if (parseInt(numOfDays) > 5) {
    const dateTime = moment(time).format('LL');
    return <Text style={style}>{dateTime}</Text>;
  } else {
    return <Text style={style}>{date}</Text>;
  }
};
