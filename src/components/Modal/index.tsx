import React, { FC } from 'react';

import { Modal, TouchableWithoutFeedback, Text } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../assets/styles/color';
import { normalize, WINDOW } from '../../lib/globals';

interface Props {
  visibility: boolean;
  handleModalVisibility: () => void;
  children?: React.ReactNode;
}

export const ModalContainer: FC<Props> = ({
  visibility,
  handleModalVisibility,
  children,
  ...props
}) => {
  return (
    <Modal visible={visibility} animationType={'fade'} transparent>
      <TouchableWithoutFeedback onPress={handleModalVisibility}>
        <OuterContainer>
          <InnerContainer>{children}</InnerContainer>
        </OuterContainer>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const OuterContainer = styled.View`
  flex: 1;
  background-color: ${Colors.transparent};
  justify-content: center;
  align-items: center;
`;
const InnerContainer = styled.View`
  width: ${WINDOW.width - 20}px;
  height: ${normalize(230)}px;
  background-color: ${Colors.white};
  justify-content: center;
  align-items: center;
  border-radius: ${normalize(5)}px;
`;
