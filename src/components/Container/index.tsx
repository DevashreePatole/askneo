import React, { ReactNode, memo } from 'react';

import {
    ViewStyle,
    ViewProps,
    TouchableWithoutFeedback,
    Keyboard,
    Modal,
    Platform,
} from 'react-native';
import styled from 'styled-components/native';
import LottieView from 'lottie-react-native';

import { Colors } from '../../assets/styles/color';
import { normalize } from '../../lib/globals';

interface ContainerProps extends ViewProps {
    children: ReactNode | ReactNode[];
    style?: ViewStyle;
    paddingStyle?: ViewStyle;
    loading?: boolean;
    withKeyboard?: boolean;
}

export const Container: React.FC<ContainerProps> = ({
    children,
    style,
    loading = false,
    withKeyboard = true,
}) => {
    const renderContainer = React.useCallback(
        () => (
            <ContainerView style={style}>
                <Modal animationType={'fade'} transparent={true} visible={loading}>
                    <LoaderContainer>
                        <LottieView
                            source={require('../../assets/images/json/loadingHand.json')}
                            style={{ width: normalize(190), height: normalize(190) }}
                            autoPlay
                            loop
                        />
                    </LoaderContainer>
                </Modal>
                {children}
            </ContainerView>
        ),
        [children],
    );

    return withKeyboard ? (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            {renderContainer()}
        </TouchableWithoutFeedback>
    ) : (
        renderContainer()
    );
};

export const SafeContainer: React.FC<ContainerProps> = memo(
    ({ children, style, paddingStyle, loading = false, withKeyboard = true }) => {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <SafeContainerView style={style}>
                    <Padding
                        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                        style={paddingStyle}>
                        <Modal animationType={'fade'} transparent={true} visible={loading}>
                            <LoaderContainer>
                                <LottieView
                                    source={require('../../assets/images/json/loadingHand.json')}
                                    style={{ width: normalize(190), height: normalize(190) }}
                                    autoPlay
                                    loop
                                />
                            </LoaderContainer>
                        </Modal>
                        {children}
                    </Padding>
                </SafeContainerView>
            </TouchableWithoutFeedback>
        );
    },
);

const ContainerView = styled.View`
  flex: 1;
  padding: ${normalize(10)}px;
  background-color: ${Colors.white};
`;

const SafeContainerView = styled.SafeAreaView`
  padding: ${normalize(10)}px;
  background-color: ${Colors.white};
`;

const Padding = styled.KeyboardAvoidingView`
  padding-top: ${normalize(20)}px;
  padding-bottom: ${normalize(20)}px;
`;

const LoaderContainer = styled.View`
  flex: 1;
  background-color: #000000aa;
  justify-content: center;
  align-items: center;
`;
