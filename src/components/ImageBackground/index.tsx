import React, { FC } from 'react';

import { Dimensions } from 'react-native';
import styled from 'styled-components/native';

import LinearGradient from 'react-native-linear-gradient';

export const ImageBackground: FC = () => {
  return (
    <BackContainer>
      <BackImage 
      resizeMode={'stretch'}
      source={require('../../assets/images/BackgroundImage.png')}
      >
         <LinearGradient colors={['#ffffff','#000000','#000000']} style={{flex:1,opacity:0.85}}>
        <Shade />
        </LinearGradient>
      </BackImage>
    </BackContainer>
  );
};

const BackImage = styled.ImageBackground`
  width: ${Dimensions.get('window').width}px;
  height: ${Dimensions.get('window').height}px;
  z-index: -1;
`;

const BackContainer = styled.View`
  position: absolute;
  width: ${Dimensions.get('window').width}px;
  height: ${Dimensions.get('window').height}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  z-index: -1;
  opacity: 0.8;
`;

const Shade = styled.View`
  flex: 1;
  background-color:#010101;
  opacity: 0.5;
`;
