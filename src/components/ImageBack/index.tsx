import React, { FC, useState } from 'react';

import { ImageBackground, ImageProps, ImageStyle } from 'react-native';
import { ImageSource } from 'react-native-image-viewing/dist/@types';

export interface ImageProp extends ImageProps {
    style?: ImageStyle;
    source: ImageSource;
}

export const ImageBack: FC<ImageProp> = ({ style, source, ...props }) => {
    const [loading, setLoading] = useState(true);

    return (
        <ImageBackground
            source={loading ? require('../../assets/images/Default.png') : source}
            onLoadEnd={() => setLoading(false)}
            {...props}
            style={style}
        />
    );
};
