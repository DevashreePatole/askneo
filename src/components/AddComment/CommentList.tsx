import React, { FC } from 'react'

import {View ,TouchableWithoutFeedback, StyleSheet} from 'react-native'
import styled from 'styled-components/native'
import { Colors } from '../../assets/styles/color'
import { normalize, textSizes } from '../../lib/globals'
import { Icon } from '../Icon'
import { ImageComponent } from '../Image'
import { Text } from '../Text'
import { TouchItem } from '../TouchItem'

interface DataProps {
    id:string;
    profileImage:string;
    employeeName: string;
    empId: string;
    likes: number;
    comment:string;
}

interface Props {
    item:DataProps;
    inputRef:any;
    setInput : (boolean) => void;
}

export const CommentListComponent:FC<Props> = ({item,inputRef,setInput}) =>{
    return(
        <TouchableWithoutFeedback>
        <ViewContainer>
          <Image source={{uri: `${item.profileImage}`}} />
          <CommentContainer>
            <Text
              fontWeight="500"
              size={textSizes.h9}
              style={{alignSelf: 'flex-start'}}>
              {item.employeeName}
            </Text>
            <DetailContainer>
              <Text size={12} style={Styles.textStyle}>
                {item.comment}
              </Text>
            </DetailContainer>
            <FooterContainer>
              <View style={{flexDirection: 'row'}}>
                <Text style={Styles.like} size={13}>
                  {item.likes}
                </Text>
                <Icon
                  name="thumbs-o-up"
                  size={normalize(18)}
                  color={Colors.darkGray}
                  type="FontAwesome"
                />
              </View>
              <TouchItem onPress={() => {
                inputRef.current?.focus()
              setInput(true)
              }}
                >
                <View>
                  <Text italic={true} style={Styles.reply}>
                    Reply
                  </Text>
                </View>
              </TouchItem>
            </FooterContainer>
          </CommentContainer>
        </ViewContainer>
      </TouchableWithoutFeedback>
    )
}
const MainContainer = styled.View`
  padding: ${normalize(10)}px;
  flex:1px;
  margin-bottom:${normalize(20)}px;
`;

const InputContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;

const InputBox = styled.TextInput`
  border-color: ${Colors.darkGray};
  border-width: ${normalize(1)}px;
  border-radius: ${normalize(25)}px;
  padding-left: ${normalize(15)}px;
  height: ${normalize(48)}px;
  margin-bottom: ${normalize(20)}px;
  width: 85%;
  margin-top: ${normalize(0)}px;
`;

const Image = styled(ImageComponent)`
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(35)}px;
`;

const ViewContainer = styled.View`
  padding-left: ${normalize(10)}px;
  margin-bottom: ${normalize(18)}px;
  flex-direction: row;
`;

const CommentContainer = styled.View`
  margin-left: ${normalize(10)}px;
  padding-top: ${normalize(2)}px;
  margin-right: ${normalize(18)}px;
  width: 80%;
`;

const IconContainer = styled(TouchItem)`
  padding-top: ${normalize(12)}px;
  padding-right: ${normalize(5)}px;
  width: ${normalize(50)}px;
  align-items: center;
`;

const DetailContainer = styled.View`
  background-color: ${Colors.appGray};
  padding: ${normalize(5)}px;
  border-radius: ${normalize(5)}px;
  margin-top: ${normalize(5)}px;
`;

const FooterContainer = styled.View`
  flex-direction: row;
  width: ${normalize(120)}px;
  justify-content: space-between;
  margin-top: ${normalize(5)}px;
  margin-left: ${normalize(5)}px;
`;

const Styles = StyleSheet.create({
  like: {
    paddingRight: 5,
    paddingTop: 2,
    color: Colors.darkGray,
  },
  reply: {
    color: Colors.darkGray,
    textDecorationLine: 'underline',
  },
  textStyle: {
    paddingTop: normalize(8),
    lineHeight: normalize(18),
    alignSelf: 'flex-start',
    paddingBottom: normalize(8),
    paddingLeft: normalize(5),
  },
});

export const CommentList= React.memo(CommentListComponent)
