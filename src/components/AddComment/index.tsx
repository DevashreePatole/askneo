import React, {FC, useRef, useState} from 'react';

import {
  FlatList,
  View,
  TextInput,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import styled from 'styled-components/native';

import {Colors} from '../../assets/styles/color';
import {Text} from '../../components';
import {TouchItem} from '../TouchItem';
import {normalize, textSizes} from '../../lib/globals';
import {Icon} from '../Icon';
import {ImageComponent} from '../Image';
import { CommentList } from './CommentList';
// import { TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const AddComment: FC = () => {
  const inputRef = useRef<TextInput>(null);

  const [commentList, setUpdatedList] = useState([
    {
      id:'1',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      empId: '1234',
      likes: 5,
      comment:
        'Sharp thinking. I appreciate your cooperation. Outstanding. This is prize-winning work.Super job/Super work',
    },
    {
      id:'2',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      empId: '1234',
      likes: 5,
      comment:
        'Sharp thinking. I appreciate your cooperation. Outstanding. This is prize-winning work.Super job/Super work',
    },
    {
      id:'3',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Vartika Verma',
      empId: '1234',
      likes: 5,
      comment:
        'Sharp thinking. I appreciate your cooperation. Outstanding. This is prize-winning work.Super job/Super work',
    },
  ]);
  const [comment, setComment] = useState({
    profileImage:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
    employeeName: 'Vartika Verma',
    empId: '1235',
    likes: 5,
    comment: '',
  });

  const [input, setInput] = useState(false);
  const [reply, setReply] = useState('');

  const addComment = () => {
    const newArray = [...commentList];
    newArray.unshift({...comment,id:(commentList.length +1).toString()});
    setUpdatedList(newArray);
    // console.log('comment called');
    setComment((prev) => {
      return {...prev, comment: ''};
    });
  };
  const _keyCountryExtractor = (item, index) => item.id

  const _renderList = ({item, index}) => {
    return (
      <CommentList item={item} inputRef={inputRef} setInput={setInput}/>
    );
  };
  return (
    <MainContainer>
      <InputContainer>
        <InputBox
          placeholder="Add a Comment"
          placeholderTextColor={Colors.darkGray}
          onChangeText={(t) => {
            setComment((prev) => {
              return {...prev, comment: t};
            });
          }}
          value={comment.comment}
        />
        <IconContainer
          disabled={comment.comment.length <= 0}
          onPress={() => {
            addComment();
          }}>
          <Icon
            name="send"
            type="FontAwesome"
            size={normalize(22)}
            color={
              comment.comment.length > 0 ? Colors.lightBlue : Colors.darkGray
            }
          />
        </IconContainer>
      </InputContainer>

      <FlatList
        data={commentList}
        renderItem={_renderList}
        keyExtractor={_keyCountryExtractor}
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}
        extraData={commentList.length}
        keyboardShouldPersistTaps={'always'}
      />
      {input && (
        <TextInput
          ref={inputRef}
          placeholder="Reply to Comment"
          value={reply}
          onChangeText={(t) => {
            setReply(t);
          }}
          style={{
            position: 'absolute',
            bottom: 0,
            borderBottomWidth: 1,
            width: '90%',
            // height: 30,
            marginLeft:25,
            paddingLeft:10,
            backgroundColor:'white'
          }}
        />
      )}
    </MainContainer>
  );
};

const MainContainer = styled.View`
  padding: ${normalize(10)}px;
  flex:1px;
  margin-bottom:${normalize(20)}px;
`;

const InputContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;

const InputBox = styled.TextInput`
  border-color: ${Colors.darkGray};
  border-width: ${normalize(1)}px;
  border-radius: ${normalize(25)}px;
  padding-left: ${normalize(15)}px;
  height: ${normalize(48)}px;
  margin-bottom: ${normalize(20)}px;
  width: 85%;
  margin-top: ${normalize(0)}px;
`;

const Image = styled(ImageComponent)`
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(35)}px;
`;

const ViewContainer = styled.View`
  padding-left: ${normalize(10)}px;
  margin-bottom: ${normalize(18)}px;
  flex-direction: row;
`;

const CommentContainer = styled.View`
  margin-left: ${normalize(10)}px;
  padding-top: ${normalize(2)}px;
  margin-right: ${normalize(18)}px;
  width: 80%;
`;

const IconContainer = styled(TouchItem)`
  padding-top: ${normalize(12)}px;
  padding-right: ${normalize(5)}px;
  width: ${normalize(50)}px;
  align-items: center;
`;

const DetailContainer = styled.View`
  background-color: ${Colors.appGray};
  padding: ${normalize(5)}px;
  border-radius: ${normalize(5)}px;
  margin-top: ${normalize(5)}px;
`;

const FooterContainer = styled.View`
  flex-direction: row;
  width: ${normalize(120)}px;
  justify-content: space-between;
  margin-top: ${normalize(5)}px;
  margin-left: ${normalize(5)}px;
`;

const Styles = StyleSheet.create({
  like: {
    paddingRight: 5,
    paddingTop: 2,
    color: Colors.darkGray,
  },
  reply: {
    color: Colors.darkGray,
    textDecorationLine: 'underline',
  },
  textStyle: {
    paddingTop: normalize(8),
    lineHeight: normalize(18),
    alignSelf: 'flex-start',
    paddingBottom: normalize(8),
    paddingLeft: normalize(5),
  },
});
