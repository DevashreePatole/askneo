import React, { FC } from 'react';

import { ActivityIndicator, ViewStyle } from 'react-native';
import styled from 'styled-components/native';
import { ifProp, prop } from 'styled-tools';

import { normalize, percent, textSizes } from '../../lib/globals';
import { Colors } from '../../assets/styles/color';
import { TouchItem } from '../TouchItem';

interface Props {
  title: string;
  loading?: boolean;
  onClick: () => void;
  style?: ViewStyle;
  disabled?: boolean;
  width?: number;
  fontSize?: number;
  color?: string;
}

const Button: FC<Props> = ({
  title,
  disabled,
  children,
  onClick,
  width,
  loading,
  fontSize,
  color,
  ...props
}) => {
  return (
    <TouchContainer
      onPress={
        disabled
          ? () => {
            ('');
          }
          : onClick
      }
      disabled={disabled}>
      <ButtonView style={props.style} disabled={disabled} width={width}>
        {loading ? (
          <ActivityIndicator
            size="small"
            color={Colors.white}
            style={{ padding: normalize(15) }}
          />
        ) : (
          <ButtonText color={color || 'black'} size={fontSize || textSizes.h7}>
            {title}
          </ButtonText>
        )}
      </ButtonView>
    </TouchContainer>
  );
};

const ButtonView = styled.View`
  background-color: ${ifProp(
  { disabled: true },
  Colors.darkGray,
  Colors.primaryColor,
)};
  width: ${({ width }: Props) => percent(width) || percent(75)}px;
  height: ${normalize(50)}px;
  border-radius: ${normalize(10)}px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const ButtonText = styled.Text<{ size; color }>`
  color: ${prop('color')};
  text-align: center;
  font-size: ${prop('size')}px;
  font-weight: 400;
`;

const TouchContainer = styled(TouchItem)`
  justify-content:center
  align-items:center
`;

export default Button;
