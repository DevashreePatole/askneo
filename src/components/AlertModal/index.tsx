import React, { FC, useImperativeHandle, useState } from 'react';

import { Modal } from 'react-native';
import styled from 'styled-components/native';

import { TouchItem } from '../TouchItem';
import { Colors } from '../../assets/styles/color';
import { Text } from '../Text';
import { normalize, textSizes } from '../../lib/globals';
import { Icon } from '../Icon';

interface Props { }

const AlertModal: FC<Props> = (props, ref) => {
    const [theme, setTheme] = useState('');
    const [icon, setIcon] = useState('');
    const [text, setText] = useState('');
    const [iconColor, setIconColor] = useState('');
    const [visible, setVisible] = useState(false);
    const [buttons, setButtons] = useState<{ text: string; action: () => void }[]>(
        [],
    );
    useImperativeHandle(ref, () => ({
        setOptionAndOpen: (preset, text, callback) => {
            switch (preset) {
                case 'error':
                    setTheme('Error');
                    setIcon('x-circle');
                    setText(text);
                    setIconColor(Colors.red);
                    setButtons([{ text: 'OK', action: () => callback() }]);
                    break;

                case 'warning':
                    setTheme('Warning');
                    setIcon('alert-triangle');
                    setIconColor(Colors.primaryColor);
                    setText(text);
                    setButtons([{ text: 'OK', action: () => callback() }]);
                    break;

                case 'success':
                    setTheme('Success');
                    setIcon('check-circle');
                    setIconColor(Colors.green);
                    setText(text);
                    setButtons([{ text: 'OK', action: () => callback() }]);
                    break;

                case 'sure':
                    setTheme('Warning');
                    setIcon('alert-triangle');
                    setIconColor(Colors.primaryColor);
                    setText(text);
                    setButtons([
                        { text: 'No', action: () => { } },
                        { text: 'Yes', action: () => callback() },
                    ]);
                    break;
            }
            open();
        },
    }));

    const open = () => {
        setVisible(true);
    };

    return (
        <Modal
            ref={ref}
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={() => setVisible(false)}>
            <BackDrop>
                <Container>
                    <IconContainer>
                        <Icon
                            size={normalize(45)}
                            name={icon}
                            color={iconColor}
                            type="Feather"
                        />
                    </IconContainer>
                    <MessageContainer>
                        <Text
                            align="flex-start"
                            size={textSizes.h5}
                            fontWeight="500"
                            style={{ marginTop: normalize(18) }}>
                            {theme}
                        </Text>
                        <Text
                            align="flex-start"
                            size={textSizes.h9}
                            style={{ marginTop: normalize(14), lineHeight: normalize(16) }}>
                            {text}
                        </Text>
                    </MessageContainer>
                    <ButtonContainer>
                        {buttons.map((button,index) => {
                            return (
                                <Button
                                key={index}
                                    onPress={() => {
                                        setVisible(false);
                                        button.action();
                                    }}>
                                    <Text
                                        style={{
                                            color:
                                                button.text == 'No'
                                                    ? Colors.darkGray
                                                    : Colors.primaryColor,
                                        }}
                                        size={textSizes.h9}
                                        fontWeight="500">
                                        {button.text || 'OK'}
                                    </Text>
                                </Button>
                            );
                        })}
                    </ButtonContainer>
                </Container>
            </BackDrop>
        </Modal>
    );
};

const BackDrop = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
  justify-content: center;
  flex-direction: row;
  align-items: center;
`;

const Container = styled.View`
  width: 80%;
  background-color: ${Colors.white};
  border-radius: ${normalize(20)}px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const IconContainer = styled.View`
  width: 100%;
  height: ${normalize(120)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: ${Colors.lightYellow2};
  border-top-left-radius: ${normalize(20)}px;
  border-top-right-radius: ${normalize(20)}px;
`;

const MessageContainer = styled.View`
  width: 100%;
  flex-direction: column;
  margin-top: ${normalize(10)}px;
  padding-left: ${normalize(28)}px;
  padding-right: ${normalize(35)}px;
`;

const Button = styled(TouchItem)`
  width: ${normalize(38)}px;
  height: ${normalize(38)}px;
  align-self: flex-end;
  margin-right: ${normalize(20)}px;
  margin-top: ${normalize(14)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin-bottom: ${normalize(14)}px;
`;

const ButtonContainer = styled.View`
flex-direction: row;
justify-content: flex-end;
width: 100%;
`;

const forwordRef = React.forwardRef(AlertModal);

export default forwordRef;
