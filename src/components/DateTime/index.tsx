import React, { FC, useEffect, useState } from 'react';

import styled from 'styled-components/native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment, { MomentInput } from 'moment';

import { normalize, textSizes } from '../../lib/globals';
import { Text } from '../Text';
import { Icon } from '../Icon';

interface DateProps {
    date: Date | string;
    setDate: any;
    maximumDate?: Date | undefined;
    minimumDate?: Date | undefined;
}

interface TimeProps {
    time: Date | string;
    setTime: any;
    minimumTime?: Date | undefined;
}

export const DatePick: FC<DateProps> = ({
    maximumDate = new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000),
    minimumDate = new Date(),
    date = '',
    setDate,
}) => {
    const [show, setShow] = useState(false);

    const showDatePicker = () => {
        setShow(true);
    };

    const hideDatePicker = () => {
        setShow(false);
    };

    const handleConfirm = (date) => {
        // console.log(moment(date).format('DD/MM/YYYY'));
        setDate(moment(date).format('DD/MM/YYYY'));
        hideDatePicker();
    };

    useEffect(() => {
        let calDate: MomentInput = moment(new Date()).add(1, 'days');
        calDate = moment(calDate).format('DD/MM/YYYY');
        setDate(calDate);
    }, []);

    return (
        <ScreenContainer>
            <Wraper>
                <Row>
                    <Icon
                        type="MaterialIcons"
                        name="date-range"
                        size={normalize(22)}
                        onPress={() => {
                            showDatePicker();
                        }}
                    />
                    <Text style={{ marginLeft: normalize(20) }} size={textSizes.h8}>
                        {date.toString()}
                    </Text>
                </Row>
                <DateTimePickerModal
                    isVisible={show}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                    minimumDate={minimumDate}
                    maximumDate={maximumDate}
                />
            </Wraper>
        </ScreenContainer>
    );
};

export const TimePick: FC<TimeProps> = ({
    time = '',
    setTime,
    minimumTime = new Date(),
}) => {
    const [show, setShow] = useState(false);

    const showDatePicker = () => {
        setShow(true);
    };

    const hideDatePicker = () => {
        setShow(false);
    };

    const handleConfirm = (date) => {
        // console.log(moment(date).format('DD/MM/YYYY'));
        setTime(moment(date).format('LT'));
        hideDatePicker();
    };

    useEffect(() => {
        let calDate: MomentInput = moment(new Date()).add(60, 'minutes');
        calDate = moment(calDate).format('LT');
        setTime(calDate);
    }, []);

    return (
        <ScreenContainer>
            <Wraper>
                <Row>
                    <Icon
                        type="Feather"
                        name="clock"
                        size={normalize(22)}
                        onPress={() => {
                            showDatePicker();
                        }}
                    />
                    <Text style={{ marginLeft: normalize(20) }} size={textSizes.h8}>
                        {time.toString()}
                    </Text>
                </Row>
                <DateTimePickerModal
                    mode="time"
                    locale="en_GB"
                    date={new Date()}
                    isVisible={show}
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                    minimumDate={minimumTime}
                />
            </Wraper>
        </ScreenContainer>
    );
};

const ScreenContainer = styled.View``;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Wraper = styled.View`
  margin-top: ${normalize(20)}px;
`;
