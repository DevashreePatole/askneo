import React, { FC } from 'react';

import { NavigationContainer } from '@react-navigation/native';

import { MainStack } from './stack/mainStack';

export const RootNavigation: FC = () => {
    return (
        <NavigationContainer>
            <MainStack/>
        </NavigationContainer>
    );
};
