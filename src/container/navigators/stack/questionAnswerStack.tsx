import React, { FC } from 'react';

import { Platform } from 'react-native';
import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';
import { RouteProp } from '@react-navigation/native';

import { QuestionAnswer } from '../../screens/QuestionAnswer';
import { Header } from '../../../components/Header';
import { Colors } from '../../../assets/styles/color';
import { QuestionDetail } from '../../screens/QuestionAnswer/QuestionDetail';
import { normalize, textSizes } from '../../../lib/globals';
import { HeaderLeft } from '../../../components/Header/headerLeft';

export type RootStackParamList = {
    QuestionAnswer: undefined;
    QuestionDetail: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type QuestionAnswerScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'QuestionAnswer'
>;
type QuestionAnswerScreenRouteProp = RouteProp<
    RootStackParamList,
    'QuestionAnswer'
>;

type Props = {
    navigation: QuestionAnswerScreenNavigationProp;
    route: QuestionAnswerScreenRouteProp;
};

export const QuestionAnswerStack: FC<Props> = ({ navigation, route }) => {
    React.useLayoutEffect(() => {
        if (getFocusedRouteNameFromRoute(route) == 'QuestionDetail') {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [getFocusedRouteNameFromRoute(route)]);

    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
            }}>
            <Stack.Screen
                name="QuestionAnswer"
                component={QuestionAnswer}
                options={{
                    title: 'Home',
                    headerTitle: () => (
                        <Header navigation={navigation} title="Questions" />
                    ),
                }}
            />
            <Stack.Screen
                name="QuestionDetail"
                component={QuestionDetail}
                options={({ navigation }) => ({
                    title: 'Question Detail',
                    headerBackTitleVisible: false,
                    headerLeft:
                        Platform.OS !== 'ios'
                            ? null
                            : () => (
                                <HeaderLeft
                                    onPress={() => navigation.navigate('QuestionAnswer')}
                                />
                            ),
                    headerTitleStyle: {
                        fontSize: textSizes.h8,
                        color: 'white',
                    },
                    headerTitleAlign: 'left',
                    headerTitleContainerStyle: {
                        left: Platform.OS !== 'ios' ? normalize(18) : normalize(45),
                    },
                })}
            />
        </Stack.Navigator>
    );
};
