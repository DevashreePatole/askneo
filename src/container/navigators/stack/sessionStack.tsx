import React, { FC } from 'react';

import { Platform } from 'react-native';
import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Feather';
import { RouteProp } from '@react-navigation/core';

import { SessionLinks } from '../../screens/SessionLinks';
import { Colors } from '../../../assets/styles/color';
import { YouTubeVideo } from '../../screens/SessionLinks/YoutubeVideo';
import { TouchItem } from '../../../components';

export type RootStackParamList = {
  Sessions: undefined;
  Video: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type SessionScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Sessions'
>;
type SessionScreenRouteProp = RouteProp<RootStackParamList, 'Sessions'>;

type Props = {
  navigation: SessionScreenNavigationProp;
  route: SessionScreenRouteProp;
};

export const SessionStack: FC<Props> = ({ navigation }) => {
  const HeaderLeft = (navigation) => (
    <TouchItem onPress={() => navigation.goBack()} style={{ left: 12 }}>
      <Icon name="arrow-left" size={25} color={'white'} />
    </TouchItem>
  );

  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
      }}>
      <Stack.Screen
        name="Sessions"
        component={SessionLinks}
        options={({ navigation }) => ({
          title: 'Session Links',
          headerBackTitleVisible: false,
          headerLeft:
            Platform.OS !== 'ios' ? null : () => HeaderLeft(navigation),
          headerTitleStyle: {
            fontSize: 18,
            color: 'white',
          },
          headerTitleAlign: 'left',
          headerTitleContainerStyle: {
            left: Platform.OS !== 'ios' ? 20 : 50,
          },
        })}
      />
      <Stack.Screen
        name="Video"
        component={YouTubeVideo}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
