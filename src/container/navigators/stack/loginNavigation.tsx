import React, { FC } from 'react';

import { Platform } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import { Login, Register } from '../../screens';
import { EnterData, SetPassword } from '../../screens/ForgotPassword';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';
import { HeaderLeft } from '../../../components/Header/headerLeft';

export type RootStackParamList = {
  Login: undefined;
  Register: undefined;
  ForgotPassword: undefined;
  SetPassword: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

export const LoginNavigation: FC = () => {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={({ navigation }) => ({
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
        headerBackTitleVisible: false,
        headerLeft:
          Platform.OS !== 'ios'
            ? null
            : () => <HeaderLeft onPress={()=>navigation.goBack()} />,
        headerTitleStyle: {
          fontSize: textSizes.h8,
          color: 'white',
        },
        headerTitleAlign: 'left',
        headerTitleContainerStyle: {
          left: Platform.OS !== 'ios' ? normalize(18) : normalize(45),
        },
      })}>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{
          title: 'Register',
        }}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={EnterData}
        options={{
          title: 'Forgot Password',
        }}
      />
      <Stack.Screen
        name="SetPassword"
        component={SetPassword}
        options={{
          title: 'Set Password',
        }}
      />
    </Stack.Navigator>
  );
};
