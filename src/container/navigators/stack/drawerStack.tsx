import React, {FC} from 'react';

import {SessionStack} from './sessionStack';
import {createStackNavigator} from '@react-navigation/stack';
import {Platform} from 'react-native';
import Toast from 'react-native-simple-toast';

import TabNavigator from '../TabNavigator';
import {InterviewQuestions} from '../../screens/InterviewQuestions';
import {Setting} from '../../screens/Settings';
import {TouchItem} from '../../../components';
import {Colors} from '../../../assets/styles/color';
import {Memes} from '../../screens/Memes';
import {SelectImage} from '../../screens/Memes/selectImage';
import {PostMeme} from '../../screens/Memes/postMeme';
import {TabQuestions} from '../../screens/InterviewQuestions/tabQuestions';
import {QuestionList} from '../../screens/InterviewQuestions/Questions/questionList';
import {Icon} from '../../../components/Icon';
import {normalize, textSizes} from '../../../lib/globals';
import {ProfileStack} from './profileStack';
import {HeaderLeft} from '../../../components/Header/headerLeft';
import {BookMarkStack} from './bookMarkStack';
import {EmployeeMap} from '../../screens/Maps';
import {AddDoubt} from '../../screens/QuestionAnswer/addDoubt';
import {BitMojiMap} from '../../screens/Maps/bitmojiMaps';
import {AddQuestions} from '../../screens/InterviewQuestions/AddQuestions';
import {Poll} from '../../screens/Poll';
import {CreatePoll} from '../../screens/Poll/createPoll';
import {CreateCommunity} from '../../screens/Community/CreateCommunity';
import {CustomizeBitmoji} from '../../screens/Maps/CustomizeBitmoji';
import {PreviewBitmoji} from '../../screens/Maps/preview';

export type RootStackParamList = {
  Home: undefined;
  Profile: undefined;
  Bookmark: undefined;
  InterviewQuestions: undefined;
  SessionLinks: undefined;
  Memes: undefined;
  Setting: undefined;
  SelectImage: undefined;
  PostMeme: undefined;
  TabQuestions: { title: string };
  Map: undefined;
  AddDoubts: undefined;
  QuestionList: { clientName: string };
  AddQuestions: undefined;
  Poll: undefined;
  CreatePoll: undefined;
  CreateCommunity: undefined;
  PreviewBitmoji: undefined;
  CustomizaBitmoji: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

export const DrawerStack: FC = () => {
  const HeaderRight = (navigation, param, screen) => {
    return (
      <TouchItem
        onPress={() => {
          if (screen == 'SelectImage') {
            if (param?.imageUri == undefined) {
              Toast.show('Please Select An Image First', Toast.SHORT, [
                'UIAlertController',
              ]);
            } else {
              navigation.navigate('PostMeme', {imageUri: param.imageUri});
            }
          } else if (screen == 'AddDoubt') {
            param?.ref?.current?.open();
            // console.log(param);
          }
        }}
        style={{right: normalize(10)}}>
        <Icon
          type="Feather"
          name="check"
          size={normalize(22)}
          color={'white'}
        />
      </TouchItem>
    );
  };

  return (
    <Stack.Navigator
      screenOptions={({navigation}) => ({
        headerStyle: {
          backgroundColor: Colors.headerBackground,
          elevation: 0,
          shadowOpacity: 0,
        },
        headerLeft:
          Platform.OS !== 'ios'
            ? null
            : () => <HeaderLeft onPress={() => navigation.goBack()} />,
        headerTitleStyle: {
          fontSize: textSizes.h8,
          color: Colors.white,
        },
        headerTitleAlign: 'left',
        headerTitleContainerStyle: {
          left: Platform.OS !== 'ios' ? normalize(18) : normalize(45),
        },
      })}>
      <Stack.Screen
        name="Home"
        component={TabNavigator}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Bookmark"
        component={BookMarkStack}
        options={{
          // title: 'Bookmarks',
          // headerBackTitleVisible: false,
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="InterviewQuestions"
        component={InterviewQuestions}
        options={{
          title: 'Interview Questions',
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="SessionLinks"
        component={SessionStack}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Memes"
        component={Memes}
        options={{
          title: 'Memes',
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Map"
        component={BitMojiMap}
        options={{
          title: 'NeoSoftTees Around Me',
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="CustomizaBitmoji"
        component={CustomizeBitmoji}
        options={{
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="PreviewBitmoji"
        component={PreviewBitmoji}
        options={{
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="Setting"
        component={Setting}
        options={{
          title: 'Settings',
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="TabQuestions"
        component={TabQuestions}
        options={({navigation, route}) => ({
          title: `${route.params.title} Questions`,
          headerBackTitleVisible: false,
        })}
      />
      <Stack.Screen
        name="QuestionList"
        component={QuestionList}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SelectImage"
        component={SelectImage}
        options={({navigation, route}) => ({
          title: 'Upload Meme',
          headerRight: () =>
            HeaderRight(navigation, route.params, 'SelectImage'),
        })}
      />

      <Stack.Screen
        name="PostMeme"
        component={PostMeme}
        options={{
          title: 'Post Meme',
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="AddDoubts"
        component={AddDoubt}
        options={({navigation, route}) => ({
          title: 'Add Doubt',
          headerBackTitleVisible: false,
          headerRight: () => HeaderRight(navigation, route.params, 'AddDoubt'),
        })}
      />
      <Stack.Screen
        name="AddQuestions"
        component={AddQuestions}
        options={({navigation, route}) => ({
          title: 'Add Interview Questions',
          headerBackTitleVisible: false,
        })}
      />

      <Stack.Screen
        name="Poll"
        component={Poll}
        options={({navigation, route}) => ({
          title: 'Poll',
          headerBackTitleVisible: false,
        })}
      />

      <Stack.Screen
        name="CreatePoll"
        component={CreatePoll}
        options={({navigation, route}) => ({
          title: 'Create Poll',
          headerBackTitleVisible: false,
        })}
      />

      <Stack.Screen
        name="CreateCommunity"
        component={CreateCommunity}
        options={({navigation, route}) => ({
          title: 'Create Community',
          headerBackTitleVisible: false,
        })}
      />
    </Stack.Navigator>
  );
};
