import React, { FC } from 'react';

import { Platform } from 'react-native';
import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';
import { Profile } from '../../screens';
import { ProfileQuestions } from '../../screens/Profile/profileQuestions';
import { ProfileBlogs } from '../../screens/Profile/profileBlogs';
import { ProfileAnswers } from '../../screens/Profile/profileAnswers';
import { ProfileSessionLinks } from '../../screens/Profile/profileSessionLinks';
import { ProfileMemes } from '../../screens/Profile/profileMemes';
import { EditProfile } from '../../screens/Profile/editProfile';
import { HeaderLeft } from '../../../components/Header/headerLeft';
import { ProfileInterviewQuestions } from '../../screens/Profile/profileInterviewQuestions';

export type RootProfileStackParamList = {
    ProfileMain: undefined;
    ProfileQuestions: undefined;
    ProfileBlogs: undefined;
    ProfileInterviewQuestions: undefined;
    ProfileAnswers: undefined;
    ProfileSessionLinks: undefined;
    ProfileMeme: undefined;
    ProfileEdit: undefined;
};

const Stack = createStackNavigator<RootProfileStackParamList>();

type CommunityScreenNavigationProp = StackNavigationProp<
    RootProfileStackParamList,
    'ProfileMain'
>;
type CommunityScreenRouteProp = RouteProp<
    RootProfileStackParamList,
    'ProfileMain'
>;

type Props = {
    navigation: CommunityScreenNavigationProp;
    route: CommunityScreenRouteProp;
};

export const ProfileStack: FC<Props> = ({ navigation, route }) => {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
                headerBackTitleVisible: false,
                headerLeft:
                    Platform.OS !== 'ios'
                        ? null
                        : () => (
                            <HeaderLeft
                                onPress={() => navigation.navigate('ProfileMain')}
                            />
                        ),
                headerTitleStyle: {
                    fontSize: textSizes.h8,
                    color: 'white',
                },
                headerTitleAlign: 'left',
                headerTitleContainerStyle: {
                    left: Platform.OS !== 'ios' ? normalize(18) : normalize(45),
                },
            })}>
            <Stack.Screen
                name="ProfileMain"
                component={Profile}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ProfileQuestions"
                component={ProfileQuestions}
                options={{
                    title: 'User Questions',
                }}
            />
            <Stack.Screen
                name="ProfileBlogs"
                component={ProfileBlogs}
                options={{
                    title: 'User Blogs',
                }}
            />
            <Stack.Screen
                name="ProfileInterviewQuestions"
                component={ProfileInterviewQuestions}
                options={{
                    title: 'User Interview Questions',
                }}
            />
            <Stack.Screen
                name="ProfileAnswers"
                component={ProfileAnswers}
                options={{
                    title: 'User Answers',
                }}
            />
            <Stack.Screen
                name="ProfileSessionLinks"
                component={ProfileSessionLinks}
                options={{
                    title: 'User Session Links',
                }}
            />
            <Stack.Screen
                name="ProfileMeme"
                component={ProfileMemes}
                options={{
                    title: 'User Meme',
                }}
            />
            <Stack.Screen
                name="ProfileEdit"
                component={EditProfile}
                options={{
                    title: 'Edit Profile',
                }}
            />
        </Stack.Navigator>
    );
};
