import React, { FC } from 'react';

import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';

import { Notification } from '../../screens/Notification';
import { Header } from '../../../components/Header';
import { Colors } from '../../../assets/styles/color';

export type RootStackParamList = {
    Notification: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type NotificationScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Notification'
>;

type Props = {
    navigation: NotificationScreenNavigationProp;
};

export const NotificationStack: FC<Props> = ({ navigation }) => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
            }}>
            <Stack.Screen
                name="Notification"
                component={Notification}
                options={{
                    title: 'Home',
                    headerTitle: () => (
                        <Header navigation={navigation} title="Notification" />
                    ),
                }}
            />
        </Stack.Navigator>
    );
};
