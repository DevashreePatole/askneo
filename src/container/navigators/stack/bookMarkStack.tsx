import React, {FC} from 'react';

import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';

import {Bookmark} from '../../screens';
import {Colors} from '../../../assets/styles/color';
import {Platform} from 'react-native';
import {HeaderLeft} from '../../../components/Header/headerLeft';
import {normalize, textSizes} from '../../../lib/globals';
import {ReadBlog} from '../../screens/Blogs/readBlog';
import { YouTubeVideo } from '../../screens/SessionLinks/YoutubeVideo';

export type BookMarkStackParamList = {
  BookMarks: undefined;
  ReadBlog: undefined;
  Video:undefined;
};

const Stack = createStackNavigator<BookMarkStackParamList>();

type NotificationScreenNavigationProp = StackNavigationProp<
BookMarkStackParamList,
  'BookMarks'
>;

type Props = {
  navigation: NotificationScreenNavigationProp;
};

export const BookMarkStack: FC<Props> = ({navigation}) => {
  return (
    <Stack.Navigator
      initialRouteName="BookMarks"
      screenOptions={({navigation}) => ({
        headerStyle: {
          backgroundColor: Colors.headerBackground,
        },
        headerBackTitleVisible: false,
        headerLeft:
          Platform.OS !== 'ios'
            ? null
            : () => <HeaderLeft onPress={() => navigation.goBack()} />,
        headerTitleStyle: {
          fontSize: textSizes.h8,
          color: 'white',
        },
        headerTitleAlign: 'left',
        headerTitleContainerStyle: {
          left: Platform.OS !== 'ios' ? normalize(18) : normalize(45),
        },
      })}>
      <Stack.Screen
        name="BookMarks"
        component={Bookmark}
        options={{
          title: 'BookMark',
        }}
      />
      <Stack.Screen
        name="ReadBlog"
        component={ReadBlog}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Video"
        component={YouTubeVideo}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
