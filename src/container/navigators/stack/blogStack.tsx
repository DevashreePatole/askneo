import React, { FC } from 'react';

import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';
import { RouteProp } from '@react-navigation/native';

import { Blog } from '../../screens/Blogs';
import { Header } from '../../../components/Header';
import { Colors } from '../../../assets/styles/color';
import { ReadBlog } from '../../screens/Blogs/readBlog';

export type RootStackParamList = {
    Blogs: undefined;
    ReadBlog: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type BlogScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Blogs'
>;
type BlogScreenRouteProp = RouteProp<RootStackParamList, 'Blogs'>;

type Props = {
    navigation: BlogScreenNavigationProp;
    route: BlogScreenRouteProp;
};

export const BlogStack: FC<Props> = ({ navigation, route }) => {
    React.useLayoutEffect(() => {
        if (getFocusedRouteNameFromRoute(route) == 'ReadBlog') {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [getFocusedRouteNameFromRoute(route)]);

    return (
        <Stack.Navigator
            initialRouteName="Blogs"
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
            }}>
            <Stack.Screen
                name="Blogs"
                component={Blog}
                options={{
                    title: 'Blog',
                    headerTitle: () => <Header navigation={navigation} title="Blog" />,
                }}
            />
            <Stack.Screen
                name="ReadBlog"
                component={ReadBlog}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
