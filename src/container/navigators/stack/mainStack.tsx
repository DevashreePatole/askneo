import React, { FC } from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import { LoginNavigation } from './loginNavigation';
import Drawer from '../Drawer';

export type AuthStackParamList = {
    Auth:undefined;
    Dashboard:undefined;
}
const Stack = createStackNavigator<AuthStackParamList>();

export const MainStack: FC = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                component={LoginNavigation}
                name="Auth"
                options={{ headerShown: false }}
            />
            <Stack.Screen
                component={Drawer}
                name="Dashboard"
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};
