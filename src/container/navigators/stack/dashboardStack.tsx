import React, { FC } from 'react';

import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

import { Dashboard } from '../../screens';
import { Header } from '../../../components/Header';
import { Colors } from '../../../assets/styles/color';

export type RootStackParamList = {
    Home: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type DashboardScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Home'
>;
type DashboardScreenRouteProp = RouteProp<RootStackParamList, 'Home'>;

type Props = {
    navigation: DashboardScreenNavigationProp;
};

export const DashboardStack: FC<Props> = ({ navigation }) => {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
            }}>
            <Stack.Screen
                name="Home"
                component={Dashboard}
                options={{
                    title: 'Home',
                    headerTitle: () => <Header navigation={navigation} title="Home" />,
                }}
            />
        </Stack.Navigator>
    );
};
