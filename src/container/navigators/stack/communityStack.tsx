import React, { FC } from 'react';

import { Platform } from 'react-native';
import {
    createStackNavigator,
    StackNavigationProp,
} from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/core';
import { RouteProp } from '@react-navigation/native';

import { Community } from '../../screens/Community';
import { Header } from '../../../components/Header';
import { Colors } from '../../../assets/styles/color';
import { CommunityDetail } from '../../screens/Community/communityDetail';
import { TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { HeaderLeft } from '../../../components/Header/headerLeft';

export type RootStackParamList = {
    Community: undefined;
    CommunityDetail: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

type CommunityScreenNavigationProp = StackNavigationProp<
    RootStackParamList,
    'Community'
>;
type CommunityScreenRouteProp = RouteProp<RootStackParamList, 'Community'>;

type Props = {
    navigation: CommunityScreenNavigationProp;
    route: CommunityScreenRouteProp;
};

export const CommunityStack: FC<Props> = ({ navigation, route }) => {
    React.useLayoutEffect(() => {
        if (getFocusedRouteNameFromRoute(route) == 'CommunityDetail') {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [getFocusedRouteNameFromRoute(route)]);

    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: Colors.headerBackground,
                },
            }}>
            <Stack.Screen
                name="Community"
                component={Community}
                options={{
                    title: 'Home',
                    headerTitle: () => (
                        <Header navigation={navigation} title="Community" />
                    ),
                }}
            />
            <Stack.Screen
                name="CommunityDetail"
                component={CommunityDetail}
                options={({ navigation }) => ({
                    headerShown:false
                })}
            />
        </Stack.Navigator>
    );
};
