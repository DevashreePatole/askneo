import React, { FC } from 'react'

import {View,Text} from 'react-native'
import {createStackNavigator} from '@react-navigation/stack'
import { InterviewQuestions } from '../../screens';

const Stack = createStackNavigator();

export const InterviewQuestionStack:FC = () =>{
    return(
        <Stack.Navigator>
           <Stack.Screen
        name="InterviewQuestions"
        component={InterviewQuestions}
        options={{
          title: 'Interview Questions',
          headerBackTitleVisible: false,
        }}
      />
        </Stack.Navigator>
    )
}