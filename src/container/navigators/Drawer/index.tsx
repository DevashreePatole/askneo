import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';

import { DrawerComponent } from './drawerComponent';
import { DrawerStack } from '../stack/drawerStack';
import { Colors } from '../../../assets/styles/color';

const Drawer = createDrawerNavigator();

export default () => {
  return (
    <Drawer.Navigator
      initialRouteName="Dashboard"
      drawerStyle={{backgroundColor:Colors.transparent}}
      drawerContent={(props) => <DrawerComponent {...props} />}
      >
      <Drawer.Screen name="Dashboard" component={DrawerStack} />
    </Drawer.Navigator>
  );
};
