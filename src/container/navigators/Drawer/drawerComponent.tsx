import React, { useRef } from 'react';

import { View, StyleSheet, ScrollView } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import styled from 'styled-components/native';
import { prop } from 'styled-tools';

import AlertModal from '../../../components/AlertModal';
import { Container, Text, TouchItem } from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

export const DrawerComponent = ({ ...props }) => {
    const routes = [
        {
            icon: 'home',
            title: 'Home',
            route: 'Home',
        },
        {
            icon: 'bookmark',
            title: 'Bookmarks',
            route: 'Bookmark',
        },
        {
            icon: 'question-circle',
            title: 'Interview Questions',
            route: 'InterviewQuestions',
        },
        {
            icon: 'link',
            title: 'Session Links',
            route: 'SessionLinks',
        },
        {
            icon: 'smile-wink',
            title: 'Memes',
            route: 'Memes',
        },
        {
            icon: 'poll',
            title: 'Poll',
            route: 'Poll'
        },
        {
            icon: 'map-marker-alt',
            title: 'NeoSoftTees Around Me',
            route: 'Map',
        },
        {
            icon: 'cog',
            title: 'Settings',
            route: 'Setting',
        },
    ];

    let alertRef = useRef();

    const handleLogout = () => {
        const warningModal = () =>
            alertRef.current?.setOptionAndOpen(
                'sure',
                'Are you sure you want to Logout',
                () => props.navigation.pop(),
            );
        warningModal();
    };

    const _DisplayRoutes = (item, index) => {
        return (
            <ItemContainer key={index}>
                <TouchItem onPress={() => props.navigation.navigate(item.route)}>
                    <Row>
                        <IconContainer>
                            <RouteIcon
                                type="FontAwesome5"
                                name={item.icon}
                                color={'black'}
                                size={normalize(18)}
                            />
                        </IconContainer>
                        <View style={{ marginLeft: normalize(4) }}>
                            <Text style={{ color: 'black' }}>{item.title}</Text>
                        </View>
                    </Row>
                </TouchItem>
            </ItemContainer>
        );
    };

    return (
        <ScreenContainer {...props}>
            <BackgroundContainer>
                <ImageComponent
                    source={require('../../../assets/images/myMonkey.gif')}
                    style={styles.backImage}
                />
            </BackgroundContainer>
            <DrawerContentScrollView contentContainerStyle={{ flex: 1 }}>
                <UserDetail>
                    <ImgContainer>
                        <TouchItem onPress={() => props.navigation.navigate('Profile')}>
                            <ProfileImg
                                size={normalize(62)}
                                source={{
                                    uri:
                                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
                                }}
                                borderRadius={normalize(62)}
                            />
                        </TouchItem>
                    </ImgContainer>
                    <TouchItem onPress={() => props.navigation.navigate('Profile')}>
                        <UserText>
                            <Text
                                size={textSizes.h8}
                                fontWeight="500"
                                style={{ color: Colors.black }}>
                                Vartika Verma
                            </Text>
                            <RouteIcon
                                type="FontAwesome5"
                                name={'chevron-right'}
                                color={'black'}
                                size={normalize(16)}
                                style={{ marginRight: normalize(14) }}
                            />
                        </UserText>
                    </TouchItem>
                </UserDetail>
                <View style={{ paddingBottom: normalize(170) }}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {routes.map((item, index) => {
                            return _DisplayRoutes(item, index);
                        })}
                    </ScrollView>
                </View>
                <LogoutContainer>
                    <TouchItem onPress={() => handleLogout()}>
                        <Row style={{ width: '100%', height: normalize(35) }}>
                            <IconContainer>
                                <RouteIcon
                                    type="FontAwesome5"
                                    name={'sign-out-alt'}
                                    color={'black'}
                                    size={normalize(18)}
                                />
                            </IconContainer>
                            <View style={{ marginLeft: normalize(5) }}>
                                <Text style={{ color: 'black' }}>Logout</Text>
                            </View>
                        </Row>
                    </TouchItem>
                </LogoutContainer>
            </DrawerContentScrollView>
            <AlertModal ref={alertRef} />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  background-color: ${Colors.primaryColor};
  border-top-right-radius: ${normalize(23)}px;
  border-bottom-right-radius: ${normalize(23)}px;
  overflow: hidden;
`;

const BackgroundContainer = styled.View`
  position: absolute;
  z-index: -1;
  width: 110%;
  height: 110%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: transparent;
`;

const UserDetail = styled.View`
  height: ${normalize(110)}px;
  padding-left: ${normalize(10)}px;
  width: 100%;
  border-bottom-width: 0.3px;
  border-bottom-color: gray;
  margin-bottom: ${normalize(20)}px;
`;

const UserText = styled.View`
  flex-direction: row;
  margin-top: ${normalize(12)}px;
  justify-content: space-between;
`;

const ProfileImg = styled(ImageComponent) <{ size: number }>`
  width: ${prop('size')}px;
  height: ${prop('size')}px;
`;

const ImgContainer = styled.View`
  width: ${normalize(65)}px;
  height: ${normalize(65)}px;
`;

const ItemContainer = styled.View`
  height: ${normalize(45)}px;
  margin-bottom: 2px;
`;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const IconContainer = styled.View`
  width: ${normalize(35)}px;
  flex-direction: row;
  justify-content: center;
`;

const RouteIcon = styled(Icon)`
  opacity: 0.8;
`;

const LogoutContainer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: ${normalize(35)}px;
`;

const styles = StyleSheet.create({
    backImage: {
        width: normalize(400),
        height: normalize(400),
        opacity: 0.1,
        position: 'relative',
        left: -5,
    },
});
