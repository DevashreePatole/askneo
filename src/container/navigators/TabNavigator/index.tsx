import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { DashboardStack } from '../stack/dashboardStack';
import { Colors } from '../../../assets/styles/color';
import { BlogStack } from '../stack/blogStack';
import { QuestionAnswerStack } from '../stack/questionAnswerStack';
import { CommunityStack } from '../stack/communityStack';
import { NotificationStack } from '../stack/notificationStack';
import { Text } from '../../../components';
import { Icon } from '../../../components/Icon';

const Tab = createBottomTabNavigator();

export default () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: Colors.primaryColor,
        inactiveBackgroundColor: Colors.black,
        style: {
          backgroundColor: Colors.black,
        },
      }}
      screenOptions={({ route }) => ({
        tabBarLabel: ({ focused, color }) => {
          let label;
          switch (route.name) {
            case 'Home':
              return (label = focused ? (
                <Text size={10} style={{ color }}>
                  Home
                </Text>
              ) : null);
            case 'Blogs':
              return (label = focused ? (
                <Text size={10} style={{ color }}>
                  Blogs
                </Text>
              ) : null);
            case 'QA':
              return (label = focused ? (
                <Text size={10} style={{ color }}>
                  QA
                </Text>
              ) : null);
            case 'Community':
              return (label = focused ? (
                <Text size={10} style={{ color }}>
                  Community
                </Text>
              ) : null);
            case 'Notification':
              return (label = focused ? (
                <Text size={10} style={{ color }}>
                  Notification
                </Text>
              ) : null);
          }
          return label;
        },
      })}>
      <Tab.Screen
        name="Home"
        component={DashboardStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon type="FontAwesome5" name="home" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Blogs"
        component={BlogStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon type="FontAwesome5" name="blog" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="QA"
        component={QuestionAnswerStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon
              type="FontAwesome5"
              name="chalkboard-teacher"
              size={20}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Community"
        component={CommunityStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon type="FontAwesome5" name="users" size={20} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Notification"
        component={NotificationStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon type="FontAwesome5" name="bell" size={20} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
