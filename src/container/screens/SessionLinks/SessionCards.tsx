import React, { FC, useEffect, useRef, useState } from 'react';

import {
  View,
  StyleSheet,
  Animated,
  Image,
  Platform,
  PermissionsAndroid,
  Alert,
  LogBox,
} from 'react-native';
import styled from 'styled-components/native';
import { Colors } from '../../../assets/styles/color';
import { Thumbnail } from 'react-native-thumbnail-video';
import moment, { isDuration } from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import Toast from 'react-native-simple-toast';
import FileViewer from 'react-native-file-viewer';
import { StackNavigationProp } from '@react-navigation/stack';

import { Text, TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { AnimatedIcon, AnimateIcon } from '../../../components/AnimatedIcon';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';
import { RootStackParamList } from '../../navigators/stack/drawerStack';

interface DataProps {
  profileImage: string;
  employeeName: string;
  technology: string;
  title: string;
  videoLink: string;
  document: string;
  ppt: string;
  postedTime: string;
  likes: string;
}

interface CardProps {
  data: DataProps;
  navigation: StackNavigationProp<RootStackParamList, 'SessionLinks'>;
}

export const SessionCard: FC<CardProps> = ({ data, navigation, ...props }) => {
  const [visible, setVisible] = useState(false);
  const [disabled, setDisable] = useState(false);
  const [like, setLike] = useState(parseInt(data.likes));
  const [downloadFile, setDownload] = useState(false);
  const [downloadPPT, setDownloadPPT] = useState(false);
  const currentValue = new Animated.Value(1);
  var pptArray = data.ppt.split('/');
  var pptName = pptArray[pptArray.length - 1];

  var documentArray = data.document.split('/');
  var documentName = documentArray[documentArray.length - 1];

  useEffect(() => {
    if (disabled == true) {
      AnimateIcon(currentValue, () => {
        setVisible(false);
      });
    }
  }, [disabled]);

  useEffect(() => {
    LogBox.ignoreLogs([
      'Unsafe legacy lifecycles will not be called for components using new component APIs.',
    ]);
  }, []);
  const handleLikeClick = () => {
    if (!disabled) {
      setLike((prev) => prev + 1);
      setDisable(true);
      setVisible(true);
    } else {
      setLike((prev) => prev - 1);
      setDisable(false);
      setVisible(false);
    }
  };

  const download = async (filePath, fileName, type) => {
    if (Platform.OS == 'ios') {
      actualDownload(filePath, fileName, type);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          actualDownload(filePath, fileName, type);
        } else {
          Alert.alert(
            'Permission Denied!',
            'You need to give storage permission to download the file',
          );
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };
  // const actualDownload = () =>{
  //   console.log('download accept')
  // }

  const actualDownload = (filePath, fileName, type) => {
    if (downloadFile == false || downloadPPT == false) {
      const dateTime = new Date();
      const { dirs } = RNFetchBlob.fs;
      let downloadDest = `${RNFS.ExternalStorageDirectoryPath}/${fileName}`;
      const dirToSave =
        Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
      let exist = RNFS.exists(`${dirToSave}/${fileName}`);
      console.log('------exist-----', exist);
      const configfb = {
        fileCache: true,
        addAndroidDownloads: {
          useDownloadManager: true,
          notification: true,
          mediaScannable: true,
          path: `${dirToSave}/${fileName}`,
          title: fileName,
        },

        //path:`${dirToSave}/${fileName}`,
      };
      const configOptions = Platform.select({
        ios: {
          fileCache: configfb.fileCache,
          title: fileName,
          path: `${dirToSave}/${fileName}`,
          //path: configfb.path,
        },
        android: configfb,
      });
      console.log('The file saved to 23233', configfb);

      RNFetchBlob.config(configOptions)
        .fetch('GET', `${filePath}`, {})
        .then((res) => {
          console.log('------files res------', res);
          if (Platform.OS === 'ios') {
            RNFetchBlob.fs.writeFile(
              configfb.addAndroidDownloads.path,
              res.data,
              'base64',
            );
            RNFetchBlob.ios.previewDocument(configfb.addAndroidDownloads.path);
          }
          Toast.show('File Downloaded successfuly', Toast.SHORT, [
            'UIAlertController',
          ]);
          if (type == 'document') {
            setDownload(true);
          } else {
            setDownloadPPT(true);
          }

          //console.log('The file saved to ', res.path());
        })
        .catch((e) => {
          console.log('The file saved to ERROR', e.message);
        });
    } else {
      Toast.show('File Already Downloaded', Toast.SHORT, ['UIAlertController']);
    }
  };
  const previewFile = (filePath) => {
    FileViewer.open(filePath)
      .then((res) => {
        console.log('res', res);
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
  const uploadedDate = moment(data.postedTime).format('ll');
  const uploadedTime = moment(data.postedTime).format('h:mm a');
  return (
    <CardContainer>
      <HeaderContainer>
        <ImageContainer
          source={{
            uri: `${data.profileImage}`,
          }}
        />
        <ProfileContainer>
          <Text size={textSizes.h10} fontWeight="500" style={styles.technology}>
            {data.employeeName}
          </Text>
          <Text size={textSizes.h12} style={styles.technology} fontWeight="300">
            {uploadedDate} at {uploadedTime}
          </Text>
        </ProfileContainer>
      </HeaderContainer>
      <TitleContainer>
        <Text
          size={textSizes.h7}
          fontWeight="500"
          style={{ alignSelf: 'flex-start' }}>
          {data.title}
        </Text>
      </TitleContainer>

      <Thumbnail
        url="https://www.youtube.com/watch?v=jt-thh8y-RE"
        onPress={() => {
          navigation.navigate('Video');
        }}
        imageWidth={normalize(310)}
        containerStyle={styles.imageStyle}
        iconStyle={{ height: 30, width: 30, tintColor: Colors.primaryColor }}
      />

      {data.document != '' ? (
        <DocumentContainer>
          <TouchItem
            onPress={() => {
              previewFile(data.document);
            }}>
            <View style={{ flexDirection: 'row' }}>
              <ImageComponent
                source={{
                  uri:
                    'https://cdn.iconscout.com/icon/free/png-512/docx-file-14-504256.png',
                }}
                style={styles.iconStyle}
              />
              <Text style={{ paddingLeft: normalize(10) }}>{documentName}</Text>
            </View>
          </TouchItem>
          <Icon
            type="MaterialIcons"
            name={downloadFile ? 'file-download-done' : 'file-download'}
            size={normalize(20)}
            color={Colors.black}
            onPress={() => {
              download(data.document, documentName, 'document');
            }}
          />
        </DocumentContainer>
      ) : null}
      {data.ppt != '' ? (
        <DocumentContainer>
          <TouchItem
            onPress={() => {
              previewFile(data.ppt);
            }}>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={{
                  uri:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoVH8Xs_xRttz2l5IKLJ83vkvuTJ4gSJ9u604-8EpVharhjsaT1je06iHNLzD-SzWbPWg&usqp=CAU',
                }}
                style={styles.iconStyle}
              />
              <Text style={{ paddingLeft: normalize(10) }}>{pptName}</Text>
            </View>
          </TouchItem>
          <Icon
            type="MaterialIcons"
            name={downloadPPT ? 'file-download-done' : 'file-download'}
            size={normalize(20)}
            color={Colors.black}
            onPress={() => {
              download(data.ppt, pptName, 'ppt');
            }}
          />
        </DocumentContainer>
      ) : null}
      <CardFooter>
        <View style={{ flexDirection: 'row' }}>
          <Icon
            type="FontAwesome"
            name={disabled ? 'heart' : 'heart-o'}
            size={normalize(25)}
            color={disabled ? Colors.primaryColor : Colors.darkGray}
            onPress={handleLikeClick}
          />
          <Text style={styles.likeStyle}>{like}</Text>
        </View>
        <Icon
          type="FontAwesome"
          name="comments"
          size={normalize(25)}
          color={Colors.darkGray}
          onPress={() => { }}
        />
        <Icon
          type="FontAwesome"
          name="send"
          size={normalize(25)}
          color={Colors.darkGray}
          onPress={() => { }}
        />
      </CardFooter>
      {visible && (
        <AnimatedIcon
          name="heart"
          size={55}
          color={Colors.primaryColor}
          currentValue={currentValue}
        />
      )}
    </CardContainer>
  );
};

const CardContainer = styled.View`
  margin-top: ${normalize(6)}px;
  margin-bottom: ${normalize(5)}px;
  border-width: 1px;
  padding-top: ${normalize(10)}px;
  padding-bottom: ${normalize(10)}px;
  border-color: ${Colors.lightGray};
  background-color: ${Colors.white};
  border-radius: ${normalize(8)}px;
`;
const HeaderContainer = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  padding-left: ${normalize(15)}px;
`;
const ImageContainer = styled(ImageComponent)`
  width: ${normalize(37)}px;
  height: ${normalize(37)}px;
  border-radius: ${normalize(37)}px;
`;
const ProfileContainer = styled.View`
  padding-left: ${normalize(8)}px;
  padding-top: ${normalize(3)}px;
`;
const CardFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${normalize(5)}px;
  padding: ${normalize(5)}px;
  padding-left: ${normalize(20)}px;
  padding-right: ${normalize(20)}px;
`;
const DocumentContainer = styled.View`
margin-left:${normalize(20)}px;
margin-right:${normalize(20)}px;
margin-bottom:${normalize(10)}px;
border-width:${normalize(1)}px;
border-radius:${normalize(5)}px;
border-color:${Colors.headerBackground}
height:${normalize(50)}px;
padding:${normalize(5)}px;
padding-left:${normalize(10)}px;
padding-right:${normalize(10)}px;
flex-direction:row
justify-content:space-between;
align-items:center
`;
const TitleContainer = styled.View`
margin:${normalize(5)}px
margin-left:${normalize(20)}px
margin-top:${normalize(10)}px
padding-top:${normalize(10)}px
`;

const styles = StyleSheet.create({
  technology: {
    marginTop: normalize(5),
    alignSelf: 'flex-start',
    left: 0,
  },
  imageStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: normalize(15),
    marginBottom: normalize(20),
  },
  likeStyle: {
    paddingTop: normalize(5),
    paddingLeft: normalize(5),
    color: Colors.darkGray,
  },
  iconStyle: {
    width: normalize(40),
    height: normalize(40),
  },
});
