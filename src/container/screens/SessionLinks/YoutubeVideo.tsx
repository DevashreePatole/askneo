import React, { FC, useEffect, useRef, useState } from 'react';

import {
  View,
  Platform,
  BackHandler,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import YouTube from 'react-native-youtube';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../../navigators/stack/sessionStack';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'Video'>;
}

export const YouTubeVideo: FC<Props> = ({ navigation }) => {
  const youtubePlayerRef = useRef<YouTube>(null);
  const [duration, setDuration] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [loading, setLoading] = useState(false);
  const [state, setState] = useState({
    height: 200,
    fullscreen: true,
  });

  var backhandler;

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  useEffect(() => {
    backhandler = BackHandler.addEventListener('hardwareBackPress', () => {
      navigation.goBack();
      return null;
    });
    return () => {
      backhandler.remove();
    };
  }, []);

  const handleOnReady = () => {
    setState((val) => {
      return { ...val, fullscreen: true };
    });
    setTimeout(
      () =>
        setState((val) => {
          return { ...val, height: 281 };
        }),
      500,
    );
  };

  const changeFull = (event) => {
    console.log('event:', event);
    if (Platform.OS === 'android') {
      if (event.state === 'playing') {
        //setState(val=>{return {...val,fullscreen:true}})
      } else if (event.state === 'paused' || event.state === 'ended') {
        //setState(val=>{return {...val,fullscreen:false}})
      } else {
        console.log('nothing');
      }
    }
  };

  return (
    <View style={{ flex: 1, zIndex: -1, backgroundColor: 'black' }}>
      {Platform.OS == 'ios' ? (
        <Icon
          name="arrow-back-ios"
          size={25}
          color="white"
          style={styles.iconStyle}
          onPress={() => navigation.goBack()}
        />
      ) : null}
      {Platform.OS == 'ios' && loading && (
        <ActivityIndicator
          size={'large'}
          color={'black'}
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        />
      )}
      <YouTube
        ref={youtubePlayerRef}
        apiKey="AIzaSyDowkY7NgUd1_QG-oWerDjd1Y1ezja05gg"
        videoId="jt-thh8y-RE"
        play={state.fullscreen}
        fullscreen={true}
        controls={1}
        rel={false}
        showFullscreenButton={false}
        onError={(e) => console.log(e.error)}
        onReady={(e) => handleOnReady()}
        onChangeState={(event) => changeFull(event)}
        onProgress={(e) => {
          setDuration(e.duration);
          setCurrentTime(e.currentTime);
        }}
        style={{ ...styles.videoStyle, height: state.height }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  videoStyle: {
    flex: 1,
    zIndex: 1,
    marginTop: Platform.OS == 'ios' ? 55 : 0,
    backgroundColor: 'black',
  },
  iconStyle: {
    position: 'absolute',
    top: 30,
    left: 20,
    zIndex: 1,
  },
});
