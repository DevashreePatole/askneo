import React, { FC } from 'react';

import { Platform, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Formik } from 'formik';
import * as yup from 'yup';

import { Container, Input } from '../../../components';
import Button from '../../../components/Button';
import { RootStackParamList } from '../../navigators/stack/loginNavigation';
import { normalize } from '../../../lib/globals';

interface FormProp {
  navigation: StackNavigationProp<RootStackParamList, 'SetPassword'>;
}

const validationSchema = yup.object({
  otp: yup.string().min(4).max(4).required(),
  password: yup
    .string()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .required()
    .matches(/^[a-zA-Z0-9_]*$/, 'Password can only contain alphanumeric.'),
  confirmPwd: yup
    .string()
    .required()
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});
export const SetPassword: FC<FormProp> = ({ navigation }) => {
  return (
    <ScreenContainer>
      <MainContainer>
        <Screen
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}>
          <FormContainer>
            <Formik
              initialValues={{
                otp: '',
                password: '',
                confirmPwd: '',
              }}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                navigation.popToTop();
              }}>
              {(props) => {
                return (
                  <View>
                    <Input
                      placeholder="Enter your email"
                      label="OTP"
                      keyboardType="number-pad"
                      value={props.values.otp}
                      onChangeText={props.handleChange('otp')}
                      onBlur={props.handleBlur('otp')}
                      errorText={props.touched.otp && props.errors.otp}
                    />
                    <Input
                      placeholder="*********"
                      label="New Password"
                      value={props.values.password}
                      onChangeText={props.handleChange('password')}
                      onBlur={props.handleBlur('password')}
                      errorText={
                        props.touched.password && props.errors.password
                      }
                    />
                    <Input
                      label="Confirm New Password"
                      placeholder="*********"
                      value={props.values.confirmPwd}
                      onChangeText={props.handleChange('confirmPwd')}
                      onBlur={props.handleBlur('confirmPwd')}
                      errorText={
                        props.touched.confirmPwd && props.errors.confirmPwd
                      }
                    />
                    <ResetButton
                      title="Reset"
                      fontSize={16}
                      onClick={props.handleSubmit}
                    />
                  </View>
                );
              }}
            </Formik>
          </FormContainer>
        </Screen>
      </MainContainer>
    </ScreenContainer>
  );
};
const ScreenContainer = styled.View`
  flex: 1;
  z-index: 1;
`;

const MainContainer = styled(Container)`
  flex-direction: row;
  padding: 0px;
  padding-top: ${normalize(10)}px;
  margin-top: ${Platform.OS == 'ios' ? normalize(20) : 0}px;
  padding-bottom: 0px;
  background-color: transparent;
`;

const ResetButton = styled(Button)`
  margin-top: ${normalize(30)}px;
  width: ${normalize(150)}px;
  height: ${normalize(40)}px;
  align-self: center;
`;

const FormContainer = styled.View`
  padding: ${normalize(35)}px;
  padding-top: 0px;
  padding-bottom: ${normalize(20)}px;
`;

const Screen = styled.ScrollView``;

const styles = StyleSheet.create({
  backButton: {
    alignSelf: 'flex-start',
    marginBottom: '15%',
  },
});
