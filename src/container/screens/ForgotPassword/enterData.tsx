import React, { FC } from 'react';

import { Platform, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Formik } from 'formik';
import * as yup from 'yup';

import { Container, Input, Text } from '../../../components';
import Button from '../../../components/Button';
import { RootStackParamList } from '../../navigators/stack/loginNavigation';
import { normalize, textSizes } from '../../../lib/globals';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'ForgotPassword'>;
}

const validationSchema = yup.object({
  email: yup.string().required().email(),
});

export const EnterData: FC<Props> = ({ navigation }) => {

  return (
    <ScreenContainer>
      <MainContainer>
        <FormContainer>
          <Formik
            initialValues={{
              email: '',
            }}
            validationSchema={validationSchema}
            onSubmit={(values) => {
              navigation.navigate('SetPassword');
            }}>
            {(props) => {
              return (
                <View>
                  <InstText size={textSizes.h10}>
                    Please enter the email you used at the time of registration
                    to get the password reset instructions.
                  </InstText>
                  <Input
                    value={props.values.email}
                    placeholder="Enter your email"
                    onChangeText={props.handleChange('email')}
                    onBlur={props.handleBlur('email')}
                    errorText={props.touched.email && props.errors.email}
                  />
                  <ResetButton
                    title="Next"
                    fontSize={textSizes.h9}
                    onClick={props.handleSubmit}
                  />
                </View>
              );
            }}
          </Formik>
        </FormContainer>
      </MainContainer>
    </ScreenContainer>
  );
};

const InstText = styled(Text)`
  margin-top: ${normalize(25)}px;
  text-align: center;
`;

const ScreenContainer = styled.View`
  flex: 1;
  z-index: 1;
`;

const MainContainer = styled(Container)`
  padding: 0px;
  padding-top: ${normalize(20)}px;
  margin-top: ${Platform.OS == 'ios' ? normalize(20) : 0}px;
  background-color: transparent;
`;

const ResetButton = styled(Button)`
  margin-top: ${normalize(30)}px;
  width: ${normalize(150)}px;
  height: ${normalize(40)}px;;
`;

const FormContainer = styled.View`
  padding: ${normalize(35)}px;
  flex-direction: column;
  align-items: center;
  padding-top: 0px;
  padding-bottom: ${normalize(20)}px;
`;

const styles = StyleSheet.create({
  backButton: {
    alignSelf: 'flex-start',
    marginBottom: '15%',
  },
});
