import React, { FC, useRef } from 'react';

import { View } from 'react-native';
import styled from 'styled-components/native';
import moment from 'moment';

import { Colors } from '../../../assets/styles/color';
import { Text, TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { Swipeable } from 'react-native-gesture-handler';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../navigators/stack/questionAnswerStack';

interface Question {
    id: string;
    title: string;
    author: string;
    tags: string[];
    answers: any[];
    postedTime: string | Date;
    likes: number;
    dislikes: number;
    authorImage: string;
    authorDes: string;
    views: string;
}

interface Prop {
    data: Question;
    index: number;
    handleLike: (index: any, type: any) => void;
    handleDislike: (index: any, type: any) => void;
    navigation: StackNavigationProp<RootStackParamList, 'QuestionAnswer'>;
    onPress?: () => void;
    swipable?: boolean;
    userQuestion?: boolean;
}

export const QuestionCard: FC<Prop> = ({
    data,
    navigation,
    onPress = () => { },
    swipable = true,
    userQuestion = false,
}) => {
    const swipeRef = useRef<Swipeable>(null);

    const leftAction = () => (
        <LeftSwipeCard>
            <Text size={textSizes.h9} fontWeight="500">
                Answer
      </Text>
            <Icon
                type="MaterialCommunityIcons"
                size={normalize(20)}
                name="reply-all-outline"
            />
        </LeftSwipeCard>
    );

    function numFormatter(num) {
        if (num > 999 && num <= 1000000) {
            return num / 1000 + 'K';
        } else if (num > 1000000) {
            return num / 1000000 + 'M';
        } else if (num <= 999) {
            return num;
        }
    }

    return (
        <Swipeable
            ref={swipeRef}
            renderLeftActions={swipable ? leftAction : undefined}
            onSwipeableLeftOpen={
                swipable
                    ? () => {
                        swipeRef.current?.close();
                        onPress();
                    }
                    : undefined
            }>
            <Container onPress={() => onPress()}>
                <Row>
                    {data.tags.map((tag, index) => {
                        return (
                            <Text
                                key={index}
                                style={{ marginRight: normalize(5), color: Colors.lightBlue }}
                                fontWeight="500"
                                size={textSizes.h11}>
                                #{tag}
                            </Text>
                        );
                    })}
                </Row>
                <View style={{ marginTop: normalize(15) }}>
                    <Text
                        align="flex-start"
                        size={textSizes.h8}
                        style={{ lineHeight: normalize(23) }}
                        fontWeight="500">
                        {data.title}
                    </Text>
                </View>
                <QuestionInfo>
                    <Text>{data.answers.length.toString()} answers</Text>
                    <Text>{numFormatter(data.views)} view</Text>
                </QuestionInfo>

                <Row style={{ marginTop: normalize(35) }}>
                    <ImageComponent
                        source={{ uri: data.authorImage }}
                        style={{
                            width: normalize(45),
                            height: normalize(45),
                            borderRadius: normalize(45),
                        }}
                    />
                    <AuthorInfo>
                        <Text align="flex-start" size={textSizes.h11} fontWeight="500">
                            {data.author}
                        </Text>
                        <Text align="flex-start" size={textSizes.h13}>
                            {data.authorDes}
                        </Text>
                    </AuthorInfo>
                </Row>
                <View style={{ alignSelf: 'flex-end' }}>
                    <Text size={textSizes.h12}>
                        {moment(data.postedTime).format('lll')}
                    </Text>
                </View>
                {userQuestion ? (
                    <TouchItem
                        onPress={() => { }}
                        style={{
                            position: 'absolute',
                            top: -normalize(5),
                            right: -normalize(5),
                            padding: normalize(2),
                        }}>
                        <Icon type="MaterialIcons" name="delete" size={normalize(22)} />
                    </TouchItem>
                ) : null}
            </Container>
        </Swipeable>
    );
};

const Container = styled(TouchItem)`
  width: 100%;
  background-color: ${Colors.white};
  border-radius: ${normalize(15)}px;
  margin-top: ${normalize(10)}px;
  margin-bottom: ${normalize(10)}px;
  padding-left: ${normalize(20)}px;
  padding-right: ${normalize(20)}px;
  padding-top: ${normalize(30)}px;
  padding-bottom: ${normalize(20)}px;
`;

const Row = styled.View`
  width: 100%;
  flex-direction: row;
`;

const QuestionInfo = styled(Row)`
  margin-top: ${normalize(22)}px;
  flex-direction: row;
  width: 50%;
  justify-content: space-between;
`;

const LeftSwipeCard = styled.View`
  width: ${normalize(60)}px;
  height: 100%;
  padding-top: ${normalize(30)}px;
  padding-bottom: ${normalize(30)}px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const AuthorInfo = styled.View`
  flex-direction: column;
  justify-content: space-around;
  align-items: flex-start;
  margin-top: ${normalize(5)}px;
  margin-bottom: ${normalize(5)}px;
  margin-left: ${normalize(12)}px;
`;
