import React, { FC, useEffect, useState } from 'react';

import { FlatList } from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../assets/styles/color';
import { AddItem, ComingSoon, Container } from '../../../components';
import { RootStackParamList } from '../../navigators/stack/questionAnswerStack';
import { QuestionCard } from './QuestionCard';

interface Props {
    navigation: StackNavigationProp<
    RootStackParamList,
        'QuestionAnswer'
    >;
}

export const QuestionAnswer: FC<Props> = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [questions, setQuestions] = useState([
        {
            id: '1',
            title: 'What is the unscrambled word of OTMH?',
            authorDes: 'React Native',
            author: 'Amar Singh',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
            tags: ['C++', 'C', 'Java', 'Javascript'],
            answers: [{}, {}],
            postedTime: '2021-02-06T16:34:00',
            likes: 10,
            dislikes: 20,
            views: 50,
        },
        {
            id: '2',
            title: 'How can I get a link to my Airbnb listing to send to friends?',
            author: 'Diksha Chaudhary',
            authorDes: 'React Native',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGmwVZ3vbHoQiz9Q2zC3dDJYJ-uCBNymbYTw&usqp=CAU',
            tags: ['Python', 'ML', 'Traveling'],
            answers: [],
            postedTime: '2021-04-06T16:00:00',
            likes: 10,
            dislikes: 20,
            views: 999,
        },
        {
            id: '3',
            title: 'How to use gif inside image in react native?',
            author: 'Nilesh Chavan',
            authorDes: 'React Native',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
            tags: ['Javascript', 'Ract native'],
            answers: [],
            postedTime: '2021-01-02T11:20:30',
            likes: 10,
            dislikes: 2,
            views: 8600,
        },
        {
            id: '4',
            title: 'How to Convert text to image in react js?',
            author: 'Devashree Patole',
            authorDes: 'React Native',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGmwVZ3vbHoQiz9Q2zC3dDJYJ-uCBNymbYTw&usqp=CAU',
            tags: ['React js'],
            answers: [],
            postedTime: '2021-04-06T16:00:00',
            likes: 100,
            dislikes: 1,
            views: 9000,
        },
    ]);
    const [modal, setModal] = useState(false);

    const handleLike = (index, type) => {
        const q = [...questions];
        if (type == 'inc') {
            q[index].likes += 1;
        } else {
            q[index].likes -= 1;
        }

        setQuestions(q);
    };

    const handleDislike = (index, type) => {
        const q = [...questions];
        if (type == 'inc') {
            q[index].dislikes += 1;
        } else {
            q[index].dislikes -= 1;
        }
        setQuestions(q);
    };

    const _renderItems = ({ item, index }) => {
        return (
            <QuestionCard
                key={index}
                index={index}
                data={item}
                handleLike={handleLike}
                handleDislike={handleDislike}
                navigation={navigation}
                onPress={()=> navigation.navigate('QuestionDetail')}
            />
        );
    };

    useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 2000);
    }, []);

    return (
        <ScreenContainer withKeyboard={false} loading={loading}>
            <FlatList
                data={questions}
                keyExtractor={(item, index) => index.toString()}
                renderItem={_renderItems}
                showsVerticalScrollIndicator={false}
            />
            <AddItem navigation={navigation} />

            <ComingSoon
                visibility={modal}
                handleModalVisibility={() => {
                    setModal(false);
                }}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  justify-content: center;
  background-color: ${Colors.appGray};
`;
