import React, { FC } from 'react';

import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Images } from '../../../assets/images/Images';
import { Container } from '../../../components';
import { ImageComponent } from '../../../components/Image';
import { RootStackParamList } from '../../navigators/stack/questionAnswerStack';

interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'QuestionDetail'>;
}

export const QuestionDetail: FC<Props> = ({ navigation }) => {
    return (
        <ScreenContainer>
            <ImageComponent
                source={Images.commingSoon}
                style={{ width: 250, height: 200 }}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  justify-content: center;
  align-items: center;
`;
