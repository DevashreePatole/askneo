import React, { FC, useEffect, useRef, useState } from 'react';

import { Animated, View } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { Text } from '../../../components';
import { Icon } from '../../../components/Icon';
import { normalize } from '../../../lib/globals';
import { ItemProps, QuestionOptionProps } from './displayCard';

interface Props {
  step: number;
  steps: number;
  selected: number | undefined;
  item: ItemProps;
  option: QuestionOptionProps;
  i: number;
  expired: boolean;
}

export const DisplayOption: FC<Props> = ({
  step,
  steps,
  selected,
  item,
  option,
  i,
  expired,
}) => {
  const animatedValue = useRef(new Animated.Value(-1000)).current;
  const reactive = useRef(new Animated.Value(-1000)).current;
  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (selected != undefined || expired) {
      Animated.timing(animatedValue, {
        toValue: reactive,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  }, [selected, expired]);

  useEffect(() => {
    reactive.setValue(-width + (width * step) / steps);
  }, [step, width]);

  return (
    <OptionView
      onLayout={(e) => {
        const newWidth = e.nativeEvent.layout.width;
        setWidth(newWidth);
      }}
      style={{
        backgroundColor:
          selected != undefined || expired ? Colors.white : Colors.black,
        overflow: 'hidden',
      }}>
      <Row style={{ width: '88%', paddingVertical: normalize(5) }}>
        <View
          style={{
            width: '85%',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text
            fontWeight="500"
            style={{
              lineHeight: normalize(15),
              marginLeft: normalize(15),
              color:
                selected != undefined || expired ? Colors.black : Colors.white,
            }}>
            {option.name}
          </Text>

          {selected == i ? (
            <Icon
              style={{ marginLeft: normalize(5) }}
              type="FontAwesome5"
              name="check-circle"
              size={normalize(20)}
            />
          ) : null}
        </View>
      </Row>
      {selected != undefined || expired ? (
        <View style={{ marginRight: normalize(10) }}>
          <Text>{step.toString()}%</Text>
        </View>
      ) : null}

      {selected != undefined || expired ? (
        <PollResult
          style={{
            width: `100%`,
            transform: [
              {
                translateX: animatedValue,
              },
            ],
          }}
        />
      ) : null}
    </OptionView>
  );
};

const Row = styled.View`
  flex-direction: row;
`;

const OptionView = styled.View`
  width: 100%;
  border-radius: ${normalize(5)}px;
  min-height: ${normalize(50)}px;
  flex-direction: row;
  align-items: center;
  margin-bottom: ${normalize(8)}px;
  justify-content: space-between;
`;

const PollResult = styled(Animated.View)`
  position: absolute;
  background-color: rgba(0, 0, 0, 0.3);
  height: 100%;
  border-radius: ${normalize(5)}px;
`;
