import React, { FC } from 'react';

import { StyleSheet } from 'react-native';
import moment from 'moment';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { Text, TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { DisplayOption } from './displayOption';

export interface QuestionOptionProps {
  name: string;
    vote: number;
}

export interface ItemProps {
  question: string;
  options: QuestionOptionProps[];
  totalVotes: number;
  pollEndDate: string;
  pollEndTime: string;
  optionSelected?: number;
}

interface Props {
  item: ItemProps;
  index: number;
  data: ItemProps[];
  setData: React.Dispatch<React.SetStateAction<ItemProps[]>>;
  expired: boolean;
}

export const DisplayCard: FC<Props> = ({
  item,
  index,
  data,
  setData,
  expired,
}) => {
  const handleOptionSelect = (index, i) => {
    const array = [...data];
    if (array[index]?.optionSelected !== undefined) {
      array[index].totalVotes -= 1;
      array[index].options[array[index]?.optionSelected].vote -= 1;
    }

    array[index].totalVotes += 1;
    array[index].options[i].vote += 1;
    array[index].optionSelected = i;
    console.log(array[index], array);
    setData(array);
  };

  return (
    <Card>
      <Text
        style={styles.title}
        align="flex-start"
        size={textSizes.h8}
        fontWeight="500">
        {item.question}
      </Text>

      <OptionsContainer>
        {item.options.map((option, i) => {
          return (
            <TouchItem
              disabled={item?.optionSelected == i || expired ? true : false}
              key={i}
              onPress={() => {
                handleOptionSelect(index, i);
              }}>
              <DisplayOption
                step={Math.round((option.vote / item.totalVotes) * 100)}
                steps={100}
                selected={item?.optionSelected}
                item={item}
                option={option}
                i={i}
                expired={expired}
              />
            </TouchItem>
          );
        })}
      </OptionsContainer>
      {!expired ? (
        <Row style={{ marginTop: normalize(10) }}>
          <Text style={{ color: Colors.black }}>
            Expires on:{' '}
            {moment(
              new Date(`${item.pollEndDate} ${item.pollEndTime}`),
            ).calendar()}
            `
          </Text>
        </Row>
      ) : null}
    </Card>
  );
};

const Card = styled.View`
  width: 100%;
  background-color: ${Colors.primaryColor};
  padding: ${normalize(15)}px;
  border-radius: ${normalize(10)}px;
  padding-top: ${normalize(20)}px;
  padding-bottom: ${normalize(20)}px;
  margin-bottom: ${normalize(20)}px;
`;

const OptionsContainer = styled.View`
  margin-top: ${normalize(15)}px;
`;

const Row = styled.View`
  flex-direction: row;
`;

const styles = StyleSheet.create({
  title: {
    textTransform: 'capitalize',
    lineHeight: normalize(20),
    color: Colors.black,
  },
});
