import React, { FC, useState } from 'react';

import { FlatList, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../assets/styles/color';
import { AddItem, Container } from '../../../components';
import { normalize } from '../../../lib/globals';
import { RootStackParamList } from '../../navigators/stack/drawerStack';
import { DisplayCard } from './displayCard';

interface Props {
    navigation: StackNavigationProp<RootStackParamList,'Poll'>
}

export const Poll: FC<Props> = ({ navigation }) => {
    const [data, setData] = useState([
        {
            question: 'Future Most Valuable Tech Jobs',
            options: [
                {
                    name: 'Javascript Developer',
                    vote: 18,
                },
                {
                    name: 'AI Developer',
                    vote: 22,
                },
                {
                    name: 'Blockchain Developer',
                    vote: 25,
                },
                {
                    name: 'Machine Learning Developer',
                    vote: 15,
                },
            ],
            totalVotes: 80,
            pollEndDate: '08/02/2021',
            pollEndTime: '5:15 PM',
        },
        {
            question: 'Samrtest mobile brand In India',
            options: [
                {
                    name: 'Realme',
                    vote: 2,
                },
                {
                    name: 'Samsung',
                    vote: 1,
                },
                {
                    name: 'Nokia',
                    vote: 9,
                },
                {
                    name: 'Iphone',
                    vote: 4,
                },
            ],
            totalVotes: 16,
            pollEndDate: '05/30/2021',
            pollEndTime: '9:15 PM',
        },
        {
            question: 'Vs Code Is the best code editor out their?',
            options: [
                {
                    name: 'Yes',
                    vote: 5,
                },
                {
                    name: 'No',
                    vote: 2,
                },
            ],
            totalVotes: 7,
            pollEndDate: '05/12/2021',
            pollEndTime: '6:16 PM',
        },
    ]);

    const _handleRender = ({ item, index }) => {
        const difference =
            new Date().getTime() -
            new Date(`${item.pollEndDate} ${item.pollEndTime}`).getTime();

        return (
            <DisplayCard
                item={item}
                index={index}
                data={data}
                setData={setData}
                expired={difference > 0 ? true : false}
            />
        );
    };

    return (
        <ScreenContainer withKeyboard={false}>
            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={_handleRender}
            />
            <AddItem
                navigation={navigation}
                styles={styles.addIcon}
                onPress={() => {
                    navigation.navigate('CreatePoll');
                }}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  background-color: ${Colors.white};
`;

const styles = StyleSheet.create({
    addIcon: {
        bottom: normalize(12),
        right: normalize(12),
        backgroundColor: Colors.silver,
    },
});
