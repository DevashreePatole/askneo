import React, { FC, useRef, useState } from 'react';

import { ScrollView, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import { StackNavigationProp } from '@react-navigation/stack';

import AlertModal from '../../../components/AlertModal';
import {
    DatePick,
    Input,
    SafeContainer,
    Text,
    TimePick,
    TouchItem,
} from '../../../components';
import Button from '../../../components/Button';
import { Icon } from '../../../components/Icon';
import { normalize } from '../../../lib/globals';
import { RootStackParamList } from '../../navigators/stack/drawerStack';

interface Props {
    navigation:StackNavigationProp<RootStackParamList,'CreatePoll'>
}

export const CreatePoll:FC<Props> = ({ navigation }) => {
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [question, setQuestion] = useState('');
    const [options, setOptions] = useState(['', '']);
    const [loading, setLoading] = useState(false);
    let alertRef = useRef();

    const handleSubmit = () => {
        let valid = true;
        if (question != '' && time != '' && date != '') {
            options.map((option) => {
                if (option == '') {
                    valid = false;
                }
            });
        } else {
            valid = false;
        }

        if (valid) {
            setLoading(true);
            setTimeout(() => {
                setLoading(false);
                const warningModal = () =>
                    alertRef?.current?.setOptionAndOpen(
                        'success',
                        'Poll created successfully. Poll will be visible after approved from admin',
                        () => navigation.goBack(),
                    );
                warningModal();
            }, 3000);
        } else {
            Toast.show('Please fill all the data', Toast.SHORT);
        }
    };

    const handleChangeText = (val, index) => {
        const data = [...options];
        data[index] = val;
        setOptions(data);
    };

    const handleIconPress = (index) => {
        const data = [...options];
        data.splice(index, 1);
        setOptions(data);
    };

    return (
        <ScreenContainer
            loading={loading}
            withKeyboard={false}
            paddingStyle={styles.paddingStyle}>
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ marginTop: normalize(10) }}>
                <Input
                    label="QUESTION"
                    value={question}
                    onChangeText={(val) => {
                        setQuestion(val);
                    }}
                    placeholder="What's your poll question?"
                />

                <ExpirationContainer>
                    <Text align="flex-start">EXPIRATION</Text>
                    <DatePick
                        minimumDate={new Date(moment(new Date()).add(1, 'days'))}
                        date={date}
                        setDate={setDate}
                    />
                    <View style={{ marginTop: normalize(5) }}>
                        <TimePick
                            minimumTime={new Date(moment(new Date()).add(60, 'minutes'))}
                            time={time}
                            setTime={setTime}
                        />
                    </View>
                </ExpirationContainer>
                <OptionContainer>
                    <Text align="flex-start" style={{ marginBottom: -normalize(10) }}>
                        OPTIONS
                    </Text>
                    {options.map((option, index) => {
                        if (index < 2) {
                            return (
                                <Input
                                    key={index}
                                    placeholder={`Option ${index + 1}`}
                                    value={option}
                                    onChangeText={(val) => {
                                        handleChangeText(val, index);
                                    }}
                                />
                            );
                        } else {
                            return (
                                <Input
                                    icon="close"
                                    iconSize={normalize(30)}
                                    iconAction={() => {
                                        handleIconPress(index);
                                    }}
                                    iconStyle={{ top: normalize(0) }}
                                    key={index}
                                    placeholder={`Option ${index + 1}`}
                                    value={option}
                                    onChangeText={(val) => {
                                        handleChangeText(val, index);
                                    }}
                                />
                            );
                        }
                    })}
                </OptionContainer>
                <TouchItem
                    onPress={() => {
                        setOptions((prevState) => [...prevState, '']);
                    }}>
                    <AddOptionContainer>
                        <Icon type="FontAwesome" name="plus" size={normalize(22)} />
                        <Text style={{ marginLeft: normalize(20) }}>Add Option</Text>
                    </AddOptionContainer>
                </TouchItem>

                <ButtonContainer>
                    <Button
                        title="Create Poll"
                        onClick={() => {
                            handleSubmit();
                        }}
                        style={styles.buttonStyle}
                    />
                </ButtonContainer>
            </ScrollView>
            <AlertModal ref={alertRef} />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(SafeContainer)`
  flex: 1;
`;

const ExpirationContainer = styled.View`
  margin-top: ${normalize(35)}px;
  margin-bottom: ${normalize(20)}px;
`;

const OptionContainer = styled.View`
  margin-top: ${normalize(20)}px;
`;

const AddOptionContainer = styled.View`
  flex-direction: row;
  margin-top: ${normalize(30)}px;
`;

const ButtonContainer = styled.View`
  margin-top: ${normalize(30)}px;
`;

const styles = StyleSheet.create({
    paddingStyle: {
        padding: normalize(20),
        paddingTop: 0,
        paddingBottom: 0,
    },
    buttonStyle: {
        width: normalize(180),
        borderRadius: normalize(20),
        marginBottom: normalize(20),
    },
});
