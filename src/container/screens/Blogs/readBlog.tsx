import React, { FC, useRef, useState } from 'react';

import {
  View,
  Platform,
  StatusBar,
  ScrollView,
  StyleSheet,
} from 'react-native';
import styled from 'styled-components/native';
import moment from 'moment';
import { StackNavigationProp } from '@react-navigation/stack';
import ImageViewer from 'react-native-image-zoom-viewer';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';

import { ImageBack, Text, TouchItem } from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { ModalContainer } from '../../../components/Modal';
import { RootStackParamList } from '../../navigators/stack/blogStack';
import { AddComment } from '../../../components/AddComment';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'ReadBlog'>;
}

export const ReadBlog: FC<Props> = ({ navigation }) => {
  const blog = {
    profileImage:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
    employeeName: 'Nilesh Chavan',
    technology: 'React Native',
    empId: '1234',
    title: 'Announcing React Native 0.62 with Flipper',
    postedOn: '2021-04-07T08:00:00',
    discription: [
      {
        subTitle: 'New dark mode features',
        description:
          "We’ve added a new Appearance module to provide access to the user's appearance preferences, such as their preferred color scheme (light or dark)." +
          ' We’ve also added a hook to subscribe to state updates to the users preferences:',
        image: [
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-BSCtIFiVsn1DJSIquGzOOImugGrhkZFHeA&usqp=CAU',
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPaOnZWCfssc7SwBAswB1k9LzcK1RtBAiGoQ&usqp=CAU',
        ],
      },
      {
        subTitle: 'New dark mode features',
        description:
          "We’ve added a new Appearance module to provide access to the user's appearance preferences, such as their preferred color scheme (light or dark)." +
          ' We’ve also added a hook to subscribe to state updates to the users preferences:',
        image: [
          'https://media.cheggcdn.com/media%2F722%2F722abbf9-ac56-4cda-8227-d3c5fdbac0e5%2FphpenVRmr.png',
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1wQ-VkbOO3bVOJnRjL6yHnYbPMAJ38tQHErKb1HsUyNi_JkwTuGaxp8uSgsdODEZvufk&usqp=CAU',
        ],
      },
    ],
    likes: '5',
    blogImage:
      'https://reactnative.dev/assets/images/0.63-logbox-a209851328e548bf0810bdee050fb960.png',
  };

  const [modal, setModal] = useState(false);
  const refRBSheet = useRef<RBSheet>(null);
  const [modalImage, setImage] = useState([
    {
      url: '',
      props: {},
    },
  ]);
  const [bookMarked, setBookMark] = useState(false);
  const uploadedDate = moment(blog.postedOn).format('LL');

  const handleBookmark = () => {
    if (bookMarked == false) {
      Toast.show('Added To Bookmark', Toast.SHORT, [
        'UIAlertController',
      ]);
    } else {
      Toast.show('Removed From Bookmark', Toast.SHORT, [
        'UIAlertController',
      ]);
    }
    setBookMark((prev) => !prev);
  };

  return (
    <MainContainer>
      {Platform.OS == 'ios' && (
        <StatusBar
          backgroundColor="#b3e6ff"
          barStyle="dark-content"
          hidden={true}
        />
      )}
      <ScrollView
        alwaysBounceVertical={false}
        showsVerticalScrollIndicator={false}>
        <View>
          <ImageBack
            source={{ uri: `${blog.blogImage}` }}
            style={{ width: '100%', height: normalize(250) }}
            resizeMode={'cover'}
            >
            <HeaderContainer>
              <TouchItem onPress={() => navigation.goBack()}>
                <Icon type="Entypo" name="chevron-left" size={normalize(32)} />
              </TouchItem>
              <TouchItem onPress={() => handleBookmark()}>
                <Icon
                  type="FontAwesome"
                  name={bookMarked ? 'bookmark' : 'bookmark-o'}
                  size={normalize(22)}
                  color={bookMarked ? Colors.primaryColor : Colors.black}
                />
              </TouchItem>
            </HeaderContainer>
          </ImageBack>

          <BlogContainer>
            <TitleContainer>
              <Text
                fontWeight="600"
                size={textSizes.h7}
                style={{ lineHeight: normalize(22) }}>
                {blog.title}
              </Text>
              <Row style={{ justifyContent: 'space-between' }}>
                <Row style={{ marginTop: normalize(10) }}>
                  <Icon
                    type="FontAwesome"
                    name="clock-o"
                    size={normalize(14)}
                    color={Colors.darkGray}
                  />
                  <Text size={textSizes.h10} style={styles.blogTimeDate}>
                    {uploadedDate}
                  </Text>
                </Row>
                <Row style={{ marginTop: normalize(10) }}>
                  <Icon
                    type="FontAwesome"
                    name="thumbs-up"
                    size={normalize(18)}
                    color={Colors.darkGray}
                  />
                  <Text style={styles.blogTimeDate}>{blog.likes}</Text>
                </Row>
              </Row>
            </TitleContainer>
            {blog.discription.map((item, index) => {
              return (
                <DetailContainer key={index}>
                  <SubTitle>{item.subTitle}</SubTitle>
                  <Description>{item.description}</Description>
                  <ImageContainer key={index} style={styles.imgContainer}>
                    {item.image.map((image, index, item) => {
                      return (
                        <View
                          key={index}
                          style={{
                            width: item.length > 1 ? '45%' : '90%',
                          }}>
                          <TouchItem
                            onPress={() => {
                              let data = modalImage;
                              data[0].url = image;
                              setImage(data);
                              setModal(true);
                            }}>
                            <ImageComponent
                              source={{ uri: image }}
                              style={{ width: '100%', height: normalize(140) }}
                              resizeMode={'stretch'}
                            />
                          </TouchItem>
                        </View>
                      );
                    })}
                  </ImageContainer>
                </DetailContainer>
              );
            })}
          </BlogContainer>
        </View>
      </ScrollView>

      <ModalContainer
        visibility={modal}
        handleModalVisibility={() => {
          setModal(false);
          setImage([
            {
              url: '',
              props: {},
            },
          ]);
        }}>
        <ImageViewer
          imageUrls={modalImage}
          backgroundColor={'white'}
          renderIndicator={() => {
            return <Text></Text>;
          }}
          style={{ height: normalize(200), width: '100%' }}
          renderImage={(props) => {
            return (
              <ImageComponent
                {...props}
                resizeMode={'contain'}
                style={{ width: '100%', height: '100%' }}
              />
            );
          }}
        />
      </ModalContainer>
      <CommentContainer
        onPress={() => {
          refRBSheet.current?.open();
        }}>
        <Icon
          type="FontAwesome"
          name="comments"
          size={normalize(22)}
          color={Colors.black}
        />
      </CommentContainer>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: Colors.black,
          },
          container: {
            borderTopRightRadius: normalize(20),
            borderTopLeftRadius: normalize(20),
            borderColor: '#d6d6d6',
            borderWidth: 1,
            height: '90%',
          },
        }}>
        <AddComment />
      </RBSheet>
    </MainContainer>
  );
};

const MainContainer = styled.View`
  background-color: ${Colors.lightGray};
`;

const Row = styled.View`
  flex-direction: row;
`;

const BlogContainer = styled.View`
  width: 100%;
  border-top-right-radius: ${normalize(20)}px;
  border-top-left-radius: ${normalize(20)}px;
  background-color: white;
  margin-top: -${normalize(15)}px;
`;

const TitleContainer = styled.View`
  align-self: flex-start;
  margin-top: ${normalize(10)}px;
  padding: ${normalize(5)}px;
  padding-left: ${normalize(14)}px;
`;

const DetailContainer = styled.View`
  padding: ${normalize(15)}px;
`;

const SubTitle = styled.Text`
  font-size: ${textSizes.h8}px;
  font-weight: 500;
  align-self: flex-start;
  padding-left: ${normalize(5)}px;
  margin-bottom: ${normalize(10)}px;
`;

const Description = styled.Text`
  line-height: 23px;
  font-size: ${textSizes.h9}px;
`;

const ImageContainer = styled.View`
  margin-top: ${normalize(20)}px;
`;

const HeaderContainer = styled.View`
  position: absolute;
  flex-direction: row;
  justify-content: space-between;
  top: ${normalize(25)}px;
  width: 100%;
  align-items: center;
  padding-right: ${normalize(20)}px;
  padding-left: ${normalize(10)}px;
`;

const CommentContainer = styled(TouchItem)`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.primaryColor};
  position: absolute;
  bottom: ${normalize(20)}px;
  right: ${normalize(10)}px;
  border-radius: ${normalize(50)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const styles = StyleSheet.create({
  imgContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  blogTimeDate: {
    color: Colors.darkGray,
    paddingLeft: normalize(10),
  },
});
