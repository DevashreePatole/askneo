import React, { FC, useEffect, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import {
  AddItem,
  ComingSoon,
  Container,
  TouchItem,
} from '../../../components';
import { CardView } from './cardView';
import { RootStackParamList } from '../../navigators/stack/blogStack';
import { Colors } from '../../../assets/styles/color';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'Blogs'>;
}

export const Blog: FC<Props> = ({ navigation }) => {
  const [loading, setLoading] = useState(false);
  let blog = [
    {
      id:'1',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      technology: 'React Native',
      empId: '1234',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      description:
        'Flipper is a developer tool for debugging mobile apps. It’s popular in the Android and iOS communities, and in this release we’ve enabled support by default for new and existing React Native apps.',
      blogImage:
        'https://reactnative.dev/assets/images/0.62-flipper-dc5a5cb54cc6033750c56f3c147c6ce3.png',
      likes: '5',
    },
    {
      id:'2',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      technology: 'React Native',
      empId: '1234',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-07T08:00:00',
      description:
        'Flipper is a developer tool for debugging mobile apps. It’s popular in the Android and iOS communities, and in this release we’ve enabled support by default for new and existing React Native apps.',
      blogImage:
        'https://reactnative.dev/assets/images/0.62-flipper-dc5a5cb54cc6033750c56f3c147c6ce3.png',
      likes: '5',
    },
  ];
  const [modal, setModal] = useState(false);

  const _renderData = ({ item }) => {
    return (
      <TouchItem onPress={() => navigation.navigate('ReadBlog')}>
        <CardView blog={item} />
      </TouchItem>
    );
  };

  const _keyCountryExtractor = (item, index) => item.id;

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  return (
    <ScreenContainer withKeyboard={false} loading={loading}>
      <FlatList
        data={blog}
        renderItem={_renderData}
        keyExtractor={_keyCountryExtractor}
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}
      />
      <AddItem navigation={navigation} />

      <ComingSoon
        visibility={modal}
        handleModalVisibility={() => {
          setModal(false);
        }}
      />
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  justify-content: center;
  background-color:${Colors.lightGray};
`;
