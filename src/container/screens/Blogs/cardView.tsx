import React, { FC, useEffect, useState } from 'react';

import styled from 'styled-components/native';
import { View, Animated, StyleSheet } from 'react-native';

import { Colors } from '../../../assets/styles/color';
import { Container, Text, TimeAgo, TouchItem } from '../../../components';
import { AnimateIcon, AnimatedIcon } from '../../../components/AnimatedIcon';
import { normalize, textSizes } from '../../../lib/globals';
import { convertToBase64, shareData } from '../../../lib/helper';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

interface BlogProps {
  title: string;
  postedOn: string;
  description: string;
  blogImage: string;
  likes: string;
  profileImage: string;
  employeeName: string;
  technology: string;
  empId: string;
}

interface Props {
  blog: BlogProps;
  userBlog?: boolean;
}

export const CardViewComponent: FC<Props> = ({ blog, userBlog = false, ...props }) => {
  const [likes, setLikes] = useState(parseInt(blog.likes));
  const [disabled, setDisabled] = useState(false);
  const [visible, setVisible] = useState(false);
  const [imageData, setImageData] = useState('');
  const [loading, setLoading] = useState(false);

  const currentValue = new Animated.Value(1);

  useEffect(() => {
    if (disabled == true) {
      AnimateIcon(currentValue, () => {
        setVisible(false);
      });
    }
  }, [disabled]);

  const handleLikeClick = () => {
    if (!disabled) {
      setLikes((prev) => prev + 1);
      setDisabled(true);
      setVisible(true);
    } else {
      setLikes((prev) => prev - 1);
      setDisabled(false);
      setVisible(false);
    }
  };

  const shareMeme = async () => {
    shareData(imageData, `${blog.title}\n\n ${blog.description}`);
  };

  const fetchData = async () => {
    convertToBase64(`${blog.blogImage}`)
      .then((data) => {
        // console.log(data)
        setImageData(data);
      })
      .catch((e) => {
        // console.log(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const getBase64Data = () => {
    setLoading(true);
  };

  useEffect(() => {
    if (loading == true) {
      fetchData();
    }
  }, [loading]);

  useEffect(() => {
    if (loading == false && imageData != '') {
      setTimeout(() => {
        shareMeme();
      }, 500);
    }
  }, [loading, imageData]);
  return (
    <CardContainer loading={loading} withKeyboard={false}>
      <HeaderContainer>
        <Image source={{ uri: `${blog.profileImage}` }} />
        <ProfileContainer>
          <Text fontWeight="500">{blog.employeeName}</Text>
          <Text style={styles.technology}>{blog.technology}</Text>
        </ProfileContainer>
      </HeaderContainer>
      <HorizontalRule />
      <TitleContainer>
        <Text fontWeight="600" size={textSizes.h8} style={styles.title}>
          {blog.title}
        </Text>
      </TitleContainer>
      <TimeAgo time={blog.postedOn} style={styles.postTime} />
      <View>
        <ImageContainer
          source={{ uri: `${blog.blogImage}` }}
          resizeMode={'stretch'}
        />
      </View>
      <DetailContainer>
        <Text numberOfLines={3} style={{ lineHeight: normalize(19) }}>
          {blog.description}
        </Text>
      </DetailContainer>
      <FooterContainer>
        <Row>
          <TouchItem onPress={handleLikeClick}>
            {disabled ? (
              <Icon
                name="heart"
                size={normalize(20)}
                type="FontAwesome"
                color={Colors.primaryColor}
              />
            ) : (
              <Icon
                name="heart-o"
                size={normalize(20)}
                type="FontAwesome"
                color={Colors.darkGray}
              />
            )}
          </TouchItem>
          <Text style={styles.likeText} size={textSizes.h9}>
            {`${likes} Likes`}
          </Text>
        </Row>
        <Row style={{ alignItems: 'center' }}>
          <TouchItem onPress={() => getBase64Data()}>
            <Icon
              name="share-alt"
              size={normalize(20)}
              type="FontAwesome"
              color={Colors.lightBlue}
            />
          </TouchItem>
          <TouchItem onPress={() => getBase64Data()}>
            <Text style={styles.shareText} size={textSizes.h9}>
              Share
            </Text>
          </TouchItem>
        </Row>
      </FooterContainer>
      {visible && (
        <AnimatedIcon
          name="heart"
          size={normalize(50)}
          color={Colors.primaryColor}
          currentValue={currentValue}
        />
      )}

      {userBlog ? (
        <TouchItem
          onPress={() => { }}
          style={{
            position: 'absolute',
            top: normalize(10),
            right: normalize(10),
            padding: normalize(2),
          }}>
          <Icon type="MaterialIcons" name="delete" size={normalize(22)} />
        </TouchItem>
      ) : null}
    </CardContainer>
  );
};

const CardContainer = styled(Container)`
  flex: 0;
  background-color: ${Colors.white};
  padding: ${normalize(10)}px;
  border-radius: ${normalize(8)}px;
  margin-top: ${normalize(10)}px;
  margin-bottom: ${normalize(10)}px;
`;

const Row = styled.View`
  flex-direction: row;
`;

const HeaderContainer = styled(Row)`
  justify-content: flex-start;
`;

const Image = styled(ImageComponent)`
  width: ${normalize(37)}px;
  height: ${normalize(37)}px;
  border-radius: ${normalize(37)}px;
`;

const ProfileContainer = styled.View`
  padding-left: ${normalize(9)}px;
  padding-top: ${normalize(9)}px;
`;

const HorizontalRule = styled.View`
  border-bottom-color: ${Colors.darkGray};
  border-bottom-width: 1px;
  width: 40%;
  align-self: center;
  margin-top: ${normalize(10)}px;
  margin-bottom: ${normalize(10)}px;
`;

const TitleContainer = styled.View`
  align-self: flex-start;
  margin-left: ${normalize(10)}px;
  margin-top: ${normalize(10)}px;
`;

const DetailContainer = styled.View`
  margin-left: ${normalize(5)}px;
  margin-top: ${normalize(10)}px;
`;

const ImageContainer = styled(ImageComponent)`
  margin-top: ${normalize(10)}px;
  border-radius: ${normalize(5)}px;
  width: 100%;
  height: ${normalize(170)}px;
`;

const FooterContainer = styled(Row)`
  margin: ${normalize(10)}px;
  justify-content: space-around;
  padding-right: ${normalize(5)}px;
  padding-left: ${normalize(5)}px;
`;

const styles = StyleSheet.create({
  technology: {
    fontSize: textSizes.h13,
    alignSelf: 'flex-start',
    left: 0,
  },
  title: {
    color: Colors.black,
    lineHeight: normalize(22),
  },
  postTime: {
    alignSelf: 'flex-end',
    paddingRight: normalize(10),
    fontSize: textSizes.h12,
  },
  likeText: {
    paddingLeft: normalize(10),
    color: Colors.darkGray,
  },
  shareText: {
    paddingLeft: normalize(10),
    color: Colors.darkGray,
  },
});

export const CardView = React.memo(CardViewComponent)
