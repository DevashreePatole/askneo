import React, { useEffect, useState } from 'react';

import { View, LogBox, StyleSheet, Platform } from 'react-native';
import libmoji from 'libmoji';
import styled from 'styled-components/native';

import { TraitOptionCard } from './bitmojiEditOptions/traitOptionCard';
import { BrandOutfit } from './bitmojiEditOptions/brandOutfits';
import { normalize } from '../../../lib/globals';
import { ImageComponent } from '../../../components/Image';
import {
  Container,
  Icon,
  ImageBack,
  SafeContainer,
  TouchItem,
} from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { BottomMainOptionComponent } from './bitmojiEditOptions/bottomOptionMain';

interface OptionsProps{
  value:string
}

interface TraitOptionsProp {
  key: string;
  options?: OptionsProps[];
}

interface AddedTraitsProps {
  name: string;
  value: number;
}

interface OutfitIndexProps{
  mainIndex: number;
  secIndex: number;
}

interface ActiveTabProps{
  key:string;
  options?:OptionsProps[]
}

export const CustomizeBitmoji = ({ navigation, route }) => {
  const [loading, setLoading] = useState(false);
  const [position, setPosition] = useState('body');
  const [traits, setTraits] = useState<TraitOptionsProp[]>([]);
  const [gender, setGender] = useState<number>(route.params.gender);
  const [styles, setStyles] = useState(4);
  const [mainURL, setMainUrl] = useState(
    `https://preview.bitmoji.com/avatar-builder-v3/preview/${position}?scale=3&gender=${gender}&style=${styles}`,
  );
  const [displayMainBitmoji, setDisplayBitMoji] = useState(
    `https://preview.bitmoji.com/avatar-builder-v3/preview/${position}?scale=3&gender=${gender}&style=${styles}`,
  );
  const [selectedOutfit, setSelectedOutfit] = useState<number|string>('');
  const [activeTab, setActiveTab] = useState<ActiveTabProps>({});
  const [tabOptions, setTabOptions] = useState<TraitOptionsProp[]>([]);
  const [selectedOutfitIndex, setSelectedOutfitIndex] = useState<OutfitIndexProps>();
  const [traitsOptions, setTraitsOptions] = useState<AddedTraitsProps[]>([]);

  useEffect(() => {
    let url = mainURL;
    let url2 = displayMainBitmoji;

    if (position == 'fashion') {
      url = url.replace('body', 'fashion');
      url2 = url2.replace('body', 'fashion');
    } else {
      url = url.replace('fashion', 'body');
      url2 = url2.replace('fashion', 'body');
    }

    setMainUrl(url);
    setDisplayBitMoji(url2);
  }, [position]);

  useEffect(() => {
    if (selectedOutfit != '') {
      let url = `https://preview.bitmoji.com/avatar-builder-v3/preview/${position}?scale=3&gender=${gender}&style=${styles}`;
      let myUrl = url;

      traitsOptions.map((op, index) => {
        myUrl += `&${op.name}=${op.value}`;
      });

      myUrl += `&outfit=${selectedOutfit}`;
      url += `&outfit=${selectedOutfit}`;
      setMainUrl(myUrl);
      setDisplayBitMoji(url);
    }
  }, [selectedOutfit]);

  useEffect(() => {
    LogBox.ignoreAllLogs();
    let type;
    if (gender == 1) {
      type = 'male';
    } else {
      type = 'female';
    }
    const trait = libmoji.getTraits(type, 'bitmoji');
    setTraits(trait);
    setActiveTab({ key: 'Outfits' });
    setTabOptions([
      {
        key: 'Outfits',
      },
      ...trait,
    ]);

    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }, []);

  useEffect(() => {
    let myUrl = displayMainBitmoji;

    traitsOptions.map((op, index) => {
      myUrl += `&${op.name}=${op.value}`;
    });

    setMainUrl(myUrl);
  }, [traitsOptions]);

  return (
    <ScreenContainer loading={loading}>
      <View style={{ height: '45%' }}>
        <ImageBack
          source={{
            uri:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGwHu70hql2KdP8TOZVms1iTql_KzYSdgifw&usqp=CAU',
          }}
          style={style.imageBack}>
          <ImageComponent
            source={{ uri: mainURL }}
            style={style.mainImage}
            resizeMode="contain"
          />
        </ImageBack>

        <View style={style.normal}>
          <FloatingButton onPress={() => setPosition('body')}>
            <ImageComponent
              resizeMode="contain"
              style={{ width: normalize(40), height: normalize(40) }}
              source={{
                uri: `https://preview.bitmoji.com/avatar-builder-v3/preview/body?scale=3&gender=${gender}&style=4`,
              }}
            />
          </FloatingButton>
        </View>
        <View style={style.fashion}>
          <FloatingButton
            onPress={() => {
              setPosition('fashion');
            }}>
            <ImageComponent
              resizeMode="contain"
              style={{ width: normalize(40), height: normalize(40) }}
              source={{
                uri: `https://preview.bitmoji.com/avatar-builder-v3/preview/fashion?scale=3&gender=${gender}&style=4`,
              }}
            />
          </FloatingButton>
        </View>
        <View style={style.done}>
          <FloatingButton
            onPress={() => {
              navigation.push('PreviewBitmoji', {
                previewImage: mainURL,
              });
            }}>
            <Icon
              type="Entypo"
              name="check"
              size={normalize(25)}
              color={Colors.white}
            />
          </FloatingButton>
        </View>
        <TouchItem
          style={style.backButton}
          onPress={() => {
            navigation.goBack();
          }}>
          <BackContainer>
            <Icon
              type="Entypo"
              name="cross"
              size={normalize(25)}
              color={Colors.white}
            />
          </BackContainer>
        </TouchItem>
      </View>
      <SafeContainer
        style={{ flex: 1 }}
        paddingStyle={{ paddingTop: 0, paddingBottom: 0 }}>
        <View style={style.scrollContainer}>
          <OptionDisplayContainer>
            {activeTab.key == 'Outfits' ? (
              <BrandOutfit
                selectedOutfitIndex={selectedOutfitIndex}
                setSelectedOutfitIndex={setSelectedOutfitIndex}
                gender={gender}
                setSelectedOutfit={setSelectedOutfit}
              />
            ) : null}

            {traits.length > 0 && activeTab?.options != undefined && (
              <TraitOptionCard
                name={activeTab.key}
                options={activeTab.options}
                traitsOptions={traitsOptions}
                setTraitsOptions={setTraitsOptions}
                gender={gender}
                style={styles}
              />
            )}
          </OptionDisplayContainer>
        </View>
        <OptionContainer>
          <BottomMainOptionComponent
            tabOptions={tabOptions}
            activeTab={activeTab}
            setActiveTab={setActiveTab}
          />
        </OptionContainer>
      </SafeContainer>
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  padding: 0px;
`;

const FloatingButton = styled.TouchableOpacity`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.transparent};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: ${normalize(10)}px;
  border-radius: ${normalize(50)}px;
`;

const OptionDisplayContainer = styled.View`
  width: 100%;
  height: 100%;
  flex-direction: column;
  align-items: center;
`;

const OptionContainer = styled.View`
  position: absolute;
  bottom: ${Platform.OS == 'ios' ? -normalize(10) : 0}px;
  flex-direction: row;
  width: 100%;
  height: 13%;
  border-radius: ${normalize(100)}px;
`;

const BackContainer = styled.View`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.transparent};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  top: ${normalize(30)};
  left: ${normalize(10)}px;
  border-radius: ${normalize(50)};
`;

const style = StyleSheet.create({
  normal: {
    width: normalize(50),
    height: normalize(50),
    position: 'absolute',
    left: normalize(10),
    bottom: normalize(10),
  },
  fashion: {
    position: 'absolute',
    right: normalize(10),
    bottom: normalize(10),
  },
  done: {
    position: 'absolute',
    right: normalize(10),
    top: normalize(30),
  },
  scrollContainer: {
    height: '87%',
    marginBottom: '13%',
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  mainImage: {
    width: '100%',
    height: '85%',
    position: 'absolute',
    bottom: normalize(20),
  },
  imageBack: {
    width: '100%',
    height: '100%',
  },
  backButton: {
    width: normalize(50),
    height: normalize(50),
    position: 'absolute',
  },
});
