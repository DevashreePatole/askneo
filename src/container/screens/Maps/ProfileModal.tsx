import React, { FC } from 'react';

import { Modal, TouchableWithoutFeedback, StyleSheet, Image } from 'react-native';
import styled from 'styled-components/native';

import { Text } from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';

interface Props {
  visibility: boolean;
  handleVisibility: () => void;
  profileImage: string;
  employeeName: string;
  designation: string;
}

export const ProfileModal: FC<Props> = ({
  visibility,
  handleVisibility,
  profileImage,
  designation,
  employeeName,
  ...props
}) => {
  return (
    <Modal visible={visibility} animationType={'fade'} transparent>
      <TouchableWithoutFeedback onPress={handleVisibility}>
        <OuterContainer>
          <InnerContainer>
            <ImageContainer>
              <Image
                source={{ uri: profileImage }}
                style={{ width: '100%', height: '100%' }}
                resizeMode={'cover'}
              />
            </ImageContainer>

            <DetailContainer>
              <Text
                style={styles.testStyle}
                size={textSizes.h7}
                fontWeight={'500'}>
                {employeeName}
              </Text>
              <Text
                style={styles.testStyle}
                size={textSizes.h9}
                fontWeight={'300'}>
                {designation}
              </Text>
            </DetailContainer>
          </InnerContainer>
        </OuterContainer>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const OuterContainer = styled.View`
  flex: 1;
  background-color: ${Colors.transparent};
  justify-content: center;
  align-items: center;
`;

const InnerContainer = styled.View`
  width: ${normalize(270)}px;
  height: ${normalize(250)}px;
  background-color: ${Colors.white};
  align-items: center;
  border-radius: ${normalize(5)}px;
`;

const ImageContainer = styled.View`
  width: ${normalize(270)}px;
  height: ${normalize(170)}px;
  border-top-end-radius: ${normalize(5)}px;
  border-top-start-radius: ${normalize(5)}px;
`;

const DetailContainer = styled.View`
  margin-top: ${normalize(8)}px;
`;

const styles = StyleSheet.create({
  testStyle: {
    marginTop: normalize(10),
  },
});
