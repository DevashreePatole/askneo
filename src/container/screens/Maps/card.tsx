import React, { FC } from 'react';

import { View, Image, StyleSheet } from 'react-native';
import { MarkerProps } from 'react-native-maps';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { Icon } from '../../../components/Icon';
import { normalize } from '../../../lib/globals';

interface Props {
  marker?: MarkerProps;
  profileImage: string;
  employeeName: string;
  bitmoji: any;
}

export const MapCard: FC<Props> = ({
  marker,
  profileImage,
  employeeName,
  bitmoji,
  ...props
}) => {
  return (
    <View style={styles.viewStyle}>
      {bitmoji != '' ? (
        <View>
          <Image
            source={bitmoji}
            style={{ width: normalize(90), height: normalize(90) }}
          />
        </View>
      ) : (
        <View>
          <OuterContainer>
            <InnerContainer>
              <Image source={{ uri: profileImage }} style={styles.imageStyle} />
            </InnerContainer>
          </OuterContainer>
          <Icon
            type="FontAwesome5"
            name="angle-double-down"
            size={normalize(25)}
            color={Colors.headerBackground}
          />
        </View>
      )}
    </View>
  );
};

const OuterContainer = styled.View`
  width: ${normalize(40)}px;
  height: ${normalize(40)}px;
  border-radius: ${normalize(8)}px;
  border-width: ${normalize(3)}px;
  border-color: ${Colors.headerBackground};
  justify-content: center;
  align-items: center;
`;
const InnerContainer = styled.View`
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(8)}px;
  border-width: ${normalize(3)}px;
  border-color: ${Colors.primaryColor};
  justify-content: center;
  align-items: center;
`;
const styles = StyleSheet.create({
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    width: normalize(30),
    height: normalize(30),
    borderRadius: normalize(5),
  },
});
