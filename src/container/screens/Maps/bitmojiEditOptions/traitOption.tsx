import React, { FC } from 'react';

import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { ImageComponent } from '../../../../components/Image';
import { normalize } from '../../../../lib/globals';

interface Props {
  index: number;
  AddTraits: any;
  item: any;
  length: any;
  name: any;
  style: any;
  gender: any;
  isSelected: any;
}

export const TraitOptionItemCard: FC<Props> = ({
  index,
  AddTraits,
  item,
  length,
  name,
  style,
  gender,
  isSelected,
}) => {
  const HandlePress = () => {
    AddTraits(item.value);
  };

  return (
    <ImageContainer
      style={{
        backgroundColor: isSelected ? Colors.primaryColor : Colors.white,
        borderWidth: isSelected ? 1 : 0,
        width: length == 2 ? '48.5%' : '31.9%',
      }}
      onPress={() => {
        HandlePress();
      }}>
      <ImageComponent
        key={index}
        source={{
          uri:
            name == 'body'
              ? `https://preview.bitmoji.com/avatar-builder-v3/preview/body?scale=3&style=${style}&gender=${gender}&${name}=${item.value}`
              : `https://preview.bitmoji.com/avatar-builder-v3/preview/head?scale=3&style=${style}&gender=${gender}&${name}=${item.value}`,
        }}
        style={styles.image}
        resizeMode="contain"
      />
    </ImageContainer>
  );
};

const ImageContainer = styled.TouchableOpacity`
  height: ${normalize(120)}px;
  margin-top: 1.4%;
  margin-right: 0.7%;
  margin-left: 0.7%;
  padding: ${normalize(10)}px;
  border-radius: ${normalize(10)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const styles = StyleSheet.create({
  image: {
    width: '90%',
    height: '90%',
  },
});

export const TraitOptionItem = React.memo(TraitOptionItemCard);
