import React, {FC} from 'react';

import {FlatList} from 'react-native';

import {BottomOption} from './bottomOption';
interface ActiveTabProps{
    key:string;
    options?:OptionsProps[]
  }

interface OptionsProps {
  value: string;
}

interface ItemProps {
  key: string;
  option?: OptionsProps[];
}

interface Props {
  tabOptions: ItemProps[];
  activeTab: ActiveTabProps;
  setActiveTab: React.Dispatch<React.SetStateAction<ActiveTabProps>>;
}

export const BottomMainOption: FC<Props> = ({
  tabOptions,
  activeTab,
  setActiveTab,
}) => {

  const images = [
    {
      image: require('../../../../assets/images/tritsImages/beard.png'),
      name: 'beard',
    },
    {
      image: require('../../../../assets/images/tritsImages/beard.png'),
      name: 'beard_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/blushing.png'),
      name: 'blush_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/brow.png'),
      name: 'brow',
    },
    {
      image: require('../../../../assets/images/tritsImages/brow.png'),
      name: 'brow_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/cheek.png'),
      name: 'cheek_details',
    },
    {
      image: require('../../../../assets/images/tritsImages/eye.png'),
      name: 'eye_details',
    },
    {
      image: require('../../../../assets/images/tritsImages/eye.png'),
      name: 'pupil_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/eyeshadow.png'),
      name: 'eyeshadow_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/glasses.png'),
      name: 'glasses',
    },
    {
      image: require('../../../../assets/images/tritsImages/hair.png'),
      name: 'hair',
    },
    {
      image: require('../../../../assets/images/tritsImages/hair.png'),
      name: 'hair_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/hat.png'),
      name: 'hat',
    },
    {
      image: require('../../../../assets/images/tritsImages/faceLines.png'),
      name: 'face_lines',
    },
    {
      image: require('../../../../assets/images/tritsImages/lipstick.png'),
      name: 'lipstick_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/mouth.png'),
      name: 'mouth',
    },
    {
      image: require('../../../../assets/images/tritsImages/muscles.png'),
      name: 'body',
    },
    {
      image: require('../../../../assets/images/tritsImages/nose.png'),
      name: 'nose',
    },
    {
      image: require('../../../../assets/images/tritsImages/outfit.png'),
      name: 'outfits',
    },
    {
      image: require('../../../../assets/images/tritsImages/proportion.png'),
      name: 'face_proportion',
    },
    {
      image: require('../../../../assets/images/tritsImages/skin.png'),
      name: 'skin_tone',
    },
    {
      image: require('../../../../assets/images/tritsImages/eyelash.png'),
      name: 'eyelash',
    },
    {
      image: require('../../../../assets/images/tritsImages/breast.png'),
      name: 'breast',
    },
  ];

  const _renderData = ({item, index}) => {
 
    let traitImg = '';
    images.map((img) => {
      if (img.name.toLowerCase() == item.key.toLowerCase()) {
        traitImg = img.image;
      }
    });

    let isSelected = false;
    if (activeTab.key == item.key) {
      isSelected = true;
    }

    if (item.key.toLowerCase() != 'breast') {
      return (
        <BottomOption
          item={item}
          index={index}
          traitImg={traitImg}
          setActiveTab={setActiveTab}
          isSelected={isSelected}
        />
      );
    }
  };

  return (
    <FlatList
      data={tabOptions}
      horizontal={true}
      keyExtractor={(item, index) => index.toString()}
      showsHorizontalScrollIndicator={false}
      renderItem={_renderData}
    />
  );
};

export const BottomMainOptionComponent = React.memo(BottomMainOption);
