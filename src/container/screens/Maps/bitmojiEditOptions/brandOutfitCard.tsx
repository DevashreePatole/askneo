import React, { FC } from 'react';

import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { TouchItem } from '../../../../components';
import { ImageComponent } from '../../../../components/Image';
import { normalize } from '../../../../lib/globals';

interface OutfitIndexProps {
  mainIndex: number;
  secIndex: number;
}

interface Props {
  mainIndex: number;
  index: number;
  setSelectedOutfit: React.Dispatch<React.SetStateAction<string | number>>;
  setSelectedOutfitIndex: React.Dispatch<
    React.SetStateAction<OutfitIndexProps | undefined>
  >;
  item: any;
  isSelected: boolean;
}

export const BrandOutfitCardComponent: FC<Props> = ({
  mainIndex,
  index,
  setSelectedOutfit,
  setSelectedOutfitIndex,
  item,
  isSelected,
}) => {

  return (
    <ImageContainer
      style={{
        borderWidth: isSelected ? 1 : 0,
        backgroundColor: isSelected ? Colors.primaryColor : Colors.white,
      }}
      onPress={() => {
        setSelectedOutfit(item.id);
        setSelectedOutfitIndex({
          mainIndex: mainIndex,
          secIndex: index,
        });
      }}>
      <ImageComponent
        key={index}
        source={{ uri: item.image }}
        style={styles.image}
        resizeMode="contain"
      />
    </ImageContainer>
  );
};

const ImageContainer = styled(TouchItem)`
  width: 32%;
  height: ${normalize(120)}px;
  margin-top: 2%;
  background-color: ${Colors.white};
  margin-right: 2%;
  padding: ${normalize(10)}px;
  border-radius: ${normalize(10)}px;
`;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
});

export const BrandOutfitCard = React.memo(BrandOutfitCardComponent);
