import React, { FC, useEffect, useState } from 'react';

import { FlatList, View } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { Text } from '../../../../components';
import { textSizes } from '../../../../lib/globals';
import { TraitOptionItem } from './traitOption';

interface Props {
  name: string;
  options: any;
  traitsOptions: any;
  setTraitsOptions: any;
  gender: number;
  style: number;
}

export const TraitOptionCardComponent: FC<Props> = ({
  name,
  options,
  traitsOptions,
  setTraitsOptions,
  gender = 1,
  style = 4,
}) => {
  const [currentSelected, setCurrentSelected] = useState({});

  useEffect(() => {
    traitsOptions.map((op) => {
      if (op.name == name) {
        setCurrentSelected(op);
      }
    });
  }, [traitsOptions]);

  const AddTraits = (val) => {
    let found = false;

    const traitsArray = [...traitsOptions];

    const newArray = traitsArray.map((op) => {
      if (op.name == name) {
        op.value = val;
        found = true;
        setCurrentSelected(op);
      }

      return op;
    });

    if (found == false) {
      const data = [...traitsArray];
      data.push({
        name: name,
        value: val,
      });
      setTraitsOptions(data);
    } else {
      setTraitsOptions(newArray);
    }
  };

  const _renderData = ({ item, index }) => {
    let isSelected = false;

    if (item.value == currentSelected.value) {
      isSelected = true;
    }

    return (
      <TraitOptionItem
        index={index}
        AddTraits={AddTraits}
        item={item}
        length={options.length}
        name={name}
        style={style}
        gender={gender}
        isSelected={isSelected}
      />
    );
  };

  const _keyExtractor = (item, index) => item.value.toString();

  return (
    <View style={{ width: '100%' }}>
      <OptionTitle>
        <Text
          fontWeight="500"
          style={{ textTransform: 'capitalize' }}
          size={textSizes.h9}>
          {name}
        </Text>
      </OptionTitle>
      <View style={{ height: '91%' }}>
        <FlatList
          data={options}
          renderItem={_renderData}
          numColumns={3}
          keyExtractor={_keyExtractor}
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

const OptionTitle = styled.View`
  width: 100%;
  height: 8%;
  background-color: ${Colors.white};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const TraitOptionCard = React.memo(TraitOptionCardComponent);
