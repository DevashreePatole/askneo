import React, { FC, useEffect, useState } from 'react';

import { FlatList, View } from 'react-native';
import libmoji from 'libmoji';
import styled from 'styled-components/native';

import { Text } from '../../../../components';
import { textSizes } from '../../../../lib/globals';
import { Colors } from '../../../../assets/styles/color';
import { BrandOutfitCard } from './brandOutfitCard';

interface OutfitIndexProps {
  mainIndex: number;
  secIndex: number;
}

interface Props {
  selectedOutfitIndex: OutfitIndexProps | undefined;
  setSelectedOutfitIndex: React.Dispatch<
    React.SetStateAction<OutfitIndexProps | undefined>
  >;
  setSelectedOutfit: React.Dispatch<React.SetStateAction<string | number>>;
  gender: number;
}

export const BrandOutfitList: FC<Props> = ({
  selectedOutfitIndex,
  setSelectedOutfitIndex,
  setSelectedOutfit,
  gender,
}) => {
  const [outfit, setOutfit] = useState([]);

  useEffect(() => {
    let res;
    if (gender == 1) {
      res = libmoji.getBrands('male');
    } else {
      res = libmoji.getBrands('female');
    }

    setOutfit(res);
  }, []);

  const _keyExtractor = (item, index) => index.toString();

  const _renderData = (item, index, mainIndex) => {
    let isSelected = false;

    if (
      selectedOutfitIndex?.mainIndex == mainIndex &&
      selectedOutfitIndex?.secIndex == index
    ) {
      isSelected = true;
    }

    return (
      <BrandOutfitCard
        mainIndex={mainIndex}
        index={index}
        setSelectedOutfit={setSelectedOutfit}
        setSelectedOutfitIndex={setSelectedOutfitIndex}
        item={item}
        isSelected={isSelected}
      />
    );
  };

  const _renderData2 = ({ item, index }) => {
    const mainIndex = index;
    return (
      <FlatList
        data={item.outfits}
        renderItem={({ item, index }) => _renderData(item, index, mainIndex)}
        numColumns={3}
        keyExtractor={_keyExtractor}
        alwaysBounceVertical={false}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    );
  };

  return (
    <View style={{ width: '100%' }}>
      <OptionTitle>
        <Text
          fontWeight="500"
          style={{ textTransform: 'capitalize' }}
          size={textSizes.h9}>
          {'Outfits'}
        </Text>
      </OptionTitle>
      <View style={{ height: '91%' }}>
        <FlatList
          data={outfit}
          renderItem={_renderData2}
          numColumns={1}
          keyExtractor={_keyExtractor}
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

const OptionTitle = styled.View`
  width: 100%;
  height: 8%;
  background-color: ${Colors.white};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const BrandOutfit = React.memo(BrandOutfitList);
