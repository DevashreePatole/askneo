import React, { FC } from 'react';

import { TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { Text } from '../../../../components';
import { ImageComponent } from '../../../../components/Image';
import { normalize } from '../../../../lib/globals';

interface ActiveTabProps {
    key: string;
    options?: OptionsProps[];
}

interface OptionsProps {
    value: string;
}

interface ItemProps {
    key: string;
    option: OptionsProps[];
}

interface Props {
    item: ItemProps;
    index: number;
    traitImg: any;
    setActiveTab: React.Dispatch<React.SetStateAction<ActiveTabProps>>;
    isSelected: boolean;
}

export const BottomOptionComponent: FC<Props> = ({
    item,
    index,
    traitImg,
    setActiveTab,
    isSelected,
}) => {
    const handleClick = React.useCallback((item, index) => {
        setActiveTab({ ...item, index: index - 1 });
    }, []);

    return (
        <TouchableOpacity
            style={{
                height: '100%',
                backgroundColor: isSelected ? Colors.primaryColor : Colors.white,
                borderRadius: normalize(100),
                marginRight: normalize(5),
            }}
            key={index}
            onPress={() => {
                handleClick(item, index);
            }}>
            <TraitOptionView>
                {traitImg == '' ? (
                    <Text align="center">{item.key.toUpperCase()}</Text>
                ) : (
                    <ImageComponent
                        source={traitImg}
                        style={{ width: normalize(30), height: normalize(30) }}
                    />
                )}
            </TraitOptionView>
        </TouchableOpacity>
    );
};

const TraitOptionView = styled.View`
  padding: ${normalize(10)}px;
  border-color: ${Colors.darkGray};
  border-radius: ${normalize(100)}px;
  border-width: 1px;
  height: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const BottomOption = React.memo(BottomOptionComponent);
