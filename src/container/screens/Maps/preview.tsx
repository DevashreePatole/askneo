import React, { FC } from 'react';

import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { Container, Icon, TouchItem } from '../../../components';
import Button from '../../../components/Button';
import { ImageComponent } from '../../../components/Image';
import { normalize } from '../../../lib/globals';

interface Props {
  navigation: any;
  route: any;
}

export const PreviewBitmoji: FC<Props> = ({ navigation, route }) => {
  return (
    <ScreenContainer>
      <ImageComponent
        source={{ uri: route.params.previewImage }}
        style={{ width: '100%', height: '70%' }}
        resizeMode="contain"
      />
      <Button
        title="Apply"
        onClick={() => {
          navigation.navigate('Map', {
            selectedImage: route.params.previewImage,
          });
        }}
        style={{ marginTop: normalize(30) }}
      />

      <BackButton
        onPress={() => {
          navigation.goBack();
        }}>
        <BackContainer>
          <Icon
            type="AntDesign"
            name="back"
            size={normalize(30)}
            color={Colors.white}
          />
        </BackContainer>
      </BackButton>
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${Colors.white};
`;

const BackContainer = styled.View`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.transparent};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  top: ${normalize(30)};
  left: 0px;
  border-radius: ${normalize(50)};
`;

const BackButton = styled(TouchItem)`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  position: absolute;
  top: ${normalize(20)}px;
  left: ${normalize(20)}px;
`;
