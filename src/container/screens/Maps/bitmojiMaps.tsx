import React, { FC, useRef, useState } from 'react';

import styled from 'styled-components/native';
import {
  View,
  StyleSheet,
  Image,
  FlatList,
} from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import RBSheet from 'react-native-raw-bottom-sheet';

import { Container, Text, TouchItem } from '../../../components';
import { MapCard } from './card';
import { ProfileModal } from './ProfileModal';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';
import Button from '../../../components/Button';

export const BitMojiMap: FC = ({ navigation, route }) => {
  const [gender, setGender] = useState(1);
  const [markers, setMarkers] = useState([
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvGbfnYcCWLB505FNa8JQIJ77h_hN5W_SXKA&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      title: 'Police Station',
      coordinates: {
        latitude: 19.418677662051042,
        longitude: 72.8061704814915,
      },
      designation: 'React Native Developer',
      bitMoji: require('../../../assets/images/bitmojiImages/BitMoji-7.png'),
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUHee9HY50WTW0v8Zkq8BtUohwYhZA87ozWA&usqp=CAU',
      employeeName: 'Vertika Verma',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.41838860173265,
        longitude: 72.81178371152534,
      },
      designation: 'Angular JS Developer',
      bitMoji: require('../../../assets/images/bitmojiImages/BitMoji-13.png'),
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-51Utmw56FMBsmHRdRVn8awPHOdTeu0Qsiw&usqp=CAU',
      employeeName: 'AkashLoad Singh',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.420888,
        longitude: 72.814673,
      },
      designation: 'Node JS Developer',
      bitMoji: require('../../../assets/images/bitmojiImages/BitMoji-14.png'),
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRx_FCSvksjoFE446Ytf_OVr7xGsLw0VcTrRg&usqp=CAU',
      employeeName: 'Diksha Chaudhary',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.42343,
        longitude: 72.810703,
      },
      designation: 'React Developer',
      bitMoji: require('../../../assets/images/bitmojiImages/BitMoji-11.png'),
    },
  ]);
  const [location, setLocation] = useState({
    latitude: 19.4158279,
    longitude: 72.810004,
  });
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [data, setdata] = useState({
    image: '',
    name: '',
    designation: '',
  });
  const refRBSheet = useRef<RBSheet>(null);
  const [selectedImage, setImage] = useState('');

  const [genderData, setGenderData] = useState([
    {
      image:
        'https://preview.bitmoji.com/avatar-builder-v3/preview/head?scale=3&style=4&gender=1',
    },
    {
      image:
        'https://preview.bitmoji.com/avatar-builder-v3/preview/head?scale=3&style=4&gender=2',
    },
  ]);

  const _renderData = ({ item, index }) => {
    return (
      <GenderContainer
        onPress={() => {
          if (index == 0) {
            setGender(1);
          } else {
            setGender(2);
          }
        }}>
        <GenderImageContainer
          style={{
            backgroundColor:
              gender == index + 1 ? Colors.primaryColor : Colors.lightGray,
          }}>
          <Image
            key={index}
            source={{ uri: item.image }}
            style={{ width: '70%', height: '70%' }}
            resizeMode="contain"
          />
        </GenderImageContainer>
      </GenderContainer>
    );
  };

  const _keyExtractor = (item, index) => index.toString();

  return (
    <ScreenContainer style={styles.container} loading={loading}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker
          coordinate={{
            latitude: location.latitude,
            longitude: location.longitude,
          }}>
          {route?.params?.selectedImage != undefined ? (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image
                source={{ uri: route?.params?.selectedImage }}
                style={{ width: normalize(90), height: normalize(90) }}
                resizeMode="contain"
              />
            </View>
          ) : (
            <Image
              source={require('../../../assets/images/location.png')}
              style={{ width: normalize(50), height: normalize(50) }}
            />
          )}
        </Marker>
        {markers.map((marker, index) => (
          <Marker
            key={index}
            coordinate={marker.coordinates}
            onPress={() => {
              setModal(true);
              setdata((val) => {
                return {
                  ...val,
                  image: marker.profileImage,
                  name: marker.employeeName,
                  designation: marker.designation,
                };
              });
            }}>
            <MapCard
              {...marker}
              profileImage={marker.profileImage}
              employeeName={marker.employeeName}
              bitmoji={marker.bitMoji}
            />
          </Marker>
        ))}
      </MapView>
      {modal && (
        <ProfileModal
          visibility={modal}
          handleVisibility={() => {
            setModal(false);
          }}
          profileImage={data.image}
          employeeName={data.name}
          designation={data.designation}
        />
      )}
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={customStyles}>
        <View>
          <Text fontWeight="500" size={textSizes.h8}>
            Select A Gender
          </Text>
          <View style={{ marginTop: normalize(10) }}>
            <FlatList
              data={genderData}
              renderItem={_renderData}
              numColumns={3}
              keyExtractor={_keyExtractor}
              alwaysBounceVertical={false}
              showsVerticalScrollIndicator={false}
            />
          </View>
          <View style={{ marginTop: normalize(40) }}>
            <Button
              title="Next"
              onClick={() => {
                refRBSheet.current?.close();
                navigation.navigate('CustomizaBitmoji', {
                  gender: gender,
                });
              }}
              style={{ width: normalize(150), height: normalize(40) }}
            />
          </View>
        </View>
      </RBSheet>
      <BitMojiContainer
        onPress={() => {
          refRBSheet.current?.open();
          // navigation.navigate('CustomizaBitmoji')
        }}>
        <ImageContainer>
          {selectedImage != '' ? (
            <Image
              source={selectedImage}
              style={{ width: normalize(40), height: normalize(40) }}
            />
          ) : (
            <Image
              source={require('../../../assets/images/location.png')}
              style={{ width: normalize(30), height: normalize(30) }}
            />
          )}
        </ImageContainer>
      </BitMojiContainer>
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
`;

const BitMojiContainer = styled(TouchItem)`
  position: absolute;
  right: ${normalize(15)}px;
  bottom: ${normalize(20)}px;
`;

const ImageContainer = styled.View`
  width: ${normalize(50)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.white};
  border-radius: ${normalize(60)}px;
  justify-content: center;
  align-items: center;
`;

const GenderContainer = styled(TouchItem)`
  width: 50%;
  height: ${normalize(150)}px;
  margin-top: 2%;
  background-color: transparent;
  padding: ${normalize(10)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const GenderImageContainer = styled.View`
  width: 100%;
  height: 100%;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-radius: ${normalize(10000)}px;
`;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const customStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    backgroundColor: Colors.white,
    borderTopRightRadius: normalize(18),
    borderTopLeftRadius: normalize(18),
    borderColor: '#d6d6d6',
    borderWidth: normalize(1),
    height: normalize(320),
  },
})