import React, { FC, useEffect, useState } from 'react';

import styled from 'styled-components/native';
import { StyleSheet, PermissionsAndroid, Platform } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Toast from 'react-native-simple-toast';

import { Container } from '../../../components';
import { MapCard } from './card';
import { ProfileModal } from './ProfileModal';

export const EmployeeMap: FC = () => {
  const [markers, setMarkers] = useState([
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvGbfnYcCWLB505FNa8JQIJ77h_hN5W_SXKA&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      title: 'Police Station',
      coordinates: {
        latitude: 19.418677662051042,
        longitude: 72.8061704814915,
      },
      designation: 'React Native Developer',
      bitMoji: '',
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTUHee9HY50WTW0v8Zkq8BtUohwYhZA87ozWA&usqp=CAU',
      employeeName: 'Vertika Verma',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.41838860173265,
        longitude: 72.81178371152534,
      },
      designation: 'Angular JS Developer',
      bitMoji: '',
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-51Utmw56FMBsmHRdRVn8awPHOdTeu0Qsiw&usqp=CAU',
      employeeName: 'AkashLoad Singh',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.417015345696363,
        longitude: 72.81070898044717,
      },
      designation: 'Node JS Developer',
      bitMoji: '',
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRx_FCSvksjoFE446Ytf_OVr7xGsLw0VcTrRg&usqp=CAU',
      employeeName: 'Diksha Chaudhary',
      title: 'State Bank Of India',
      coordinates: {
        latitude: 19.41906,
        longitude: 72.812653,
      },
      designation: 'React Developer',
      bitMoji: '',
    },
  ]);
  const [location, setLocation] = useState({
    latitude: 19.4158279,
    longitude: 72.810004,
  });
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [data, setdata] = useState({
    image: '',
    name: '',
    designation: '',
  });

  useEffect(() => {
    //getLocation();
  }, []);

  const askPermision = async () => {
    setLoading(true);
    if (Platform.OS == 'ios') {
      getLocation();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getLocation();
        } else {
          Toast.show('Location Permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  const getLocation = () => {
    Geolocation.getCurrentPosition(
      (info) => {
        console.log('info', info);
        var coords = info.coords;
        setLocation((prev) => {
          return {
            ...prev,
            latitude: coords.latitude,
            longitude: coords.longitude,
          };
        });
        setLoading(false);
      },
      (error) => {
        console.log('error', error);
        Toast.show(error.message, Toast.SHORT, ['UIAlertController']);
      },
    );
  };
  
  return (
    <ScreenContainer style={styles.container} loading={loading}>
      {/* {console.log('-----coordinates----', location)} */}
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={{
          latitude: location.latitude,
          longitude: location.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker
          coordinate={{
            latitude: location.latitude,
            longitude: location.longitude,
          }}
          title={'Me'}
        />
        {markers.map((marker, index) => (
          <Marker
            key={index}
            coordinate={marker.coordinates}
            onPress={() => {
              setModal(true);
              setdata((val) => {
                return {
                  ...val,
                  image: marker.profileImage,
                  name: marker.employeeName,
                  designation: marker.designation,
                };
              });
            }}>
            <MapCard
              {...marker}
              profileImage={marker.profileImage}
              employeeName={marker.employeeName}
            />
          </Marker>
        ))}
      </MapView>
      {modal && (
        <ProfileModal
          visibility={modal}
          handleVisibility={() => {
            setModal(false);
          }}
          profileImage={data.image}
          employeeName={data.name}
          designation={data.designation}
        />
      )}
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
`;
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
