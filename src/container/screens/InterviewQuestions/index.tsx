import React, {FC, useCallback, useEffect, useState} from 'react';

import {FlatList, Platform} from 'react-native';
import styled from 'styled-components/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {Container, Text, TouchItem} from '../../../components';
import {Colors} from '../../../assets/styles/color';
import {normalize, textSizes} from '../../../lib/globals';
import {RootStackParamList} from '../../navigators/stack/drawerStack';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'InterviewQuestions'>;
}

export const InterviewQuestions: FC<Props> = ({navigation}) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  const technologyList = [
    {
      title: 'HTML',
    },
    {
      title: 'CSS',
    },
    {
      title: 'JavaScript',
    },
    {
      title: 'React JS',
    },
    {
      title: 'React Native',
    },
    {
      title: 'Angular',
    },
    {
      title: 'Node JS',
    },
    {
      title: 'Ionic',
    },
    {
      title: 'Android',
    },
    {
      title: 'Python',
    },
  ];

  const _keyExtractor = (item, index) => index.toString();

  const renderList = ({item}) => {
    return (
      <CardContainer
        onPress={() => {
          navigation.navigate('TabQuestions', {title: item.title});
        }}>
        <Text fontWeight="500" size={textSizes.h8}>
          {item.title}
        </Text>
      </CardContainer>
    );
  };
  return (
    <ScreenContainer loading={loading} withKeyboard={false}>
      {technologyList.length > 1 ? (
        <FlatList
          horizontal={false}
          numColumns={2}
          data={technologyList}
          keyExtractor={_keyExtractor}
          renderItem={renderList}
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}
        />
      ) : (
        <CardContainer onPress={() => {}}>
          <Text fontWeight="500" size={textSizes.h8}>
            {technologyList[0].title}
          </Text>
        </CardContainer>
      )}
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  padding-top: ${normalize(5)}px;
  padding-bottom: ${normalize(10)}px;
`;

const CardContainer = styled(TouchItem)`
  width: 45%;
  height: ${Platform.OS == 'ios' ? normalize(140) : normalize(120)}px;
  background-color: ${Colors.primaryColor};
  border-radius: ${normalize(20)}px;
  justify-content: center;
  margin-top: ${normalize(14)}px;
  margin-right: ${normalize(7)}px;
  margin-left: ${normalize(7)}px;
`;

