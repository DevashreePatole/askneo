import React, { FC, useState } from 'react';

import { Text, FlatList, StyleSheet, Platform } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../../assets/styles/color';
import { AddItem, ComingSoon, TouchItem } from '../../../../components';
import { Icon } from '../../../../components/Icon';
import { normalize, textSizes } from '../../../../lib/globals';
import { RootStackParamList } from '../../../navigators/stack/drawerStack';

interface Props {
  navigation:StackNavigationProp<RootStackParamList,'TabQuestions'>
}

export const ClientCards: FC<Props> = ({ navigation }) => {
  const clientList = [
    {
      name: 'Halight',
    },
    {
      name: 'Vanso',
    },
    {
      name: 'IDFC First Bank',
    },
    {
      name: 'IIFL',
    },
    {
      name: 'Cryptopill - HealthCare',
    },
    {
      name: 'Mahindra & Mahindra',
    },
    {
      name: 'Halight',
    },
    {
      name: 'Vanso',
    },
    {
      name: 'IDFC First Bank',
    },
    {
      name: 'IIFL',
    },
    {
      name: 'Cryptopill - HealthCare',
    },
    {
      name: 'Mahindra & Mahindra',
    },
  ];

  const _keyExtractor = (item, index) => index.toString();

  const _renderList = ({ item }) => {
    return (
      <CardContainer
        onPress={() => {
          navigation.navigate('QuestionList', { clientName: item.name });
        }}
        style={styles.shadowStyle}>
        <CardView>
          <Text style={styles.clientName}>{item.name}</Text>
          <Icon type="Feather" name="arrow-right" size={normalize(22)} />
        </CardView>
      </CardContainer>
    );
  };

  return (
    <ScreenContainer>
      <FlatList
        data={clientList}
        renderItem={_renderList}
        keyExtractor={_keyExtractor}
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}
      />
      <AddItem
        navigation={navigation}
        styles={{ bottom: normalize(10), right: normalize(15) }}
      />

    </ScreenContainer>
  );
};

const ScreenContainer = styled.View`
  background-color: ${Colors.lightGray};
  padding-top: ${normalize(15)}px;
  padding-bottom: ${normalize(10)}px;
`;

const CardContainer = styled(TouchItem)`
  margin-left: ${normalize(28)}px;
  margin-right: ${normalize(28)}px;
  margin-bottom: ${normalize(14)}px;
  background-color: ${Colors.white};
  border-radius: ${normalize(18)}px;
`;

const CardView = styled.View`
  padding: ${normalize(14)}px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const styles = StyleSheet.create({
  shadowStyle: {
    borderColor: '#ddd',
    borderBottomWidth: 1,
    shadowColor: Colors.darkGray,
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: normalize(5),
    elevation: Platform.OS != 'ios' ? normalize(10) : 0,
    paddingHorizontal: normalize(10),
  },
  clientName: {
    paddingLeft: normalize(20),
    fontSize: textSizes.h8,
    fontWeight: '500',
  },
});
