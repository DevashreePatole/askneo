import React, { FC, useEffect, useRef, useState } from 'react';

import { View, Text, StyleSheet, Platform, Animated } from 'react-native';
import styled from 'styled-components/native';
import moment from 'moment';
import RBSheet from 'react-native-raw-bottom-sheet';

import { AnimateIcon, AnimatedIcon } from '../../../../components/AnimatedIcon';
import { Colors } from '../../../../assets/styles/color';
import { AddComment, TouchItem } from '../../../../components';
import { List } from './list';
import { normalize } from '../../../../lib/globals';
import { Icon } from '../../../../components/Icon';

interface DataProps {
  clientName: string;
  profileImage: string;
  employeeName: string;
  questions: string[];
  likes: string;
  postedTime: string;
  experience: string;
  tips: string[];
}

interface Props {
  data: DataProps;
  index: number;
}

export const QuestionCard: FC<Props> = ({ data, index, ...props }) => {
  const refRBSheet = useRef<RBSheet>(null);
  const commentRef = useRef<RBSheet>(null);
  const uploadedDate = moment(data.postedTime).format('ll');
  const uploadedTime = moment(data.postedTime).format('h:mm a');
  const numOfQuestions = data.questions.length;
  const [visible, setVisible] = useState(false);
  const [disabled, setDisable] = useState(false);
  const [like, setLike] = useState(parseInt(data.likes));
  const currentValue = new Animated.Value(1);

  useEffect(() => {
    if (visible == true) {
      AnimateIcon(currentValue, () => {
        setVisible(false);
      });
    }
  }, [visible]);
  
  const handleLikeClick = () => {
    if (!disabled) {
      setLike((prev) => prev + 1);
      setDisable(true);
      setVisible(true);
    } else {
      setLike((prev) => prev - 1);
      setDisable(false);
      setVisible(false);
    }
  };

  if (index == 0) {
    return <CardContainerHeader />;
  }
  return (
    <View>
      <CardContainer>
        <CardHeader>
          <ImageContainer source={{ uri: `${data.profileImage}` }} />
          <ProfileContainer>
            <Text>{data.employeeName}</Text>
            <Text style={styles.profileText}>
              {uploadedDate} at {uploadedTime}
            </Text>
          </ProfileContainer>
        </CardHeader>
        <QuestionContainer>
          {data.questions.map((item, index) => {
            if (index >= 4) {
              return null;
            } else {
              return (
                <View
                  key={index}
                  style={{ paddingTop: Platform.OS == 'ios' ? 8 : 4 }}>
                  <Text>{item}</Text>
                </View>
              );
            }
          })}
          {numOfQuestions > 4 ? (
            <TouchItem
              onPress={() => {
                refRBSheet.current?.open();
              }}>
              <Text style={styles.readMoreStyle}>Read More</Text>
            </TouchItem>
          ) : null}
        </QuestionContainer>

        <CardFooter>
          <Row>
            <Icon
              type="FontAwesome"
              name={disabled ? 'thumbs-up' : 'thumbs-o-up'}
              size={normalize(22)}
              color={disabled ? Colors.primaryColor : Colors.darkGray}
              onPress={handleLikeClick}
            />
            <Text
              style={{
                paddingTop: normalize(4),
                paddingLeft: normalize(4),
                color: Colors.darkGray,
              }}>
              {like}
            </Text>
          </Row>

          <Icon
            type="FontAwesome"
            name="comments"
            size={normalize(22)}
            color={Colors.darkGray}
            onPress={()=>{commentRef.current?.open();}}
          />
          <Icon
            type="FontAwesome"
            name="share-alt"
            size={normalize(22)}
            color={Colors.darkGray}
          />
        </CardFooter>
        {visible && (
          <AnimatedIcon
            name="thumbs-up"
            size={normalize(32)}
            color={Colors.primaryColor}
            currentValue={currentValue}
            style={{ left: '48%' }}
          />
        )}
      </CardContainer>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        animationType={'slide'}
        customStyles={customStyles}>
        <List questions={data.questions} />
      </RBSheet>
      <RBSheet
        ref={commentRef}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={customStyles}>
        <AddComment />
      </RBSheet>
    </View>
  );
};

const Row = styled.View`
  flex-direction: row;
`;

const CardContainer = styled.View`
  background-color: ${Colors.white};
  padding: ${normalize(10)}px;
  border-radius: ${normalize(8)}px;
  margin-left: ${normalize(18)}px;
  margin-right: ${normalize(18)}px;
  margin-bottom: ${normalize(18)}px;
  padding-bottom: ${normalize(10)}px;
`;

const CardHeader = styled(Row)``;

const ProfileContainer = styled.View`
  padding-left: ${normalize(10)}px;
  justify-content: center;
`;

const ImageContainer = styled.Image`
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(35)}px;
`;

const QuestionContainer = styled.View`
  width: 85%;
  margin-left: ${normalize(18)}px;
  margin-top: ${normalize(14)}px;
`;

const CardFooter = styled(Row)`
  justify-content: space-between;
  margin-top: ${normalize(14)}px;
  padding: ${normalize(5)}px;
  padding-left: ${normalize(10)}px;
  padding-right: ${normalize(10)}px;
`;

const CardContainerHeader = styled.View`
  background-color: ${Colors.lightGray};
  width: 100%;
  height: ${normalize(40)}px;
  border-top-left-radius: ${normalize(28)}px;
  border-top-right-radius: ${normalize(28)}px;
  margin-top: ${-normalize(20)}px;
`;

const styles = StyleSheet.create({
  profileText: {
    fontWeight: '200',
    fontSize: 10,
    paddingTop: normalize(2),
  },
  readMoreStyle: {
    textDecorationLine: 'underline',
    color: Colors.primaryColor,
    fontWeight: '400',
    textAlign: 'right',
    paddingTop: Platform.OS == 'ios' ? normalize(3) : 0,
  },
});

const customStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(20),
    borderTopLeftRadius: normalize(20),
    borderColor: '#d6d6d6',
    borderWidth: 1,
    height: '88%',
  },
});
