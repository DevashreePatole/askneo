import React, { FC } from 'react';

import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Platform,
  StatusBar,
} from 'react-native';
import styled from 'styled-components/native';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../../assets/styles/color';
import { TouchItem } from '../../../../components';
import { Icon } from '../../../../components/Icon';
import { normalize, textSizes } from '../../../../lib/globals';
import { RootStackParamList } from '../../../navigators/stack/drawerStack';
import { QuestionCard } from './questionCard';

interface Props {
  navigation: StackNavigationProp<RootStackParamList,'QuestionList'>
  route: RouteProp<RootStackParamList,'QuestionList'>
}

export const QuestionList: FC<Props> = ({ navigation, route }) => {
  const questionList = [
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: '',
    },
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: '',
    },
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: '',
    },
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: '',
    },
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: '',
    },
  ];

  const _keyExtractor = (item, index) => index.toString();

  const _renderList = ({ item, index }) => {
    return <QuestionCard data={item} index={index} />;
  };
  
  const _renderHeader = () => {
    return (
      <View style={{ backgroundColor: Colors.primaryColor }}>
        <HeaderContainer>
          <HeaderView>
            <Iconcontainer
              onPress={() => {
                navigation.goBack();
              }}>
              <Icon
                type="MaterialIcons"
                name="keyboard-arrow-left"
                size={normalize(28)}
                color={Colors.headerBackground}
              />
            </Iconcontainer>
            <View style={styles.viewStyle}>
              <Text style={styles.textStyle}>{route.params.clientName}</Text>
            </View>
          </HeaderView>
        </HeaderContainer>
      </View>
    );
  };
  return (
    <ScreenContainer>
      {Platform.OS == 'ios' && (
        <StatusBar
          backgroundColor="#b3e6ff"
          barStyle="dark-content"
          hidden={true}
        />
      )}
      <FlatList
        data={questionList}
        renderItem={_renderList}
        keyExtractor={_keyExtractor}
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}
        ListHeaderComponent={_renderHeader}
        stickyHeaderIndices={[1]}
      />
    </ScreenContainer>
  );
};
const ScreenContainer = styled.View`
  flex: 1px;
`;

const HeaderContainer = styled.View`
  padding-top: ${Platform.OS == 'ios' ? normalize(50) : normalize(18)}px;
  padding-bottom: ${normalize(40)}px;
`;
const HeaderView = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-left: ${normalize(30)}px;
`;
const Iconcontainer = styled(TouchItem)`
  width: ${normalize(40)}px;
  height: ${normalize(40)}px;
  border-radius: ${normalize(40)}px;
  background-color: ${Colors.white};
  flex-direction: row;
  justify-content: center;
  align-items: center;
  opacity: 0.4;
`;

const styles = StyleSheet.create({
  viewStyle: {
    alignItems: 'center',
    width: '70%',
  },
  textStyle: {
    fontWeight: '500',
    fontSize: textSizes.h6,
  },
});
