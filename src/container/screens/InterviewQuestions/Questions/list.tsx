import React, { FC } from 'react';

import { Text, FlatList } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { normalize } from '../../../../lib/globals';

interface Props {
    questions: string[];
}
export const List: FC<Props> = ({ questions, ...props }) => {
    const _keyExtractor = (item, index) => index.toString();
    
    const _renderList = ({ item, index }) => {
        return (
            <ViewContainer>
                <Text>{index + 1} .</Text>
                <Text>{item}</Text>
            </ViewContainer>
        );
    };
    return (
        <ScreenContainer>
            <FlatList
                data={questions}
                renderItem={_renderList}
                keyExtractor={_keyExtractor}
                showsVerticalScrollIndicator={false}
                alwaysBounceVertical={false}
            />
        </ScreenContainer>
    );
};
const ScreenContainer = styled.View`
  padding: ${normalize(10)}px;
`;
const ViewContainer = styled.View`
  padding-left: ${normalize(10)}px;
  justify-content: flex-start;
  height: ${normalize(50)}px;
  border-bottom-width: ${normalize(1)}px;
  border-bottom-color: ${Colors.darkGray};
  flex-direction: row;
  align-items: center;
`;
