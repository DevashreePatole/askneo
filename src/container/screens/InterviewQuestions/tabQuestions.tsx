import {StackNavigationProp} from '@react-navigation/stack';
import React, {FC, useState} from 'react';

import {useWindowDimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';

import {Colors} from '../../../assets/styles/color';
import {RootStackParamList} from '../../navigators/stack/drawerStack';
import {ClientCards} from './Questions';
import {Tips} from './Tips';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'TabQuestions'>;
}

export const TabQuestions: FC<Props> = ({navigation}) => {
  const FirstRoute = () => <ClientCards navigation={navigation} />;
  const SecondRoute = () => <Tips navigation={navigation} />;

  const layout = useWindowDimensions();

  const [index, setIndex] = useState(0);

  const [routes] = useState([
    {key: 'first', title: 'Questions'},
    {key: 'second', title: 'Tips'},
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      labelStyle={{fontWeight: '600', fontSize: 15}}
      indicatorStyle={{borderWidth: 2, borderColor: Colors.primaryColor}}
      style={{backgroundColor: Colors.headerBackground}}
    />
  );

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      renderTabBar={renderTabBar}
    />
  );
};
