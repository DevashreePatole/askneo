import React, { FC, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';

import { AddItem, ComingSoon, Container } from '../../../../components';
import { TipsCard } from './tipsCard';
import { Colors } from '../../../../assets/styles/color';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../../navigators/stack/drawerStack';

interface Props {
  navigation: StackNavigationProp<RootStackParamList,'TabQuestions'>
}

export const Tips: FC<Props> = ({ navigation }) => {
  const questionList = [
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'Cryptopill - HealthCare',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'Halight',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'IIFL',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'Vanso',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'IDFC First Bank',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
    {
      clientName: 'IDFC First Bank',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      questions: [
        'Explain React Native?',
        'List the essential components of React Native.',
        'What are the advantages of React Native?',
        'What are the disadvantages of React Native?',
      ],
      likes: '5',
      postedTime: '2021-02-06T16:34:00',
      experience: '',
      tips: [
        'You should never use Expo on a serious project, for a number of reasons.' +
        'Firstly, the entire build process is not done locally, but in Expo cloud, which means little to no control and potential server issues while building.',
        'ESLint is a fantastic extension which is extremely easy to install, set up and use, so be sure to include it in your project from the very beginning.',
      ],
    },
  ];

  const [modal, setModal] = useState(false);

  const _keyExtractor = (item, index) => index.toString();

  const _renderList = ({ item, index }) => {
    return <TipsCard data={item} />;
  };
  
  return (
    <ScreenContainer withKeyboard={false}>
      <FlatList
        data={questionList}
        keyExtractor={_keyExtractor}
        renderItem={_renderList}
        alwaysBounceVertical={false}
        showsVerticalScrollIndicator={false}
      />
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  background-color: ${Colors.lightGray};
`;
