import React, { FC, useEffect, useRef, useState } from 'react';

import { View, Text, StyleSheet, Animated } from 'react-native';
import styled from 'styled-components/native';
import moment from 'moment';

import { Colors } from '../../../../assets/styles/color';
import { AnimateIcon, AnimatedIcon } from '../../../../components/AnimatedIcon';
import { shareData } from '../../../../lib/helper';
import { Icon } from '../../../../components/Icon';
import { normalize, textSizes } from '../../../../lib/globals';
import { ImageComponent } from '../../../../components/Image';
import RBSheet from 'react-native-raw-bottom-sheet';
import { AddComment } from '../../../../components';

interface DataProps {
  clientName: string;
  profileImage: string;
  employeeName: string;
  questions: string[];
  likes: string;
  postedTime: string;
  experience: string;
  tips: string[];
}

interface Props {
  data: DataProps;
}

export const TipsCardComponent: FC<Props> = ({ data, ...props }) => {
  const [visible, setVisible] = useState(false);
  const [disabled, setDisable] = useState(false);
  const [like, setLike] = useState(parseInt(data.likes));
  const currentValue = new Animated.Value(1);
  const commentRef = useRef<RBSheet>(null);

  useEffect(() => {
    if (visible == true) {
      AnimateIcon(currentValue, () => {
        setVisible(false);
      });
    }
  }, [visible]);

  const handleLikeClick = () => {
    if (!disabled) {
      setLike((prev) => prev + 1);
      setDisable(true);
      setVisible(true);
    } else {
      setLike((prev) => prev - 1);
      setDisable(false);
      setVisible(false);
    }
  };

  const share = (data) => {
    shareData('', `${data.tips} author ${data.employeeName}`);
  };

  const uploadedDate = moment(data.postedTime).format('ll');
  const uploadedTime = moment(data.postedTime).format('h:mm a');
  console.log('------tips flatlist ----------')
  return (
    <CardContainer>
      <HeaderContainer>
        <ImageContainer source={{ uri: `${data.profileImage}` }} />
        <View style={styles.profileStyle}>
          <Text style={{ ...styles.textStyle, fontSize: textSizes.h9 }}>
            {data.employeeName}
          </Text>
          <Text
            style={{
              ...styles.textStyle,
              fontSize: textSizes.h12,
              color: Colors.darkGray,
              marginTop: normalize(2),
            }}>
            {uploadedDate} at {uploadedTime}
          </Text>
        </View>
      </HeaderContainer>
      <TipsContainer>
        {data.tips.map((item, index) => {
          return (
            <TipsView key={index}>
              <Icon
                type="MaterialCommunityIcons"
                name="feather"
                size={normalize(18)}
                style={{ paddingRight: normalize(5) }}
              />
              <Text style={{ marginRight: normalize(15) }}>{item}</Text>
            </TipsView>
          );
        })}
      </TipsContainer>
      <CardFooter>
        <View style={{ flexDirection: 'row' }}>
          <Icon
            type="FontAwesome"
            name={disabled ? 'thumbs-up' : 'thumbs-o-up'}
            size={normalize(22)}
            color={disabled ? Colors.primaryColor : Colors.darkGray}
            onPress={handleLikeClick}
          />
          <Text
            style={{
              paddingTop: normalize(4),
              paddingLeft: normalize(4),
              color: Colors.darkGray,
            }}>
            {like}
          </Text>
        </View>

        <Icon
          type="FontAwesome"
          name="comments"
          size={normalize(22)}
          color={Colors.darkGray}
          onPress={()=>{commentRef.current?.open();}}
        />
        <Icon
          type="FontAwesome"
          name="share-alt"
          size={normalize(22)}
          color={Colors.darkGray}
          onPress={() => {
            share(data);
          }}
        />
      </CardFooter>
      {visible && (
        <AnimatedIcon
          name="thumbs-up"
          size={normalize(32)}
          color={Colors.primaryColor}
          currentValue={currentValue}
        />
      )}
      <RBSheet
        ref={commentRef}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={customStyles}>
        <AddComment />
      </RBSheet>
    </CardContainer>
  );
};

const CardContainer = styled.View`
  background-color: ${Colors.white};
  padding: 10px;
  margin-top: 5px;
  margin-bottom: 10px;
  border-radius: 5px;
`;

const HeaderContainer = styled.View`
  padding: 5px;
  flex-direction: row;
  align-items: center;
`;

const ImageContainer = styled(ImageComponent)`
  width: 40px;
  height: 40px;
  border-radius: 40px;
  border-width: 1px;
  border-color: ${Colors.primaryColor};
`;

const TipsContainer = styled.View`
  padding: 10px;
`;

const TipsView = styled.View`
  flex-direction: row;
  margin-bottom: 12px;
`;

const CardFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 5px;
  padding: 5px;
  padding-left: 10px;
  padding-right: 10px;
`;
const styles = StyleSheet.create({
  textStyle: {
    fontWeight: '500',
  },
  profileStyle: {
    marginLeft: normalize(8),
  },
});
const customStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(20),
    borderTopLeftRadius: normalize(20),
    borderColor: '#d6d6d6',
    borderWidth: 1,
    height: '88%',
  },
});

export const TipsCard = React.memo(TipsCardComponent)

