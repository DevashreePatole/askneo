import React, {FC, useEffect, useState} from 'react';

import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';
import styled from 'styled-components/native';
import {ifProp} from 'styled-tools';

import {Colors} from '../../../assets/styles/color';
import {Container, Input, TouchItem, Icon} from '../../../components';
import {normalize} from '../../../lib/globals';

export const AddQuestions: FC = () => {
  const [data, setData] = useState({
    clientName: '',
    technology: '',
  });
  const [questions, setQuestions] = useState<string[]>([]);
  const [tips, setTips] = useState<string[]>([]);
  const [disabled, setDisabled] = useState(true);
  const [questionClicked, questionButton] = useState(true);
  const [tipsClicked, tipsButton] = useState(true);

  useEffect(() => {
    handleDisable();
    if (questions.length == 0) {
      questionButton(true);
    }
    if (tips.length == 0) {
      tipsButton(true);
    }
  }, [questions, tips]);

  const handleChange = (val, index, type) => {
    if (type == 'question') {
      const newarray = [...questions];
      newarray[index] = val;
      setQuestions(newarray);
    } else {
      const data = [...tips];
      data[index] = val;
      setTips(data);
    }
  };

  const handleIconPress = (index, type) => {
    if (type == 'question') {
      const newdata = [...questions];
      newdata.splice(index, 1);
      setQuestions(newdata);
    } else {
      const data = [...tips];
      data.splice(index, 1);
      setTips(data);
    }
  };

  const addItems = (type) => {
    if (type == 'question' && questionClicked) {
      const newData = [''];
      setQuestions(newData);
      questionButton(false);
    } else if (tipsClicked) {
      const data = [''];
      setTips(data);
      tipsButton(false);
    }
  };

  const handleDisable = () => {
    if (questions.length > 0 || tips.length > 0) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  return (
    <ScreenContainer withKeyboard={false}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}>
        <InputContainer>
          <Input
            label="Client Name"
            value={data.clientName}
            onChangeText={(val) => {
              setData((prev) => {
                return {...prev, clientName: val};
              });
            }}
          />
          <Input
            label="Technology"
            value={data.technology}
            onChangeText={(val) => {
              setData((prev) => {
                return {...prev, technology: val};
              });
            }}
          />
          <View style={styles.viewStyle}>
            <ViewContainer
              button={questionClicked}
              onPress={() => {
                addItems('question');
              }}
              style={{borderRightWidth: 1}}>
              <Text>Add Questions</Text>
            </ViewContainer>
            <ViewContainer
              button={tipsClicked}
              onPress={() => {
                addItems('tips');
              }}>
              <Text>Add Tips</Text>
            </ViewContainer>
          </View>

          {questions.length > 0 ? (
            <View>
              <Text style={styles.titleStyle}>Interview Questions</Text>
              {questions.map((item, index) => {
                return (
                  <Input
                    key={index}
                    placeholder={'Add question here'}
                    multiline={true}
                    value={item}
                    onChangeText={(val) => {
                      handleChange(val, index, 'question');
                    }}
                    icon="close"
                    iconSize={normalize(25)}
                    iconAction={() => {
                      handleIconPress(index, 'question');
                    }}
                    iconStyle={{top: normalize(0)}}
                  />
                );
              })}
              <TouchItem
                onPress={() => {
                  setQuestions((prev) => {
                    return [...prev, ''];
                  });
                }}>
                <AddContainer>
                  <Icon type="Entypo" name="plus" size={normalize(25)} />
                  <Text style={styles.textStyle}>Add More</Text>
                </AddContainer>
              </TouchItem>
            </View>
          ) : null}
          {tips.length > 0 ? (
            <View>
              <Text style={styles.titleStyle}>Interview Tips</Text>
              {tips.map((item, index) => {
                return (
                  <Input
                    key={index}
                    placeholder={'Add Tips here'}
                    multiline={true}
                    value={item}
                    onChangeText={(val) => {
                      handleChange(val, index, 'tips');
                    }}
                    icon="close"
                    iconSize={normalize(25)}
                    iconAction={() => {
                      handleIconPress(index, 'tips');
                    }}
                    iconStyle={{top: normalize(0)}}
                  />
                );
              })}
              <TouchItem
                onPress={() => {
                  setTips((prev) => {
                    return [...prev, ''];
                  });
                }}>
                <AddContainer>
                  <Icon type="Entypo" name="plus" size={normalize(25)} />
                  <Text style={styles.textStyle}>Add More</Text>
                </AddContainer>
              </TouchItem>
            </View>
          ) : null}
        </InputContainer>
        {disabled && (
          <ImageContainer>
            <Image
              style={{width: 300, height: 250}}
              source={{
                uri:
                  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP8AAADGCAMAAAAqo6adAAABIFBMVEXz+fXf9+f///+k67yRmJ9QWGPg9+jj+evDx8zr9e5/ypXk/eyS0qSUm6KSl6D5/PrR8Nuk2rR0p4ms2rna9uOHz5zh4+Xr7O729/hPUWF6x5BUo3BnsYDb3uGwt7zp+O6p88HH69K248Od167F5M5Phmie6bdFTFpDSVhwd4C6vsRlbHago6i43sKGx5qT4qxrx4jR6Np4fodISVt/05qDipJRcmdVXWeEkpLJ39J2eYVylod6pI+wxLt8iIteZHCXp6ObrKervbZmgHqFtpvR0dWOxaSa3bJPkmq+0sdbu3iJmJeorLOvw7qAhI5qiX5dcXFlZnVwgoBAN1OVsKGp0LZ2r49kiXqAopFil3lfpHqFnpZzroh3z5I+PlNQbGVescKPAAASHUlEQVR4nO2d+UPiSBbHBWICFAEdBZRpDsU2hGAjkQx44gUuA0077u4cu+vu//9fbFXOSuUOiWzP5vtDtwcQPu+9eu/VQdzaSpQoUaJEiRIlSpQoUaJEiRIlSpQoUaJEiRIlSpToA5XLNZtNRhP8upnb9Fv6IEFwJuUg5k9uhVyTdkLHjbDptxmLfLGrov9scZBr+mbX9CeKggCeN0fBpt94FMo5Zrv/hyBYi/67t8Da9N+3BaKgh6K/z2IQPOU7itk0y9ZWCSnA43Phcr6TNhwCpWlP5MVe1fcTInS+ok1mgVJ2xHHb29tcZ4q+8xEG0Tpf1ubGQEnqbKviUCSwWZMFShYZEztDEVhgU/git61L3OY4riNiBqALFpWtKuxEEBIbSQKlFYavqlMxfn/ezPlQ83z/+zRAqWfFR8NA+30un/als+MocsKHG6CU7VjxoQGozfB/tAFKVRvvywYQShvh/8AkWNrKVbP29CgFZDfD/0EGKG01eK7DOXgfBUAX56+46iBKfvpD8Bsu6DL+aAvn/0D/f0gnKNpmPUyS9siP548/B5Z4d+dD9+sNgMJfdZVn/AOQGsB/gE8DxI1vV/IJ/qmZ/8BVHv4Hg8V2p9Ph+IcB8GWEmKcCFa/gh9lfj8H14x/cd1R7cx3+/nE+8DZBrCOgRHn4HuJ3I+z/BDza0Oxi+94zAmLl94j+hsRXjPnPuvUPCNbLcYJXBMRZAyru/Pw028Pnv+v5H4ztrsbdexkgRv6pKz/XyHZx/jXjf2CfazgP/DgDIOvKL2SzWaFu4O+vxQ8esYvxkqjz33oZID5+V/9zvSz0v6Ql4GZqv7wW/9h4aYllJf8DIL4AcC1/I+j+7rLaUx6KVrVq/vi/HNlRAMHwPktRrP7dnWcNjI2/5OZ+FvH30hLKADn0Nph2TdYWZCzla04637VdAQR6rE1YlmJZfQB4VoD4AsC1/cvK/t9C/CqQ/B9TaEL88yPHHo92WACVY03geYmlBF6gWF69TmfuZYAY54EOvodmWS1ZieeFHl/Kmd4M4of4gVd5YfrnKUqiYOxD76MRoA2Ikedz4+OvjmzwJ8ulxBXZZTfbbaQlgpQp5NIh8FOXnRHEZiURGUCWlgIXb5ceERBfBrQdAV2+kVXUaGyR6Ykp0GHwwbwjsCvofgHxS4hfS4FcajEeuFogxgFg0wJIUlZT4/HygeRv/xwCPwVuOYGdCCwLBwHMffIo0GJPbA7u3JNAfPw5Swnklgb/8kEk8xx9HAY/BR64CStC//PI/XIQwAw4olAS4BYA3A3cnh3jAJhY/E9lDf/fWYcmHWp1B7Y/Iznr8XLoKxlgJPAruQQsQMq1DYhvGcBuCtyY6v7/6nehxpMfvq7idD3/QRVZJQl0Hppj16fHxm+XADihq/F7Nud+8W85hZ8y8etFoDN/uHR7fnzLIHY9MK/5v+fZm/jlR8NM5hVM+JQ2/EaPrvwxJgArPqe5P1ucR4R/j6JMrnoSTs9LevSNXBNgjAlAsvAb+b/o6hT/+HM5yNTWh9VGACtU03r/NXGPtNj4t7pkAhgZ+X/l6hT/Ul5YqXqwq9b5pYZ2cW68KX4yAXBLg5+PZPiDhUqJ+EVe9z8lr7CqV124XyrGdWDC/aKB3/V4Uz5FaxZesajkUbr7cauTfebH8Oea5LokFv3ZVTTD/1a7wAivfaw4CsAfRwFQjqqDryb+roG/jMb9sPXVY4vF8HkRv+7IvdRGz6+f28MXZrkGhh/N6Mf8j1b+VHpK4PUVEMX/Y1cDRFwA8ePK2MocnvtW0XgfisZPlcn4K0GAZRDn3x4179xGW6RTYPORTaU5QfQjzftd9mrstSgRQKZtL5Fd8SIlT4Vwfu6R4d1eIy56qHmHg9oWjImfOE9FR0/sfMAcoPZBJv5tbpBy6wFio4eSS950OjX4I6SH9jX8f3jIiaxd/UMjwHUOHB89jE9ptVqJSJIEC3QvsswnSyuwhxejp+vfv2H8r6+HF4eHugGEhUu7GQW9w0n1QVHfuzioVir16TSivl/Bv5TD/+L1+aUFNbvS+SetVuZldj260EzQiZff4aQ6GB9Y9nC8N2X8S/b9axvCIpn40Q9arfa1ZgGXvZD1ne9ADwY96x5WL7IAkKvrxY0CL/NTZn5kgsz1hZIDnTNgLM6H8PPH12JP0XK5bDQa3S7KhW/olFIU+Kj5u5jp+Lb80ALtkRwCHcequya+zZotSN2OpV62oh9eqlbh6K/U63IlWLLS+Ov6VRDAInfxbIBm2kb8P2E/zmRe5QhwHHhR44PBQsp67OROJY+NCR/8Izz4kae1SRB79WzizygjIBZ+Cz5IjYvWrGfVATu2xoDNZz4s0ioNmFxcmzHbh0V5A3hldj/8BcoBHafV9WjxH8WqD3qkKk/OS5g9m099kNL2h4HwjcBsvTz9crW6mmhR0brWfnEDU0DHKZAixR93fdIj9Yip+eAdozm1V+ZduSi4JL0sW2D2PMvoP9cN1Bq5VMDw+JbMD97qftnr4jSdbjzib4rZ6w/b7V1Ve4TOZH05rSn8g1crPqHZTP/y+bDz6MAffv5nqfvgreLb9UprIOF9GfPltOD1vHamgPjB4NWLPpO5aRtfXzi6P/z834J/P/WLL6mPPMDbEqZ2eub1xPypfAgGx2+/OPEbv2i9Ou4thl7/sQz+S9Yvfk83VBELgMH7sJk+cDwBivJq6b2P8h+Ne3/27M2fuXGcAITltw5+3i9+xTBU1Tikxpz1311PgcOH08PhPp2ieRzz5dohEzwb4z/TdlwFDctvwZ/7Tv0i9rWkv4Cf4X/SHzIphjdFfMumECiGucG+cWz/Qq5/r+P+JfZNXXeMPPydw18+At3OlBlgxs+0yD7AzjCtb07xH5H7U/7d3zN1SKL2AnD457ziv3y6C0Qi3znzf8O+fnbacP1w96cl03dTtTDT8vD3EEx/d22y7XPiNyWAF4fTsCHL/xruTxfN36pLYswMDf+6g5SH5obDtxkB2Zo58uOZseWQAMKVf6v7RSungw6IMplVjoIw+f6J11NP+rUZ6f3nv7Yd8M0J4DfLDEpWuPRPdhPM/G9nZMNq1Rkt85MTJCUAaHn4u2vv9J1EvL5wboRb11iqeP61UCjky4VyO4W/+1Dpn+h8mf1C4Xhn31NHe+U9G6quPA88GcLhX3GK/3odWa2cGRKIT4fcjfNEAG+NXnqAYfZ2GOaotoO991DuN7d+zHF5HwYV7akUunjTagA0NOHkp+3l/vT7ad/k35ftDieQCQEXlhpabyDF7EJ0OpU33n+44W/G32v7P7dIM2XagrW8RN2P9/DPDcunOP7z3e089ejU/meIBHBNK/zw36P1hr8p/OmjQqBjm0x+yy4A6Boc/lWbnI9p0MfDv/Ubap3AvQu/aQo4u1X56SPjcyShhr8p+zO1YKdW6X1rl9sbKMPfQ3j6a728Kiu6t27x38Zb4AWw8ofBN2V/+jjoh5KZtiXPw2mwPPzNC4cl8mEFI/3B2FcvSzu1/7KeTAnAwr/+8GfKgQ8t71iLQJFGw59s/slHDU91/m/6tAHcuwSAqQW+Hlj4169+IfiZTxb+6qLmXf3h5F9J/632K7abQc6HTPx4Czz7auEP5X58+NP7AZK/xv9laWFbDYfmuY8Nv5r+Wplr4gAJ33Y0gGlxYAwI/vWbX3onBH9BtOBVfmibwt9uEf1MTn+tGU9uZdFOKyCoAhrLwTABEPzh5v44cDj+KWWBu7Ot/t0VZawptjPvrdbsybqQSzvOgFqZ1+snrQa0rsn4D4VvTv+h+CuSxb9Ty/yxSk06UoPqbGsPzp9++u3t0WbzFDiuAMyebp7hP22Vnzbzh1z6Mpk+zPgv1OsSSZsmJ5CVSUPdNW4IynB57/+dtt06Bm8O/E83rdbNt5a2H/SN8H84fHP3Ow/FX6EsxS1L7JlKDVU8L8jWag6HDjdAA2Nb5z8/vZjsQo7/sCv/ZtPbfyjVg79et64X4D+pTkVK4290G9voZyf9d4dLgYUN/cvTM7kN+mDmD4mP84Pbbrj4r7CWLfKulumqy9VSErs6v8gL6Fe7p06tBnhoezoflQLaFP+hN37wK4ulkPzdbWsAoFvYV5biEuW7umD4v0v1mrlcOeM40SLnADbOh7nwwdz/hsXH8j/4Ot0Kx1/P/mQJgMan2ufXpV4Z+KWCvywWu1e14bB/uud0qbmZFjrfOh7ad8CY/zFruL9kvAvAp0Pxo6Mwv75ZAmC7YSqLMAKWy+LVb7PZ69UMTvyHjp+PHJjWgGycn8k8o51Gpo0yKCPzh4TfxW9S+cc0HD+c50//+NUSAOaNgfSBwP/z92f5QjPrDTID6ovbL/3flBcOd0NozT8sf33RtOwYkCvDvUsA/ByJWVfB+A2Q22xI/gOYAMfgqyUAJNO8YBrlWUnX9xOOH4zTIfnTkP8OWPcMKviByYg+I+Tn/YTkl9bkf7Scl8DW/Jaet2+JShvgr9YrEnDdNOo5ndeJXhvgP6hXRMhvDQA9E9x+GH54/rvQ/Ol6ZZVy2TReRfXxaH/vJyT/4iA0f7WCTv44BYAY4UeE/LyfAPyFHUMny9D8B/XeCXoJuwA4EE52PlQB+LcGylauckbx7SAsf7qyfEav8Lt1l+eA39t11OcffOuPz5//cf4+lHWuPj0vH68sFMyb1SdB/iiD8kcclMYpFbL/R5jdR9TbpSwloMo7H4MGD6tiEK3e1P3SIa28wO4++vfohLjZfgB8Veoa8KUYmr8qtzfggQiA+pvb0J+Y73HhLXXDZKhseTNtdAd5+iiQw+2lvB8weAvLr30SyBwArj0vuJe8iU0q1hT+/knU/NomUKj1T6XEy6TgFt8M8eh5A7u/+EnlV5YOIuRXR0C49W+ZtaccSQX3hgGWrp9VD+5+qvgv5cCAcmo4Un5lGWgN/q56Ig/cikoOqIseTV9g91PFGzUBFqLn31qTv651+CD1cEex1N2D+4eiwENg91PFf6r85Rj4c+vxHxhjHe1qeN+1Orj7Kb0A5GPgRylgDf6AHwWFtT8EvlYAhuotJtG/kfFDA6zDLwXjt7if9REPWgEYyof+ouaHBliDvxfkHkDgUSTQpBFPeVqg+C+8AYqcf6u5Bn83yD2wgECwshMwv5tIHkFQvFEKoPyxkRj4t+jw/PVHa4fveJZqTtzejZIeAGgORhPB1QRaAeijJX94ZSZq/lwY/hNFPxxbjggfH9mbAPAEGDtJgcvFhGKL/IR3SQWLIdYAKYeV6ONN8x85azdvu8tzSYZ/cTHnBUm5563ET0QnExTfsQZI4Wf2BpHhh+N3DnM4Ty2c2Xym+o50vzARjewHTSAI9ibQCiAip/cVKxSiww/J7/rrvPXPnA3M7mepiUjAOplAKwCoAdLOagZZ7/l4fpuSCham1peVJnaVz9YExR+UAvCONj7L8uXbEYZ/HPw2RypNvQ+7IpOB2QSmdFj8XSkA7ymaPkLvlQ603Pc/wU/OfBzxVRPwAi/pNlALwHCHpmsoBnbyUeKH43dfjj0j7WNufVnBlttkgqIIR4Ik3w1dLQD9Iya/AxuM43KU9OH4v3xG+gtam33/y2dNf8yO1U++E+kP3JpaX9bV/boJKBgGKA7UAtBvl3eYneNyhKU/PP/FIdR/0MDs/+dQ18XkXtnwJx5P9D6++BUbsJI4+vc/lAJQKxQK7dpZxPjh+OU70vwk8/9kulNZZ3thvUEa0fv45EcRIAjjx8HuqdoAyUvftYjvdxkpv2wDbmw2AbgrBvU/TAAw+u8eYTgB5riPrwDt1/7H+VUTGBYgeh8vfnnkTxR2+fk7Q2wFCC1YbDz/e/HLA0F3P7nqa+VnCfaHS3wxjVYKgHZ4lFmvAJB/pznXtk5iPdSU89+Fmv8uDm3171vt4b+Qu1oT4vurK/UL6eoXyG65XF5dAVK/PzpJkxAB8HeJ2/Hkz/OB9fOPss6RfnTSJ/XBtR8/EXL6AXxSHr4h6+XkC/1c017w/Av5h6UDGMDPH2rG/2Yz7f2x2PVE+/rb0e4KPgyCqCkvOkQu9KKb/Iv2QQTjIGiW9BL0+6apginXjOIP2Mtivjd2TbnmuoHANL9XdkMwEkKEAvPdet1esDj4sQPNIJf/qchJoTqpSm5YVMVemBIlSpQoUaJEiRIlSpQoUaJEiRIlSpQoUaJEiRL9P+m/kr71e+7AbgkAAAAASUVORK5CYII=',
              }}
            />
          </ImageContainer>
        )}

        <ButtonContainer>
          <Button
            disabled={disabled}
            onPress={
              disabled
                ? () => {}
                : () => {
                    console.log('submit');
                  }
            }
            style={{width: 300}}>
            <Text>Submit</Text>
          </Button>
        </ButtonContainer>
      </ScrollView>
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
`;

const InputContainer = styled.View`
  margin: ${normalize(10)}px;
`;

const ButtonContainer = styled.View`
flex-direction:row
justify-content:space-evenly
align-items:center;
margin-top:${normalize(20)}px;
`;
const Button = styled(TouchItem)<{disabled}>`
 width:${normalize(150)}px;
 height:${normalize(35)}px;
 background-color:${ifProp(
   {disabled: true},
   Colors.lightGray,
   Colors.primaryColor,
 )}
 justify-content:center
 align-items:center
 border-radius:${normalize(10)}px;
`;
const ViewContainer = styled(TouchItem)<button>`
  width:50%;
  height:${normalize(35)}
  background-color:${ifProp(
    {button: true},
    Colors.primaryColor,
    Colors.lightGray,
  )}
  justify-content:center;
  align-items:center;
`;

const AddContainer = styled.View`
flex-direction:row
align-items:center
margin-top:${normalize(10)}px;
`;
const ImageContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: ${normalize(25)}px;
  margin-bottom: ${normalize(10)}px;
`;
const styles = StyleSheet.create({
  titleStyle: {
    marginTop: normalize(20),
    fontSize: normalize(15),
    fontWeight: '500',
  },
  textStyle: {
    fontWeight: '400',
    fontSize: normalize(15),
    marginLeft: normalize(5),
  },
  viewStyle: {
    flexDirection: 'row',
    marginTop: normalize(20),
  },
});
