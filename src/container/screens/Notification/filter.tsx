import React, { FC } from 'react';

import { Text } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';

interface Props {
  onClick: () => void;
}

export const FilterNotification: FC<Props> = ({ onClick, ...props }) => {
  const list = [
    { icon: 'bell', title: 'All Notifications', iconLib: 'FontAwesome5' },
    { icon: 'blog', title: 'Blogs', iconLib: 'FontAwesome5' },
    {
      icon: 'question-answer',
      title: 'Interview Questions',
      iconLib: 'MaretialIcon',
    },
    { icon: 'users', title: 'Community', iconLib: 'FontAwesome5' },
    { icon: 'chalkboard-teacher', title: 'Questions', iconLib: 'FontAwesome5' },
    { icon: 'smile-wink', title: 'Memes', iconLib: 'FontAwesome5' },
    { icon: 'link', title: 'Session Links', iconLib: 'FontAwesome5' },
  ];

  return (
    <MainContainer>
      {list.map((item, index) => {
        return (
          <TouchItem key={index} onPress={onClick}>
            <ViewContainer>
              {item.iconLib === 'MaretialIcon' ? (
                <IconContainer>
                  <Icon
                    name={item.icon}
                    size={normalize(18)}
                    type="MaterialIcons"
                  />
                </IconContainer>
              ) : (
                <IconContainer>
                  <Icon
                    name={item.icon}
                    size={normalize(18)}
                    type="FontAwesome5"
                  />
                </IconContainer>
              )}
              <Text style={{ fontSize: textSizes.h8, marginLeft: normalize(4) }}>
                {item.title}
              </Text>
              <RightIcon>
                <Icon
                  type="MaterialIcons"
                  name="keyboard-arrow-right"
                  size={normalize(22)}
                />
              </RightIcon>
            </ViewContainer>
          </TouchItem>
        );
      })}
    </MainContainer>
  );
};
const MainContainer = styled.View`
  padding-top: ${normalize(20)}px;
`;

const ViewContainer = styled.View`
  border-bottom-width: 1px;
  border-color: ${Colors.darkGray};
  padding: ${normalize(15)}px;
  padding-top: ${normalize(20)}px;
  padding-bottom: ${normalize(20)}px;
  flex-direction: row;
  width: 100%;
  align-items: center;
`;

const IconContainer = styled.View`
  width: ${normalize(25)}px;
  margin-right: ${normalize(15)}px;
  margin-left: ${normalize(5)}px;
`;

const RightIcon = styled.View`
  position: absolute;
  right: ${normalize(15)}px;
`;
