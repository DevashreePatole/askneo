import React, { FC, useState, useRef } from 'react';

import { FlatList, View, Text, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';
import moment from 'moment';

import { Colors } from '../../../assets/styles/color';
import { ComingSoon, TouchItem } from '../../../components';
import { FilterNotification } from './filter';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

export const Notification: FC = () => {
  const [notification, setNotification] = useState([
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: true,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: false,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Vertika Verma',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: false,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Akash Singh',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: false,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Diksha Choudhary',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: false,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: true,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Nilesh Chavan',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: false,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
      viewed: true,
    },
    {
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
      employeeName: 'Devashree Patole',
      posted: 'Blog',
      title: 'Announcing React Native 0.62 with Flipper',
      postedOn: '2021-04-01T16:00:00',
    },
  ]);
  const refRBSheet = useRef<RBSheet>(null);
  const [modal, setModal] = useState(false);

  const updateList = (index) => {
    const newArray = [...notification];
    newArray[index].viewed = true;
    setNotification(newArray);
  };

  const viewAll = () => {
    const newArray = [...notification];
    newArray.map((item, index) => {
      return (item.viewed = true);
    });
    setNotification(newArray);
  };

  const _rendetNotification = ({ item, index }) => {
    const uploadedDate = moment(item.postedOn).format('ll');
    const uploadedTime = moment(item.postedOn).format('h:mm a');
    return (
      <TouchItem
        onPress={() => {
          updateList(index);
        }}>
        <ViewContainer
          style={{
            backgroundColor: !item.viewed ? 'rgba(0,0,0,0.1)' : Colors.white,
          }}>
          <ImageContainer source={{ uri: item.profileImage }} />
          <NotificaionContainer>
            <Text style={{ lineHeight: normalize(19) }}>
              <Text style={{ fontWeight: '600' }}>{item.employeeName}</Text> .
              Posted a {item.posted}.{'\n'}
              {item.title}
            </Text>
            <Text style={styles.dateTime}>
              {uploadedDate} at {uploadedTime}
            </Text>
          </NotificaionContainer>
          {!item.viewed && (
            <Icon
              type="Entypo"
              name="dot-single"
              size={normalize(40)}
              color={Colors.black}
              style={{ position: 'absolute', right: 0 }}
            />
          )}
        </ViewContainer>
      </TouchItem>
    );
  };

  const _keyCountryExtractor = (item, index) => index.toString();
  return (
    <ScreenContainer>
      <HeaderContainer>
        <TouchItem
          onPress={() => {
            viewAll();
          }}
          style={styles.markAllReadBtn}>
          <Text style={styles.allReadText}>Mark All As Read</Text>
        </TouchItem>
        <TouchItem
          onPress={() => {
            refRBSheet.current?.open();
            //setModal(true)
          }}
          style={styles.filterBtn}>
          <View style={{ flexDirection: 'row' }}>
            <Icon
              type="FontAwesome"
              name="filter"
              size={normalize(18)}
              color={Colors.black}
            />
            <Text style={styles.filterText}>Filter</Text>
          </View>
        </TouchItem>
      </HeaderContainer>
      <FlatList
        data={notification}
        renderItem={_rendetNotification}
        keyExtractor={_keyCountryExtractor}
        alwaysBounceVertical={false}
        showsVerticalScrollIndicator={false}
      />
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        //height={WINDOW.height * 0.9}
        animationType={'slide'}
        customStyles={{
          wrapper: {
            backgroundColor: 'transparent',
          },
          draggableIcon: {
            backgroundColor: Colors.black,
          },
          container: {
            borderTopRightRadius: normalize(18),
            borderTopLeftRadius: normalize(18),
            borderColor: '#d6d6d6',
            borderWidth: 1,
             height: '90%',
          },
        }}>
        <FilterNotification
          onClick={() => {
            refRBSheet.current?.close();
          }}
        />
      </RBSheet>
      <ComingSoon
        visibility={modal}
        handleModalVisibility={() => {
          setModal(false);
        }}
      />
    </ScreenContainer>
  );
};

const ScreenContainer = styled.View`
  flex: 1;
  background-color: ${Colors.white};
`;

const ViewContainer = styled.View`
  border-bottom-width: ${normalize(1)}px;
  border-color: ${Colors.darkGray};
  padding-left: ${normalize(10)}px;
  padding: ${normalize(10)}px;
  flex-direction: row;
  width: 100%;
  align-items: center;
`;

const ImageContainer = styled(ImageComponent)`
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(35)}px;
  z-index: 1;
`;

const NotificaionContainer = styled.View`
  padding-left: ${normalize(10)}px;
  padding-top: ${normalize(5)}px;
  width: 75%;
  margin-left: ${normalize(5)}px;
  z-index: 1;
`;

const HeaderContainer = styled.View`
  flex-direction: row;
  border-bottom-width: ${normalize(1)}px;
  border-color:${Colors.darkGray}
  height: ${normalize(50)}px;
  background-color:rgba(204, 204, 204,0.2);
`;

const styles = StyleSheet.create({
  markAllReadBtn: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderRightColor: Colors.darkGray,
  },
  filterBtn: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dateTime: {
    fontSize: textSizes.h12,
    color: Colors.darkGray,
    paddingTop: normalize(5),
  },
  allReadText: {
    fontSize: textSizes.h9,
    fontWeight: '600',
  },
  filterText: {
    fontSize: textSizes.h9,
    paddingLeft: normalize(5),
    fontWeight: '600',
  },
});
