import React, { FC, useState } from 'react';

import styled from 'styled-components/native';
import Toast from 'react-native-simple-toast';

import { Colors } from '../../../assets/styles/color';
import { Container, Input, Text, TouchItem } from '../../../components';
import Button from '../../../components/Button';
import { Icon } from '../../../components/Icon';
import { normalize, textSizes } from '../../../lib/globals';
import { ScrollView } from 'react-native';

export const Setting: FC = () => {
    const [email, setEmail] = useState('');
    const handleAddEmail = () => {
        setEmail('');
        Toast.show('Feature coming soon', Toast.SHORT, ['UIAlertController']);
    };

    return (
        <ScreenContainer>
            <ScrollView>
                <TouchItem onPress={() => { }}>
                    <BranchSelector>
                        <Text>Account</Text>
                        <Icon
                            type="Entypo"
                            color={Colors.darkGray}
                            name={'chevron-down'}
                            size={normalize(32)}
                        />
                    </BranchSelector>
                </TouchItem>

                <Box>
                    <Text fontWeight="500" align={'flex-start'}>
                        Email
                    </Text>
                </Box>

                <Box>
                    <Text align={'flex-start'} style={{ marginBottom: normalize(5) }}>
                        vartika.verma@neosoftmail.com
                    </Text>
                    <Text align={'flex-start'} style={{ color: Colors.darkGray }}>
                        Primary Email
                    </Text>
                </Box>
                <Box>
                    <Text
                        fontWeight="500"
                        align={'flex-start'}
                        style={{ marginBottom: normalize(5) }}>
                        Add Email
                    </Text>
                    <Input
                        value={email}
                        onChangeText={(val) => setEmail(val)}
                        placeholder="name@example.com"
                        style={{
                            borderWidth: 2,
                            borderColor: Colors.lightGray,
                            height: normalize(50),
                            marginTop: -normalize(25),
                        }}
                    />
                    <Button
                        disabled={email.length <= 0}
                        title="Add Email"
                        onClick={handleAddEmail}
                        style={{
                            width: normalize(120),
                            height: normalize(40),
                            marginTop: normalize(15),
                            alignSelf: 'flex-start',
                            borderRadius: normalize(20),
                        }}
                        fontSize={textSizes.h8}
                    />
                </Box>

                <Box style={{ marginTop: normalize(6) }}>
                    <Text fontWeight="500" align={'flex-start'}>
                        Password
                    </Text>
                </Box>

                <Box>
                    <TouchItem
                        onPress={() => {
                            Toast.show('Feature coming soon', Toast.SHORT, [
                                'UIAlertController',
                            ]);
                        }}>
                        <Text
                            align={'flex-start'}
                            style={{ marginBottom: normalize(5), color: Colors.lightBlue }}>
                            Change Password
                        </Text>
                    </TouchItem>
                </Box>

                <Box style={{ marginTop: normalize(6) }}>
                    <Text fontWeight="500" align={'flex-start'}>
                        Deactivate Your Account
                    </Text>
                </Box>

                <Box>
                    <TouchItem
                        onPress={() => {
                            Toast.show('Feature coming soon', Toast.SHORT, [
                                'UIAlertController',
                            ]);
                        }}>
                        <Text
                            align={'flex-start'}
                            style={{ marginBottom: normalize(5), color: Colors.darkRed }}>
                            Deactivate Account
                        </Text>
                    </TouchItem>
                </Box>
            </ScrollView>
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: 0px;
  background-color: ${Colors.appGray};
`;

const BranchSelector = styled.View`
  width: 100%;
  height: ${normalize(55)}px;
  background-color: ${Colors.white};
  margin-bottom: ${normalize(3)}px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: ${normalize(15)}px;
  padding-right: ${normalize(10)}px;
`;

const Box = styled.View`
  width: 100%;
  margin-bottom: ${normalize(3)}px;
  padding: ${normalize(15)}px;
  background-color: ${Colors.white};
  flex-direction: column;
  align-items: flex-start;
`;
