import React, { FC, useEffect, useRef, useState } from 'react';

import { Animated, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';

import { AddComment, Text, TimeAgo, TouchItem } from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { ComingSoon } from '../../../components/ComingSoon';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { AnimatedIcon, AnimateIcon } from '../../../components/AnimatedIcon';

interface DataProps {
  title: string;
  image: string;
  postedOn: string;
  employeeName: string;
  technology: string;
  likes: string;
  comments: string;
}
interface Props {
  data: DataProps;
}

export const CardViewComponent: FC<Props> = ({ data, ...props }) => {
  const [modal, setModal] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [likes, setLikes] = useState(parseInt(data.likes));
  const [visible, setVisible] = useState(false);
  const refRBSheet = useRef<RBSheet>(null);
  const currentValue = new Animated.Value(1);

  useEffect(() => {
    if (disabled == true) {
      AnimateIcon(currentValue, () => {
        setVisible(false);
      });
    }
  }, [disabled]);

  const handleLikeClick = () => {
    if (!disabled) {
      setLikes((prev) => prev + 1);
      setDisabled(true);
      setVisible(true);
    } else {
      setLikes((prev) => prev - 1);
      setDisabled(false);
      setVisible(false);
    }
  };

  return (
    <CardContainer onPress={() => setModal(true)}>
      <HeaderContainer>
        <ImageContainer
          source={{
            uri: `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU`,
          }}
        />
        <ProfileContainer>
          <Text size={textSizes.h10} fontWeight="500">
            {data.employeeName}
          </Text>
          <Text size={textSizes.h13} style={styles.technology}>
            {data.technology}
          </Text>
        </ProfileContainer>
      </HeaderContainer>
      <DetailContainer>
        <Text size={textSizes.h10} fontWeight="500" style={styles.title}>
          {data.title}
        </Text>
        <FooterContainer>
          <FooterLikeComment>
            <TouchItem onPress={handleLikeClick}>
              <Row>
                <Icon
                  name={disabled ? 'thumbs-up' : 'thumbs-o-up'}
                  type="FontAwesome"
                  size={normalize(18)}
                  color={disabled ? Colors.primaryColor : Colors.darkGray}
                />
                <Text style={styles.iconText}>{likes}</Text>
              </Row>
            </TouchItem>
            <TouchItem onPress={() => refRBSheet.current?.open()}>
              <Row>
                <Icon
                  name="comment-o"
                  type="FontAwesome"
                  size={normalize(18)}
                  color={Colors.darkGray}
                />
                <Text style={styles.iconText}>{data.comments}</Text>
              </Row>
            </TouchItem>
          </FooterLikeComment>
          <TimeAgo
            time={data.postedOn}
            style={{ alignSelf: 'flex-end', padding: normalize(4) }}
          />
        </FooterContainer>
      </DetailContainer>
      <ComingSoon
        visibility={modal}
        handleModalVisibility={() => {
          setModal(false);
        }}
      />

      {visible && (
        <AnimatedIcon
          name="thumbs-up"
          size={normalize(25)}
          color={Colors.primaryColor}
          currentValue={currentValue}
          style={{ top: '40%', left: '48%' }}
        />
      )}

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={customStyles}>
        <AddComment />
      </RBSheet>
    </CardContainer>
  );
};

const CardContainer = styled(TouchItem)`
  margin-top: ${normalize(6)}px;
  margin-bottom: ${normalize(5)}px;
  border-width: 1px;
  padding: ${normalize(10)}px;
  border-color: ${Colors.lightGray};
  background-color: ${Colors.white};
  border-radius: ${normalize(8)}px;
`;

const Row = styled.View`
  flex-direction: row;
`;

const ImageContainer = styled.Image`
  width: ${normalize(37)}px;
  height: ${normalize(37)}px;
  border-radius: ${normalize(37)}px;
`;
const HeaderContainer = styled.View`
  flex-direction: row;
  justify-content: flex-start;
`;
const ProfileContainer = styled.View`
  padding-left: ${normalize(8)}px;
  padding-top: ${normalize(8)}px;
`;
const DetailContainer = styled.View`
  justify-content: flex-start;
  margin-top: ${normalize(15)}px;
`;

const FooterContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${normalize(15)}px;
`;
const FooterLikeComment = styled.View`
  width: ${normalize(110)}px;
  height: ${normalize(38)}px;
  background-color: ${Colors.lightGray};
  margin-left: ${normalize(10)}px;
  justify-content: space-around;
  align-items: center;
  border-radius: ${normalize(30)}px;
  flex-direction: row;
`;

const styles = StyleSheet.create({
  technology: {
    marginTop: normalize(3),
    alignSelf: 'flex-start',
    left: 0,
  },
  title: {
    alignSelf: 'flex-start',
    paddingLeft: normalize(10),
    lineHeight: normalize(15),
  },
  iconText: {
    fontSize: textSizes.h12,
    paddingLeft: normalize(5),
    textAlign: 'center',
  },
});

const customStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(20),
    borderTopLeftRadius: normalize(20),
    borderColor: '#d6d6d6',
    borderWidth: 1,
    height: '90%',
  },
});

export const CardView = React.memo(CardViewComponent);
