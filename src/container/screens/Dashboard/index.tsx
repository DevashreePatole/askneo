import React, { FC, useRef, useState } from 'react';

import { BackHandler, FlatList } from 'react-native';
import styled from 'styled-components/native';
import { useFocusEffect } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

import AlertModal from '../../../components/AlertModal';
import { Container, AddItem } from '../../../components';
import { CardView } from './cardView';
import { Colors } from '../../../assets/styles/color';
import {RootStackParamList} from '../../navigators/stack/dashboardStack';

interface Props {
  navigation: StackNavigationProp<RootStackParamList,'Home'>
}

export const Dashboard: FC<Props> = ({ navigation }) => {

  const [data, setData] = useState([
    {
      title: 'Blog posted on React Native for the Styled component',
      postedOn: '2021-04-14T08:00:00',
      image:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      employeeName: 'Akash Singh',
      technology: 'React Native',
      likes: '5',
      comments: '2',
    },
    {
      title: 'Session of Tensorflow ',
      postedOn: '2021-04-12T16:00:00',
      image:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      employeeName: 'Nilesh Chavhan',
      technology: 'React Native',
      likes: '10',
      comments: '5',
    },
    {
      title: 'Session of Tensorflow ',
      postedOn: '2021-03-31T16:00:00',
      image:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      employeeName: 'Devashree Patole',
      technology: 'React Native',
      likes: '3',
      comments: '1',
    },
    {
      title: 'Session of Tensorflow ',
      postedOn: '2021-03-31T16:00:00',
      image:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      employeeName: 'Vertika Verma',
      technology: 'React Native',
      likes: '6',
      comments: '4',
    },
    {
      title: 'Session of Tensorflow ',
      postedOn: '2021-03-31T16:00:00',
      image:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      employeeName: 'Diksha Choudhary',
      technology: 'React Native',
      likes: '9',
      comments: '6',
    },
  ]);

  let alertRef = useRef();

  const handleBackPress = () => {
    const warningModal = () =>
      alertRef.current?.setOptionAndOpen(
        'sure',
        'Are you sure you want to Exit',
        () => BackHandler.exitApp(),
      );
    warningModal();

    return true;
  };

  useFocusEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackPress);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
    };
  });

  const _keyCountryExtractor = (item, index) => index.toString();

  const _renderData = ({ item }) => {
    return <CardView data={item} />;
  };
  return (
    <MainContainer withKeyboard={false}>
      <FlatList
        data={data}
        renderItem={_renderData}
        keyExtractor={_keyCountryExtractor}
        showsVerticalScrollIndicator={false}
        alwaysBounceVertical={false}
      />
      <AddItem navigation={navigation} />
      <AlertModal ref={alertRef} />
    </MainContainer>
  );
};

const MainContainer = styled(Container)`
  justify-content: center;
  background-color: ${Colors.lightGray};
`;
