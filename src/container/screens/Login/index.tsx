import React, {useState} from 'react';

import {StyleSheet, View} from 'react-native';
import styled from 'styled-components/native';
import {prop} from 'styled-tools';
import {Formik} from 'formik';
import * as yup from 'yup';
import {StackNavigationProp} from '@react-navigation/stack';
import {CompositeNavigationProp} from '@react-navigation/native';

import {Container, Text, Input, TouchItem} from '../../../components';
import Button from '../../../components/Button';
import {Colors} from '../../../assets/styles/color';
import {ImageBackground} from '../../../components/ImageBackground';
import {AuthStackParamList} from '../../navigators/stack/mainStack';
import {RootStackParamList} from '../../navigators/stack/loginNavigation';
import {normalize, percent, textSizes} from '../../../lib/globals';

type LoginProps = CompositeNavigationProp<
  StackNavigationProp<AuthStackParamList>,
  StackNavigationProp<RootStackParamList>
>;

interface Props {
  navigation: LoginProps;
}

const validationSchema = yup.object({
  UserName: yup.string().email().required(),
  Password: yup
    .string()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .required()
    .matches(/^[a-zA-Z0-9_]*$/, 'Password can only contain alphanumeric.'),
});

export const Login: React.FC<Props> = ({navigation}) => {
  const [loading, setLoading] = useState(false);

  const handleLogin = (values) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      navigation.navigate('Dashboard');
    }, 2000);
  };

  return (
    <ScreenContainer>
      <MainContainer withKeyboard={false} loading={loading}>
        <Screen
          style={{alignSelf: 'center'}}
          showsVerticalScrollIndicator={true}
          alwaysBounceVertical={false}>
          <FormContainer>
            <LogoContainer>
              <Logo
                style={{position: 'relative', left: -normalize(23)}}
                size={normalize(140)}
                source={require('../../../assets/images/FinalLOGO.png')}
              />
            </LogoContainer>
            <StepText size={textSizes.h5}>Proceed with your</StepText>
            <LoginText size={textSizes.h3} fontWeight="500">
              Login
            </LoginText>
            <Form>
              <Formik
                initialValues={{
                  UserName: 'akash@gmail.com',
                  Password: '123456789',
                }}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                  handleLogin(values);
                }}>
                {(props) => {
                  return (
                    <View>
                      <Input
                        label="Username"
                        placeholder={'akash@gmail.com'}
                        icon="user"
                        value={props.values.UserName}
                        onChangeText={props.handleChange('UserName')}
                        onBlur={props.handleBlur('UserName')}
                        errorText={
                          props.touched.UserName && props.errors.UserName
                        }
                        style={{borderColor: 'white'}}
                        inputColor={'white'}
                      />
                      <Input
                        label="Password"
                        placeholder={'*********'}
                        icon="eye-slash"
                        value={props.values.Password}
                        secureTextEntry={true}
                        onChangeText={props.handleChange('Password')}
                        onBlur={props.handleBlur('Password')}
                        errorText={
                          props.touched.Password && props.errors.Password
                        }
                        style={{borderColor: 'white'}}
                        inputColor={'white'}
                      />
                      <LoginButton title="Login" onClick={props.handleSubmit} />
                    </View>
                  );
                }}
              </Formik>
              <TouchItem onPress={() => navigation.navigate('ForgotPassword')}>
                <Text
                  size={textSizes.h10}
                  fontWeight="500"
                  style={styles.forgetPassword}>
                  Forgot Password?
                </Text>
              </TouchItem>
              <SignUpContainer>
                <Text size={textSizes.h10} style={{color: 'white'}}>
                  Don't have an account?
                </Text>
                <TouchItem onPress={() => navigation.navigate('Register')}>
                  <Text
                    size={textSizes.h10}
                    fontWeight="500"
                    style={styles.signUpText}>
                    Sign Up
                  </Text>
                </TouchItem>
              </SignUpContainer>
            </Form>
          </FormContainer>
        </Screen>
      </MainContainer>
      <ImageBackground />
    </ScreenContainer>
  );
};

const ScreenContainer = styled.View`
  flex: 1;
  z-index: 1;
`;

const MainContainer = styled(Container)`
  flex-direction: row;
  align-content: center;
  padding: 0px;
  padding-top: ${normalize(20)}px;
  padding-bottom: ${normalize(20)}px;
  background-color: transparent;
`;
const LogoContainer = styled.View`
  width: 100%;
  margin-top: ${normalize(20)}px;
`;

const Logo = styled.Image<{size}>`
  width: ${prop('size')}px;
  height: ${prop('size')}px;
`;

const StepText = styled(Text)`
  align-self: flex-start;
  color: white;
`;

const LoginText = styled(Text)`
  align-self: flex-start;
  margin-top: ${normalize(15)}px;
  color: ${Colors.white};
`;

const FormContainer = styled.View`
  padding: ${normalize(35)}px;
  padding-top: 0px;
  padding-bottom: ${normalize(20)}px;
`;

const Form = styled.View`
  margin-top: ${normalize(35)}px;
`;

const LoginButton = styled(Button)`
  width: ${percent(80)}px;
  margin-top: ${normalize(40)}px;
  background-color: ${Colors.primaryColor};
`;

const SignUpContainer = styled.View`
  flex-direction: row;
  align-self: center;
  margin-top: ${normalize(35)}px;
`;

const Screen = styled.ScrollView``;

const styles = StyleSheet.create({
  forgetPassword: {
    marginTop: normalize(20),
    color: Colors.white,
  },
  signUpText: {
    color: Colors.white,
    marginLeft: normalize(10),
  },
});
