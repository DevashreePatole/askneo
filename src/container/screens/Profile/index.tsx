import React, { FC, useState } from 'react';

import { ScrollView, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { SafeContainer, Text, TouchItem } from '../../../components';
import { Colors } from '../../../assets/styles/color';
import { Icon } from '../../../components/Icon';
import { normalize, textSizes, WINDOW } from '../../../lib/globals';
import { RootProfileStackParamList } from '../../navigators/stack/profileStack';
import { ImageComponent } from '../../../components/Image';

interface Props {
  navigation: StackNavigationProp<RootProfileStackParamList, 'ProfileMain'>;
}
export const Profile: FC<Props> = ({ navigation }) => {
  const [options, setOptions] = useState([
    {
      name: 'Questions',
      icon: 'question-answer',
      type: 'MaterialIcons',
      route: 'ProfileQuestions',
    },
    {
      name: 'Blogs',
      icon: 'blog',
      type: 'FontAwesome5',
      route: 'ProfileBlogs',
    },
    {
      name: 'Interview Question',
      icon: 'question-circle',
      type: 'FontAwesome5',
      route: 'ProfileInterviewQuestions',
    },
    {
      name: 'Answers',
      icon: 'reply-all-outline',
      type: 'MaterialCommunityIcons',
      route: 'ProfileAnswers',
    },
    {
      name: 'Session Links',
      icon: 'link',
      type: 'FontAwesome5',
      route: 'ProfileSessionLinks',
    },
    {
      name: 'Memes',
      icon: 'smile-wink',
      type: 'FontAwesome5',
      route: 'ProfileMeme',
    },
  ]);
  const [displayEdit, setDisplayEdit] = useState(false);

  return (
    <ScreenContainer withKeyboard={false}>
      <ProfileHeader>
        <HeaderIcons>
          <Icon
            type="Entypo"
            name="chevron-left"
            size={normalize(32)}
            onPress={() => navigation.goBack()}
          />
          <Icon
            type="Feather"
            name="more-horizontal"
            size={normalize(28)}
            onPress={() => {
              setDisplayEdit((prevState) => !prevState);
            }}
          />
        </HeaderIcons>

        <UserDetail>
          <View style={styles.imageBox}>
            <ImageComponent
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
              }}
              style={styles.profileImg}
            />
            <BadgeContainer>
              <Icon
                type="FontAwesome5"
                name="award"
                size={normalize(25)}
                color={Colors.silver}
                style={{ marginTop: normalize(-2) }}
              />
            </BadgeContainer>
          </View>
          <Text fontWeight="500" size={textSizes.h8}>
            Vartika Verma
          </Text>
          <Text
            fontWeight="500"
            size={textSizes.h8}
            style={{ marginTop: normalize(10) }}>
            React Native Developer
          </Text>
        </UserDetail>

        {displayEdit ? (
          <MoreContainer>
            <TouchItem
              onPress={() => {
                setDisplayEdit(false);
                navigation.navigate('ProfileEdit');
              }}
              style={{ padding: normalize(10) }}>
              <Row>
                <Icon
                  type="MaterialCommunityIcons"
                  name="account-edit"
                  size={normalize(18)}
                />
                <Text
                  size={textSizes.h10}
                  style={{ marginLeft: normalize(5), fontWeight: '500' }}>
                  Edit Profile
                </Text>
              </Row>
            </TouchItem>
            <UpArrow>
              <Icon
                type="AntDesign"
                name="caretup"
                size={normalize(30)}
                color={Colors.white}
              />
            </UpArrow>
          </MoreContainer>
        ) : null}
      </ProfileHeader>

      <UserActivity>
        <ScrollView
          showsVerticalScrollIndicator={false}
          alwaysBounceVertical={false}>
          {options.map((option, index) => {
            return (
              <TouchItem
                key={index}
                onPress={() => {
                  navigation.navigate(option.route);
                }}>
                <OptionContainer>
                  <Row>
                    <RouteIconContainer>
                      <Icon
                        type={option.type}
                        name={option.icon}
                        size={normalize(18)}
                        color={Colors.primaryColor}
                      />
                    </RouteIconContainer>
                    <Text fontWeight="500" size={textSizes.h9}>
                      {option.name}
                    </Text>
                  </Row>

                  <Icon
                    type="Entypo"
                    name="chevron-right"
                    size={normalize(25)}
                  />
                </OptionContainer>
              </TouchItem>
            );
          })}
        </ScrollView>
      </UserActivity>
    </ScreenContainer>
  );
};

const ScreenContainer = styled(SafeContainer)`
  background-color: ${Colors.lightGray};
  padding-left: 0px;
  padding-right: 0px;
  padding: 0px;
`;

const Row = styled.View`
  flex-direction: row;
`;

const ProfileHeader = styled.View`
  width: 100%;
  height: ${normalize(250)}px;
  padding-left: ${normalize(10)}px;
  padding-right: ${normalize(15)}px;
`;

const HeaderIcons = styled(Row)`
  width: 100%;
  justify-content: space-between;
`;

const UserDetail = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: ${normalize(20)}px;
  z-index:-1;
`;

const UserActivity = styled.View`
  width: 100%;
  height: ${WINDOW.height - normalize(290)}px;
  background-color: ${Colors.white};
  border-top-left-radius: ${normalize(30)}px;
  border-top-right-radius: ${normalize(30)}px;
  padding: ${normalize(30)}px;
  padding-bottom: ${normalize(20)}px;
`;

const OptionContainer = styled(Row)`
  width: 100%;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${normalize(40)}px;
`;

const RouteIconContainer = styled(Row)`
  width: ${normalize(40)}px;
  height: ${normalize(40)}px;
  background-color: ${Colors.black};
  margin-right: ${normalize(20)}px;
  align-items: center;
  justify-content: center;
  border-radius: ${normalize(5)}px;
`;

const BadgeContainer = styled(Row)`
  position: absolute;
  top: -${normalize(10)}px;
  right: -${normalize(10)}px;
  width: ${normalize(32)}px;
  height: ${normalize(32)}px;
  justify-content: center;
  align-items: center;
  border-radius: ${normalize(10)}px;
  background-color: black;
`;

const MoreContainer = styled(Row)`
  position: absolute;
  top: ${normalize(28)}px;
  background-color: white;
  right: ${normalize(15)}px;
  align-items: center;
  justify-content: center;
  border-radius: ${normalize(4)}px;
  padding: ${normalize(18)}px;
  padding-top: ${normalize(5)}px;
  padding-bottom: ${normalize(5)}px;
`;

const UpArrow = styled.View`
  position: absolute;
  top: -${normalize(14)}px;
  right: ${normalize(5)}px;
`;

const styles = StyleSheet.create({
  imageBox: {
    marginBottom: normalize(20),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    borderRadius: normalize(15),
  },
  profileImg: {
    width: normalize(95),
    height: normalize(95),
    borderRadius: normalize(15),
  },
});
