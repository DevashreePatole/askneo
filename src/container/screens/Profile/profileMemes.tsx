import React, { FC, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Container } from '../../../components';
import { normalize } from '../../../lib/globals';
import { RootProfileStackParamList } from '../../navigators/stack/profileStack';
import { DisplayCard } from '../Memes/displayCard';

interface Props {
    navigation: StackNavigationProp<RootProfileStackParamList, 'ProfileMeme'>;
}

interface MemeProp {
    author: string;
    caption: string;
    authorImage: string;
    memeImage: string;
}

export const ProfileMemes: FC<Props> = ({ navigation }) => {
    const [loading, setLoading] = useState(false);

    const _renderMemes = ({ item, index }) => {
        return <DisplayCard key={index} meme={item} userMeme={true} />;
    };

    const [memes, setMemes] = useState<MemeProp[]>([
        {
            author: 'Vartika Verma',
            caption: 'Why Brain... Why???😰🥱😴',
            authorImage:
                'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
            memeImage:
                'https://www.thecoderpedia.com/wp-content/uploads/2020/06/Programming-Memes-Programmer-while-sleeping.jpg?x78269',
        },
        {
            author: 'Vartika Verma',
            caption: 'Utha le re Baba... 🤢 (Mere ko nahi, in charo ko uthale )😈',
            authorImage:
                'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
            memeImage: 'https://i.redd.it/0snws7y219b11.png',
        },
    ]);

    return (
        <ScreenContainer withKeyboard={false} loading={loading}>
            <FlatList
                data={memes}
                keyExtractor={(item, index) => index.toString()}
                renderItem={_renderMemes}
                showsVerticalScrollIndicator={false}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: 0;
  padding-top: ${normalize(10)}px;
  padding-bottom: ${normalize(10)}px;
`;
