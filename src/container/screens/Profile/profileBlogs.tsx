import React, { FC, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../assets/styles/color';
import { Container, TouchItem } from '../../../components';
import { RootProfileStackParamList } from '../../navigators/stack/profileStack';
import { CardView } from '../Blogs/cardView';

interface Props {
    navigation: StackNavigationProp<RootProfileStackParamList, 'ProfileBlogs'>;
}

export const ProfileBlogs: FC<Props> = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    let blog = [
        {
            profileImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPZQ_Z6Jir7-1quljeW8Nea3KQ3uXEVbtQ6w&usqp=CAU',
            employeeName: 'Vartika Verma',
            technology: 'React Native',
            empId: '1234',
            title: 'Announcing React Native 0.62 with Flipper',
            postedOn: '2021-04-01T16:00:00',
            description:
                'Flipper is a developer tool for debugging mobile apps. It’s popular in the Android and iOS communities, and in this release we’ve enabled support by default for new and existing React Native apps.',
            blogImage:
                'https://reactnative.dev/assets/images/0.62-flipper-dc5a5cb54cc6033750c56f3c147c6ce3.png',
            likes: '5',
        },
    ];

    const _renderData = ({ item }) => {
        return (
            <TouchItem onPress={() => { }}>
                <CardView blog={item} userBlog={true}/>
            </TouchItem>
        );
    };

    const _keyCountryExtractor = (item, index) => index.toString();

    return (
        <ScreenContainer withKeyboard={false} loading={loading}>
            <FlatList
                data={blog}
                renderItem={_renderData}
                keyExtractor={_keyCountryExtractor}
                showsVerticalScrollIndicator={false}
                alwaysBounceVertical={false}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  justify-content: center;
  background-color: ${Colors.lightGray};
`;
