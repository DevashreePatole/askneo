import React, {FC, useRef, useState} from 'react';

import {View, ScrollView, Platform, StatusBar} from 'react-native';
import styled, {css} from 'styled-components/native';
import {Formik} from 'formik';
import * as yup from 'yup';
import {StackNavigationProp} from '@react-navigation/stack';
import {ifProp} from 'styled-tools';

import {Container, Input, Text, TouchItem} from '../../../components';
import {Colors} from '../../../assets/styles/color';
import Button from '../../../components/Button';
import {Icon} from '../../../components/Icon';
import {RootStackParamList} from '../../navigators/stack/loginNavigation';
import * as globals from '../../../lib/globals';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'Register'>;
}

const validationSchema = yup.object({
  firstName: yup.string().min(2).required(),
  lastName: yup.string().min(2).required(),
  email: yup.string().email().required(),
  phoneNo: yup.string().required().min(10).max(10),
  empId: yup.string().required().min(4).max(4),
  password: yup
    .string()
    .min(8, 'Password is too short - should be 8 chars minimum.')
    .required()
    .matches(/^[a-zA-Z0-9_]*$/, 'Password can only contain alphanumeric.'),
  confirmPwd: yup
    .string()
    .required()
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});

export const Register: FC<Props> = ({navigation}) => {
  const scrollRef = useRef<ScrollView>(null);
  const [scrollBottom, setBottom] = useState(false);
  const [scrollTop, setTop] = useState(false);

  const onBottomPress = () => {
    scrollRef.current?.scrollToEnd({
      animated: true,
    });
  };

  const onTopPress = () => {
    scrollRef.current?.scrollTo({
      x: 0,
      animated: true,
    });
  };

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 40
    );
  };
  const isCloseToTop = ({layoutMeasurement, contentOffset, contentSize}) => {
    return contentOffset.y == 0;
  };

  return (
    <ScreenContainer>
      <StatusBar barStyle="light-content" backgroundColor={Colors.black} />
      <MainContainer withKeyboard={false}>
        <ScrollView
          ref={scrollRef}
          scrollEventThrottle={16}
          style={{paddingTop: Platform.OS == 'ios' ? globals.normalize(30) : 0}}
          alwaysBounceVertical={false}
          showsVerticalScrollIndicator={false}
          onContentSizeChange={(contentWidth, contentHeight) => {
            globals.WINDOW.height < contentHeight ? setBottom(true) : null;
          }}
          onScroll={({nativeEvent}) => {
            if (isCloseToTop(nativeEvent)) {
              setTop(false);
              setBottom(true);
            }
            if (isCloseToBottom(nativeEvent)) {
              setBottom(false);
              setTop(true);
            }
          }}>
          <FormContainer>
            <Formik
              initialValues={{
                firstName: '',
                lastName: '',
                email: '',
                phoneNo: '',
                empId: '',
                password: '',
                confirmPwd: '',
              }}
              validationSchema={validationSchema}
              onSubmit={(values) => {
                console.log(values);
                navigation.navigate('Login');
              }}>
              {(props) => {
                return (
                  <View>
                    <NameContainer>
                      <View style={{width: '45%'}}>
                        <Input
                          label="First Name"
                          placeholder="Jhon"
                          value={props.values.firstName}
                          onChangeText={props.handleChange('firstName')}
                          onBlur={props.handleBlur('firstName')}
                          errorText={
                            props.touched.firstName && props.errors.firstName
                          }
                        />
                      </View>
                      <View style={{width: '45%'}}>
                        <Input
                          label="Last Name"
                          placeholder="Doe"
                          value={props.values.lastName}
                          onChangeText={props.handleChange('lastName')}
                          onBlur={props.handleBlur('lastName')}
                          errorText={
                            props.touched.lastName && props.errors.lastName
                          }
                        />
                      </View>
                    </NameContainer>

                    <Input
                      label="Email Id"
                      placeholder="xyz@gmail.com"
                      value={props.values.email}
                      onChangeText={props.handleChange('email')}
                      onBlur={props.handleBlur('email')}
                      errorText={props.touched.email && props.errors.email}
                    />
                    <Input
                      label="Phone Number"
                      placeholder="0123456789"
                      value={props.values.phoneNo}
                      keyboardType={'numeric'}
                      onChangeText={props.handleChange('phoneNo')}
                      onBlur={props.handleBlur('phoneNo')}
                      errorText={props.touched.phoneNo && props.errors.phoneNo}
                    />
                    <Input
                      label="Employee Id"
                      placeholder="0245"
                      value={props.values.empId}
                      keyboardType={'numeric'}
                      onChangeText={props.handleChange('empId')}
                      onBlur={props.handleBlur('empId')}
                      errorText={props.touched.empId && props.errors.empId}
                    />
                    <Input
                      label="Password"
                      placeholder="********"
                      value={props.values.password}
                      icon="eye-slash"
                      secureTextEntry={true}
                      onChangeText={props.handleChange('password')}
                      onBlur={props.handleBlur('password')}
                      errorText={
                        props.touched.password && props.errors.password
                      }
                    />
                    <Input
                      label="Confirm Password"
                      placeholder="********"
                      value={props.values.confirmPwd}
                      icon="eye-slash"
                      secureTextEntry={true}
                      editable={props.values.password.length > 0 ? true : false}
                      onChangeText={props.handleChange('confirmPwd')}
                      onBlur={props.handleBlur('confirmPwd')}
                      errorText={
                        props.touched.confirmPwd && props.errors.confirmPwd
                      }
                    />
                    <ButtonContainer
                      title="Register"
                      onClick={props.handleSubmit}
                    />
                  </View>
                );
              }}
            </Formik>
          </FormContainer>
        </ScrollView>
      </MainContainer>
      {/* <ImageBackground /> */}
      {scrollBottom ? (
        <IconContainer
          scrollBtn={scrollBottom}
          onPress={() => {
            onBottomPress();
          }}
          //style={{...styles.scrollButton, bottom: 10}}
        >
          <Icon
            type="MaterialIcons"
            name="keyboard-arrow-down"
            size={globals.normalize(28)}
            color={Colors.black}
            //style={styles.iconStyle}
          />
        </IconContainer>
      ) : null}

      {scrollTop ? (
        <IconContainer
          onPress={() => {
            onTopPress();
          }}
          scrollBtn={scrollBottom}>
          <Icon
            type="MaterialIcons"
            name="keyboard-arrow-up"
            size={globals.normalize(28)}
            color={Colors.black}
          />
        </IconContainer>
      ) : null}
    </ScreenContainer>
  );
};

const ScreenContainer = styled.View`
  flex: 1;
  z-index: 1;
`;

const MainContainer = styled(Container)`
  flex-direction: row;
  align-content: center;
  padding: 0px;
  padding-top: ${globals.normalize(10)}px;
  padding-bottom: ${globals.normalize(20)}px;
  background-color: transparent;
`;

const NameContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
`;

const TextContainer = styled(Text)`
  color: ${Colors.black};
  padding-bottom: ${globals.normalize(20)}px;
`;

const ButtonContainer = styled(Button)`
  margin-top: ${globals.normalize(30)}px;
  width: ${globals.percent(80)}px;
  margin-bottom: ${globals.normalize(20)}px;
`;
const IconContainer = styled(TouchItem)<{scrollBtn}>`
  position: absolute;
  ${ifProp(
    {scrollBtn: true},
    css`
      bottom: ${globals.normalize(10)}px;
    `,
    css`
      top: ${globals.normalize(15)}px;
    `,
  )};
  right: ${globals.normalize(5)}px;
  width: ${globals.normalize(40)}px;
  height: ${globals.normalize(40)}px;
  background-color: ${Colors.primaryColor};
  border-radius: ${globals.normalize(50)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const FormContainer = styled.View`
  padding: ${globals.normalize(35)}px;
  padding-top: 0px;
  padding-bottom: ${globals.normalize(20)}px;
`;
