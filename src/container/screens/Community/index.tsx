import React, { FC, useEffect, useState } from 'react';

import { LogBox, ScrollView, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { FlatList } from 'react-native-gesture-handler';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { AddItem, ComingSoon, Container, Text } from '../../../components';
import { CommunityCard } from './CommunityCard';
import { SubCard } from './SubCard';
import { RootStackParamList } from '../../navigators/stack/communityStack';
import { normalize } from '../../../lib/globals';
import { ImageComponent } from '../../../components/Image';

interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'Community'>;
}

export const Community: FC<Props> = ({ navigation }) => {
    const [subCommunities, setSubCommunties] = useState([
        // {
        //     name: 'C/C ++ programming',
        //     secondaryImage:
        //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
        // },
        // {
        //     name: 'Data Structures',
        //     secondaryImage:
        //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
        // },
    ]);
    const [loading, setLoading] = useState(false);
    const [community, setCommunity] = useState([
        {
            id: '1',
            title: 'Node JS',
            communities: [
                {
                    id: '1',
                    name: 'Node Js Hacks and Tips',
                    admin: 'Akash Singh',
                    member: 20,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpEjkDzIn2Vx-JMtHJUTESLwzQSDQI32-gIA&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS16ZAt8VDhkcpY4o_Gl-SPR7AREYRe-3qsA&usqp=CAU',
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '2',
                    name: 'Node Champs',
                    admin: 'Diksha Choudhary',
                    member: 120,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSHlSE87ib88fRYZuuP_8u23gOoTm-6e0lEvw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXiIxOUkIyBDOtVpHpmRQWR4WBGyfmFlDFOw&usqp=CAU',
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '3',
                    name: 'Node JS Advanced',
                    admin: 'Devashree Patole',
                    member: 129,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZpvAc3kLmiD3nMUt4PSev5EIEX49ldFpCmw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
                    category: 'Private',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '4',
                    name: 'Node World',
                    admin: 'Vartika Verma',
                    member: 202,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZpvAc3kLmiD3nMUt4PSev5EIEX49ldFpCmw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
            ],
        },
        {
            id: '2',
            title: 'React Js',
            communities: [
                {
                    id: '1',
                    name: 'React JS Hackers',
                    admin: 'Akash Singh',
                    member: 190,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZCw0ueVE1bzWXaTfUg2V3Fo03jnTrONpIDg&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSD1kWMDA9JDew6n1KdI61tl-FMEfA2Tfq5Tw&usqp=CAU',
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '2',
                    name: 'React Pros',
                    admin: 'Diksha Choudhary',
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZpvAc3kLmiD3nMUt4PSev5EIEX49ldFpCmw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
                    member: 200,
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '3',
                    name: 'React Redux',
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZpvAc3kLmiD3nMUt4PSev5EIEX49ldFpCmw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
                    admin: 'Devashree Patole',
                    member: 2229,
                    category: 'Private',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
            ],
        },
        {
            id: '2',
            title: 'Python',
            communities: [
                {
                    id: '1',
                    name: 'Python Basics',
                    admin: 'Akash Singh',
                    member: 100,
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmieEqqPvX3Q7jYbV8Rayj8w0LQddQVm1F4w&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9LrV-RiqTzjF9e2XwejPJkIocy_Pr4GKzpQ&usqp=CAU',
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
                {
                    id: '2',
                    name: 'Advanced Python',
                    admin: 'Diksha Choudhary',
                    primaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZpvAc3kLmiD3nMUt4PSev5EIEX49ldFpCmw&usqp=CAU',
                    secondaryImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3D3UGgKtZ8wvBUQnCTht1JfdRt75A9BrXLA&usqp=CAU',
                    member: 20,
                    category: 'Public',
                    description: 'Group for node js and Javadcript concepts and Question',
                },
            ],
        },
    ]);
    const [modal, setModal] = useState(false);

    useEffect(() => {
        LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 2000);
    }, []);

    const _displayCards = ({ item, index }) => {
        return (
            <View style={{ marginBottom: normalize(20) }}>
                <Text
                    size={18}
                    fontWeight="500"
                    align={'flex-start'}
                    style={{ marginLeft: normalize(10), marginBottom: normalize(2) }}>
                    {`${item.title} (${item.communities.length})`}
                </Text>

                <View>
                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={item.communities}
                        keyExtractor={({ id }) => id}
                        renderItem={({ item }) => (
                            <CommunityCard navigation={navigation} data={item} />
                        )}
                    />
                </View>
            </View>
        );
    };

    return (
        <ScreenContainer withKeyboard={false} loading={loading}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <ContentContainer>
                    {subCommunities.length != 0 ? (
                        <SubHeaderContainer>
                            <HeaderTitle size={18} fontWeight="500">
                                Subscribed Communities
                            </HeaderTitle>
                            <FlatList
                                data={subCommunities}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => <SubCard data={item} />}
                            />
                        </SubHeaderContainer>
                    ) : (
                        <WelcomeHeader>
                            <WelcomeFC>
                                <HeaderTitle size={16} fontWeight="500">
                                    Welcome to Community!
                                </HeaderTitle>
                                <Text
                                    size={13}
                                    align={'flex-start'}
                                    style={{
                                        marginLeft: normalize(10),
                                        marginBottom: normalize(10),
                                        lineHeight: 17,
                                    }}>
                                    Follow Community to expore your interest on Different Topics
                                </Text>
                            </WelcomeFC>
                            <View style={{ width: '29%' }}>
                                <ImageComponent
                                    source={{
                                        uri:
                                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRv8Y48KRLBHW-4GKSd6_e9wHWwulGBioaVw&usqp=CAU',
                                    }}
                                    style={{ width: '100%', height: normalize(100) }}
                                    resizeMode={'contain'}
                                />
                            </View>
                        </WelcomeHeader>
                    )}

                    <FlatList
                        data={community}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={_displayCards}
                    />
                </ContentContainer>
            </ScrollView>
            <View style={{ paddingRight: 10 }}>
                <AddItem navigation={navigation} />

                <ComingSoon
                    visibility={modal}
                    handleModalVisibility={() => {
                        setModal(false);
                    }}
                />
            </View>
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: 0;
  padding-bottom: ${normalize(10)}px;
  background-color: ${Colors.appGray};
`;

const ContentContainer = styled.View`
  padding: ${normalize(10)}px;
  padding-top: 0px;
`;

const SubHeaderContainer = styled.View`
  background-color: ${Colors.white};
  padding-top: ${normalize(20)}px;
  margin-bottom: ${normalize(30)}px;
`;

const HeaderTitle = styled(Text)`
  margin-left: ${normalize(10)}px;
  margin-bottom: ${normalize(10)}px;
  align-self: flex-start;
`;

const WelcomeHeader = styled.View`
  background-color: ${Colors.white};
  flex-direction: row;
  margin-bottom: ${normalize(30)}px;
`;

const WelcomeFC = styled.View`
  width: 70%;
  padding-bottom: ${normalize(10)}px;
  padding-top: ${normalize(30)}px;
`;
