import React, { FC, useState } from 'react';

import { View, StyleSheet } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { Icon, Text, TouchItem } from '../../../../components';
import Button from '../../../../components/Button';
import { normalize, textSizes } from '../../../../lib/globals';

interface Props {
    title: string;
    description: string;
    rbRef: any;
    buttonTitle:string;
}

export const AdminActionCard: FC<Props> = ({ title, description, rbRef,buttonTitle }) => {
    const [show, setShow] = useState(false);

    return (
        <View>
            <TouchItem
                onPress={() => {
                    setShow((prevState) => !prevState);
                }}>
                <Card>
                    <Row>
                        <Icon
                            type="AntDesign"
                            name="checkcircleo"
                            size={normalize(20)}
                            color={Colors.primaryColor}
                        />
                        <Text size={textSizes.h9} style={{ marginLeft: normalize(10) }}>
                            {title}
                        </Text>
                    </Row>
                    <Icon
                        type="Feather"
                        name={show ? 'chevron-up' : 'chevron-down'}
                        size={normalize(22)}
                    />
                </Card>
            </TouchItem>
            {show && (
                <DetailContainer>
                    <Text align="flex-start" style={{ lineHeight: normalize(16) }}>
                        {description}
                    </Text>

                    <Button
                        onClick={() => {
                            rbRef.current?.open();
                        }}
                        title={buttonTitle}
                        fontSize={textSizes.h9}
                        style={styles.button}
                    />
                </DetailContainer>
            )}
        </View>
    );
};

const Row = styled.View`
  flex-direction: row;
`;

const Card = styled(Row)`
  width: 100%;
  justify-content: space-between;
  padding: ${normalize(10)}px;
`;

const DetailContainer = styled.View`
  width: 100%;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  padding-left: ${normalize(40)}px;
`;

const styles = StyleSheet.create({
    button: {
        width: normalize(120),
        height: normalize(30),
        marginTop: normalize(10),
        marginBottom: normalize(15),
        borderRadius: normalize(15),
    },
});
