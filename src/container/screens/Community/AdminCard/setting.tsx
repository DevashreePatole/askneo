import React, { FC, useRef } from 'react';

import { StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';

import { Colors } from '../../../../assets/styles/color';
import { normalize } from '../../../../lib/globals';
import { AdminActionCard } from './adminActionCard';

export const Setting:FC = () => {
    const refRBSheet = useRef<RBSheet>(null);

    return (
        <Container>
            <AdminActionCard
                title="Setting up your community"
                description="Edit the community setting by adding some restrictions."
                rbRef={refRBSheet}
                buttonTitle="Setting visuals"
            />

            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                animationType={'slide'}
                customStyles={customStyles}>
                <View></View>
            </RBSheet>
        </Container>
    );
};

const Container = styled.View``;

const customStyles = StyleSheet.create({
    wrapper: {
        backgroundColor: 'transparent',
    },
    draggableIcon: {
        backgroundColor: Colors.black,
    },
    container: {
        borderTopRightRadius: normalize(18),
        borderTopLeftRadius: normalize(18),
        borderColor: '#d6d6d6',
        borderWidth: normalize(1),
        height: '90%',
    },
});
