import React, { FC } from 'react';

import styled from 'styled-components/native';

import { Colors } from '../../../../assets/styles/color';
import { Text } from '../../../../components';
import { normalize } from '../../../../lib/globals';
import { AddPeople } from './add';
import { EditCommunity } from './edit';
import { Setting } from './setting';

export const AdminCard: FC = () => {
    return (
        <Container>
            <TitleContainer>
                <Text style={{ marginLeft: normalize(10) }} fontWeight="500">
                    Admin Dashboard
                </Text>
            </TitleContainer>
            <EditCommunity />
            <AddPeople />
            <Setting />
        </Container>
    );
};

const Container = styled.View`
  width: 100%;
  background-color: ${Colors.white};
  border-radius: ${normalize(5)}px;
  padding: ${normalize(8)}px;
`;

const TitleContainer = styled.View`
  width: 100%;
  height: ${normalize(40)}px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-bottom-color: ${Colors.lightGray};
  border-bottom-width: 1px;
`;
