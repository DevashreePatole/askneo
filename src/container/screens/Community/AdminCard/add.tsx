import React, { FC, useRef, useState } from 'react';

import { ScrollView, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';

import { Colors } from '../../../../assets/styles/color';
import { Input, Text } from '../../../../components';
import Button from '../../../../components/Button';
import { normalize, textSizes } from '../../../../lib/globals';
import { ImageComponent } from '../../../../components/Image';
import { AdminActionCard } from './adminActionCard';

export const AddPeople:FC = () => {
  const refRBSheet = useRef<RBSheet>(null);
  const [input, setInput] = useState('');

  const [users, setUsers] = useState([
    {
      name: 'Nilesh Chavan',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Vartika Verma',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
    {
      name: 'Akashsingh Lodh',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Devashree Patole',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
    {
      name: 'Kulwindar Singh',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Diksha Choudhary',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
  ]);

  const [filteredList, setFilteredList] = useState([...users]);

  const handleButtonClick = (user, index) => {
    const data = [...users];
    if (data[index].added == undefined) {
      data[index].added = true;
    } else {
      data[index].added = undefined;
    }
    setUsers(data);
  };

  const handleFilterChange = (val) => {
    const data = [...users];
    setInput(val);
    setFilteredList(
      data.filter((user) => {
        return user.name.toLowerCase().includes(val.toLowerCase());
      }),
    );
  };

  return (
    <Container>
      <AdminActionCard
        title="Add people in community"
        description="Invite more people so that you can grow your community"
        rbRef={refRBSheet}
        buttonTitle="Add People"
      />

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={CustomStyles}>
        <View>
          <SearchContainer>
            <Input
              style={styles.search}
              value={input}
              onChangeText={(val) => {
                handleFilterChange(val);
              }}
              placeholder="search by name"
            />
          </SearchContainer>
          <ScrollView showsVerticalScrollIndicator={false}>
            {filteredList.map((user, index) => {
              return (
                <UserCards key={index}>
                  <UserInfo>
                    <ImageComponent
                      source={{
                        uri: user.profileImage,
                      }}
                      style={styles.image}
                    />
                    <View style={{ marginLeft: normalize(5) }}>
                      <Text align="flex-start">{user.name}</Text>
                    </View>
                  </UserInfo>
                  <Button
                    title={user.added == undefined ? 'Add' : 'Remove'}
                    style={{
                      ...styles.button,
                      backgroundColor:
                        user.added == undefined
                          ? Colors.primaryColor
                          : Colors.black,
                    }}
                    color={
                      user.added == undefined ? Colors.black : Colors.white
                    }
                    fontSize={textSizes.h10}
                    onClick={() => {
                      handleButtonClick(user, index);
                    }}
                  />
                </UserCards>
              );
            })}
          </ScrollView>
        </View>
      </RBSheet>
    </Container>
  );
};

const Row = styled.View`
  flex-direction: row;
`;

const Container = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${Colors.lightGray};
`;

const UserCards = styled(Row)`
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${normalize(10)}px;
  padding-left: ${normalize(15)}px;
  padding-right: ${normalize(10)}px;
  border-bottom-width: 1px;
  border-bottom-color: ${Colors.darkGray};
  padding-bottom: ${normalize(10)}px;
`;

const UserInfo = styled(Row)`
  width: 70%;
  flex-direction: row;
  align-items: center;
`;

const SearchContainer = styled.View`
  width: 100%;
  padding: ${normalize(10)}px;
  margin-bottom: ${normalize(15)}px;
`;

const styles = StyleSheet.create({
  image: {
    width: normalize(30),
    height: normalize(30),
    borderRadius: normalize(30),
  },
  button: {
    width: normalize(75),
    height: normalize(35),
    borderRadius: normalize(20),
  },
  search: {
    marginTop: -normalize(35),
    height: normalize(45),
    borderRadius: normalize(5),
    borderWidth: 1,
    borderColor: Colors.darkGray,
    paddingLeft: normalize(20),
  },
});

const CustomStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(18),
    borderTopLeftRadius: normalize(18),
    borderColor: '#d6d6d6',
    borderWidth: normalize(1),
    height: '90%',
  },
});
