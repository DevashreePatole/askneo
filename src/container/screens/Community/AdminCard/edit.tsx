import React, { FC, useRef } from 'react';

import { StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';

import { Colors } from '../../../../assets/styles/color';
import { Icon, Text, TouchItem } from '../../../../components';
import Button from '../../../../components/Button';
import { normalize, textSizes } from '../../../../lib/globals';
import { ImageComponent } from '../../../../components/Image';
import { AdminActionCard } from './adminActionCard';

export const EditCommunity:FC = () => {
  const refRBSheet = useRef<RBSheet>(null);

  return (
    <Container>
      <AdminActionCard
        title="Edit visuals of your community"
        description="Edit the community setting by adding some restrictions."
        rbRef={refRBSheet}
        buttonTitle="Edit visuals"
      />

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        animationType={'slide'}
        customStyles={CustomStyles}>
        <SheetHeader>
          <Icon
            type="Entypo"
            name="cross"
            size={normalize(35)}
            style={{ opacity: 0.7 }}
            onPress={() => {
              refRBSheet.current?.close();
            }}
          />
          <Button
            title="Done"
            onClick={() => { }}
            style={styles.sheetButton}
            fontSize={textSizes.h8}
          />
        </SheetHeader>
        <SheetMainContainer>
          <View>
            <Text align="flex-start" fontWeight="500" size={textSizes.h8}>
              Edit visuals
            </Text>
            <Text
              align="flex-start"
              size={textSizes.h9}
              style={{ marginTop: normalize(5) }}>
              You can edit this section in Community Setting
            </Text>
          </View>

          <View style={{ marginTop: normalize(40) }}>
            <Text fontWeight="500" size={textSizes.h9} align="flex-start">
              Primary Image
            </Text>
            <ImageContainer>
              <ImageComponent
                source={{
                  uri:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS16ZAt8VDhkcpY4o_Gl-SPR7AREYRe-3qsA&usqp=CAU',
                }}
                style={styles.image}
              />
              <EditButton onPress={() => { }}>
                <IconContainer>
                  <Icon type="Feather" name="edit-2" size={normalize(15)} />
                </IconContainer>
              </EditButton>
            </ImageContainer>
          </View>

          <View style={{ marginTop: normalize(40) }}>
            <Text fontWeight="500" size={textSizes.h9} align="flex-start">
              Cover Image
            </Text>
            <ImageContainer style={{ width: '80%' }}>
              <ImageComponent
                source={{
                  uri:
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSHlSE87ib88fRYZuuP_8u23gOoTm-6e0lEvw&usqp=CAU',
                }}
                style={{
                  ...styles.image,
                  borderRadius: normalize(10),
                }}
              />
              <EditButton onPress={() => { }}>
                <IconContainer>
                  <Icon type="Feather" name="edit-2" size={normalize(15)} />
                </IconContainer>
              </EditButton>
            </ImageContainer>
          </View>
        </SheetMainContainer>
      </RBSheet>
    </Container>
  );
};

const Row = styled.View`
  flex-direction: row;
`;

const Container = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: ${Colors.lightGray};
`;

const SheetHeader = styled(Row)`
  width: 100%;
  justify-content: space-between;
`;

const SheetMainContainer = styled.View`
  margin-top: ${normalize(20)}px;
  margin-left: ${normalize(10)}px;
`;

const ImageContainer = styled.View`
  width: ${normalize(90)}px;
  height: ${normalize(90)}px;
  margin-top: ${normalize(20)}px;
`;

const EditButton = styled(TouchItem)`
  position: absolute;
  top: -${normalize(10)}px;
  right: -${normalize(10)}px;
  width: ${normalize(35)}px;
  height: ${normalize(35)}px;
  border-radius: ${normalize(25)}px;
`;

const IconContainer = styled(Row)`
  background-color: ${Colors.primaryColor};
  width: 100%;
  height: 100%;
  border-width: 2px;
  border-color: ${Colors.white};
  border-radius: ${normalize(25)}px;
  justify-content: center;
  align-items: center;
`;

const styles = StyleSheet.create({
  sheetButton: {
    width: normalize(90),
    height: normalize(40),
    borderRadius: normalize(20),
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: normalize(30),
  },
});

const CustomStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(18),
    borderTopLeftRadius: normalize(18),
    borderColor: '#d6d6d6',
    borderWidth: normalize(1),
    height: '90%',
    paddingLeft: normalize(10),
    paddingRight: normalize(10),
  },
});
