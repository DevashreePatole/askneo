import React, { FC } from 'react';

import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';

import { Colors } from '../../../assets/styles/color';
import { ImageBack, Text, TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';

interface CommunityData {
    id: string;
    name: string;
    member: number;
    category: string;
    admin: string;
    description: string;
    primaryImage: string;
    secondaryImage: string;
}

interface Props {
    data: CommunityData;
    navigation: any;
}

export const CommunityCard: FC<Props> = ({ data, navigation }) => {
    return (
        <Container
            key={data.id}
            onPress={() => {
                navigation.navigate('CommunityDetail', {
                    data: data
                });
            }}>
            <Card>
                <CoverImageContainer>
                    <ImageBack
                        source={{ uri: data.primaryImage }}
                        style={{ width: '100%', height: '100%' }}>
                        <InnerContainer />
                    </ImageBack>
                </CoverImageContainer>
                <ContentBack>
                    <ContentContainer>
                        <Text fontWeight="500" numberOfLines={2} style={styles.name}>
                            {data.name}
                        </Text>
                        <Text numberOfLines={3} size={textSizes.h11} style={styles.description}>
                            {data.description}
                        </Text>
                        <Text
                            size={textSizes.h12}
                            style={styles.member}>{`${data.member} Members`}</Text>
                    </ContentContainer>
                </ContentBack>
                <MainImageContainer>
                    <ImageBack
                        source={{ uri: data.secondaryImage }}
                        style={{ width: '100%', height: '100%' }}
                        resizeMode={'stretch'}>
                        <InnerContainer />
                    </ImageBack>
                </MainImageContainer>
            </Card>
        </Container>
    );
};

const Container = styled(TouchItem)`
  padding-right: ${normalize(5)}px;
  padding-left: ${normalize(5)}px;
  margin-top: ${normalize(10)}px;
  margin-bottom: ${normalize(10)}px;
`;

const Card = styled.View`
  width: ${normalize(150)}px;
  height: ${normalize(215)}px;
  background-color: white;
  border-radius: ${normalize(10)}px;
  overflow: hidden;
`;

const CoverImageContainer = styled.View`
  height: ${normalize(55)}px;
  width: 100%;
  background-color: ${Colors.darkGray};
`;

const InnerContainer = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.2);
`;

const MainImageContainer = styled.View`
  position: absolute;
  width: ${normalize(45)}px;
  height: ${normalize(45)}px;
  background-color: ${Colors.darkGray};
  left: 35%;
  top: ${normalize(28)}px;
  border-radius: ${normalize(13)}px;
  overflow: hidden;
  border-width: 2px;
  border-color: ${Colors.white};
`;

const ContentBack = styled.View`
  border-top-right-radius: ${normalize(10)}px;
  border-top-left-radius: ${normalize(10)}px;
  background-color: ${Colors.white};
  margin-top: -${normalize(5)}px; ;
`;

const ContentContainer = styled.View`
  width: 100%;
  margin-top: ${normalize(22)}px;
  padding: ${normalize(8)}px;
`;

const styles = StyleSheet.create({
    name: {
        lineHeight: normalize(16),
        textAlign: 'center',
    },
    description: {
        textAlign: 'center',
        marginTop: normalize(8),
        opacity: 0.8,
        lineHeight: normalize(15),
        paddingHorizontal: normalize(8)
    },
    member: {
        textAlign: 'center',
        marginTop: normalize(15),
        opacity: 0.8,
    },
});
