import React, { FC } from 'react';

import { ScrollView } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/core';

import { Colors } from '../../../assets/styles/color';
import { Container } from '../../../components';
import { normalize } from '../../../lib/globals';
import { RootStackParamList } from '../../navigators/stack/communityStack';
import { CommunityHeader } from './communityHeader';
import { CommunityTabBar } from './tabbar';


interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'CommunityDetail'>;
    route: RouteProp<RootStackParamList,'CommunityDetail'>;
}

export const CommunityDetail: FC<Props> = ({ navigation, route }) => {
    return (
        <ScreenContainer withKeyboard={false}>
            <ScrollView>
                <CommunityHeader navigation={navigation} data={route.params.data} />
                <CommunityTabBar navigation={navigation} data={route.params.data} />
            </ScrollView>
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: 0;
  padding-bottom: ${normalize(10)}px;
  background-color: ${Colors.appGray};
`;
