import React, { FC, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../../assets/styles/color';
import { Container } from '../../../../components';
import { RootStackParamList } from '../../../navigators/stack/communityStack';
import { QuestionCard } from '../../QuestionAnswer/QuestionCard';

interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'CommunityDetail'>;
}

export const Questions: FC<Props> = ({ navigation }) => {
    const [loading, setLoading] = useState(false);
    const [questions, setQuestions] = useState([
        {
            id: '1',
            title: 'What is the unscrambled word of OTMH?',
            authorDes: 'React Native',
            author: 'Vartika Verma',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGmwVZ3vbHoQiz9Q2zC3dDJYJ-uCBNymbYTw&usqp=CAU',
            tags: ['C++', 'C', 'Java', 'Javascript'],
            answers: [{}, {}],
            postedTime: '2021-02-06T16:34:00',
            likes: 10,
            dislikes: 20,
            views: 50,
        },
        {
            id: '2',
            title: 'How can I get a link to my Airbnb listing to send to friends?',
            author: 'Vartika Verma',
            authorDes: 'React Native',
            authorImage:
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSGmwVZ3vbHoQiz9Q2zC3dDJYJ-uCBNymbYTw&usqp=CAU',
            tags: ['Python', 'ML', 'Traveling'],
            answers: [],
            postedTime: '2021-04-06T16:00:00',
            likes: 10,
            dislikes: 20,
            views: 999,
        },
    ]);

    const _renderItems = ({ item, index }) => {
        return (
            <QuestionCard
                key={index}
                index={index}
                data={item}
                handleLike={() => { }}
                handleDislike={() => { }}
                navigation={navigation}
                swipable={false}
                // userQuestion={true}
            // onPress={()=> {}}
            />
        );
    };

    return (
        <ScreenContainer withKeyboard={false} loading={loading}>
            <FlatList
                data={questions}
                keyExtractor={(item, index) => index.toString()}
                renderItem={_renderItems}
                showsVerticalScrollIndicator={false}
            />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  justify-content: center;
  background-color: ${Colors.appGray};
`;
