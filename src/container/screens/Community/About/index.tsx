import React, { FC, useState } from 'react';

import { ScrollView, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../../assets/styles/color';
import { Text } from '../../../../components';
import { ImageComponent } from '../../../../components/Image';
import { normalize, textSizes } from '../../../../lib/globals';
import { RootStackParamList } from '../../../navigators/stack/communityStack';

interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'CommunityDetail'>;
}

export const Abouts: FC<Props> = ({ navigation }) => {
    const [data, setData] = useState([
        {
            title: 'Admin',
            authority: 'Admin can manage uploaded posts, question and settings',
            members: [
                {
                    name: 'Vartika Verma',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
                },
            ],
        },
        {
            title: 'Members',
            authority: 'Member can upload posts and questions',
            members: [
                {
                    name: 'Nilesh Chavan',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
                },
                {
                    name: 'Devashree Patole',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
                },
                {
                    name: 'Akashsingh Lodh',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
                },
                {
                    name: 'Diksha Choudhary',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
                },
                {
                    name: 'Kulwinder Singh',
                    designation: 'React Native Developer',
                    profileImage:
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
                },
            ],
        },
    ]);
    return (
        <ScreenContainer>
            <ScrollView>
                {data.map((res, index) => {
                    return (
                        <CardContainer key={index}>
                            <CardHeader>
                                <Text size={textSizes.h8} fontWeight="400" align="flex-start">
                                    {res.title}
                                </Text>
                                <Text
                                    size={textSizes.h10}
                                    align="flex-start"
                                    style={styles.authority}>
                                    {res.authority}
                                </Text>
                            </CardHeader>
                            {res.members.map((r, index) => {
                                return (
                                    <UserCard key={index}>
                                        <ImageComponent
                                            style={styles.userProfile}
                                            source={{
                                                uri: r.profileImage,
                                            }}
                                        />
                                        <UserInfo>
                                            <Text
                                                size={textSizes.h9}
                                                align="flex-start"
                                                fontWeight="500">
                                                {r.name}
                                            </Text>
                                            <Text
                                                size={textSizes.h10}
                                                align="flex-start"
                                                style={{ marginTop: normalize(5) }}>
                                                {r.designation}
                                            </Text>
                                        </UserInfo>
                                    </UserCard>
                                );
                            })}
                        </CardContainer>
                    );
                })}
            </ScrollView>
        </ScreenContainer>
    );
};

const ScreenContainer = styled.View`
  justify-content: center;
  background-color: ${Colors.lightGray};
`;

const CardContainer = styled.View`
  width: 100%;
  margin-bottom: ${normalize(10)}px;
`;

const CardHeader = styled.View`
  padding: ${normalize(15)}px;
  padding-top: ${normalize(20)}px;
  padding-bottom: ${normalize(20)}px;
  background-color: ${Colors.white};
`;

const UserCard = styled.View`
  padding: ${normalize(15)}px;
  margin-top: ${normalize(2)}px;
  background-color: ${Colors.white};
  padding-top: ${normalize(12)}px;
  padding-bottom: ${normalize(12)}px;
  flex-direction: row;
  align-items: center;
`;

const UserInfo = styled.View`
  flex-direction: column;
  margin-left: ${normalize(10)}px;
`;

const styles = StyleSheet.create({
    userProfile: {
        width: normalize(40),
        height: normalize(40),
        borderRadius: normalize(20),
    },
    authority: {
        marginTop: normalize(5),
        lineHeight: normalize(16),
        color: '#585858',
    },
});
