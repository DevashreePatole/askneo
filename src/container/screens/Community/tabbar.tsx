import React, { FC, useState } from 'react';

import { StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../assets/styles/color';
import { Text, TouchItem } from '../../../components';
import { normalize, textSizes } from '../../../lib/globals';
import { RootStackParamList } from '../../navigators/stack/communityStack';
import { Abouts } from './About';
import { Posts } from './Posts';
import { Questions } from './Questions';

interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'CommunityDetail'>;
}

export const CommunityTabBar: FC<Props> = ({ navigation }) => {
    const [options, setOptions] = useState([
        {
            name: 'ABOUT',
        },
        {
            name: 'POSTS',
        },
        {
            name: 'QUESTIONS',
        },
    ]);

    const [currentIndex, setCurrentIndex] = useState(0);

    const renderContent = () => {
        if (currentIndex == 0) {
            return <Abouts navigation={navigation}/>;
        } else if (currentIndex == 1) {
            return <Posts navigation={navigation} />;
        } else {
            return <Questions navigation={navigation} />;
        }
    };

    return (
        <View style={{ width: '100%' }}>
            <TabContainer>
                {options.map((op, index) => {
                    return (
                        <TouchItem
                            key={index}
                            onPress={() => {
                                setCurrentIndex(index);
                            }}
                            style={{
                                ...styles.tabButton,
                                borderBottomWidth: currentIndex == index ? normalize(4) : 0,
                            }}>
                            <Text size={textSizes.h9} style={{ color: Colors.white }}>
                                {op.name}
                            </Text>
                        </TouchItem>
                    );
                })}
            </TabContainer>
            {renderContent()}
        </View>
    );
};

const TabContainer = styled.View`
  width: 100%;
  flex-direction: row;
  background-color: ${Colors.headerBackground};
`;

const styles = StyleSheet.create({
    tabButton: {
        flex: 1,
        paddingTop: normalize(15),
        paddingBottom: normalize(15),
        borderBottomColor: Colors.primaryColor,
    },
});
