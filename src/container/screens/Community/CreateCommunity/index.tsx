import React, { FC, useRef, useState } from 'react';

import { ScrollView, StyleSheet, View } from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import { StackNavigationProp } from '@react-navigation/stack';

import AlertModal from '../../../../components/AlertModal';
import { Colors } from '../../../../assets/styles/color';
import { Container, Input, Text, TouchItem } from '../../../../components';
import Button from '../../../../components/Button';
import { Icon } from '../../../../components/Icon';
import { normalize, textSizes } from '../../../../lib/globals';
import { ImageComponent } from '../../../../components/Image';
import { RootStackParamList } from '../../../navigators/stack/communityStack';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'Community'>;
}

export const CreateCommunity: FC<Props> = ({ navigation }) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [techonlogy, setTechnology] = useState('');
  const [input, setInput] = useState('');
  const refRBSheet = useRef<RBSheet>(null);

  const [users, setUsers] = useState([
    {
      name: 'Nilesh Chavan',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Vartika Verma',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
    {
      name: 'Akashsingh Lodh',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Devashree Patole',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
    {
      name: 'Kulwindar Singh',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQaBHUeRlTyK4kVHnOVvYmAz1G7NyYyH9AgFA&usqp=CAU',
    },
    {
      name: 'Diksha Choudhary',
      profileImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHh9Y7tcmwEr_od64uK2G5DokOr_yxia1xVw&usqp=CAU',
    },
  ]);

  const [filteredList, setFilteredList] = useState([...users]);
  let alertRef = useRef();
  const [loading, setLoading] = useState(false);

  const handleButtonClick = (user, index) => {
    const data = [...users];
    if (data[index].added == undefined) {
      data[index].added = true;
    } else {
      data[index].added = undefined;
    }
    setUsers(data);
  };

  const handleFilterChange = (val) => {
    const data = [...users];
    setInput(val);
    setFilteredList(
      data.filter((user) => {
        return user.name.toLowerCase().includes(val.toLowerCase());
      }),
    );
  };

  const handleOnSubmit = () => {
    if (name != '' && description != '' && techonlogy != '') {
      setLoading(true);
      setTimeout(() => {
        setLoading(false);
        const warningModal = () =>
          alertRef?.current?.setOptionAndOpen(
            'success',
            'Community Creaded Successfully',
            () => navigation.goBack(),
          );
        warningModal();
      }, 3000);
    } else {
      Toast.show('Please fill all the data', Toast.SHORT);
    }
  };

  return (
    <ScreenContainer loading={loading}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text
          align="flex-start"
          size={textSizes.h8}
          fontWeight="500"
          style={{ marginTop: normalize(20) }}>
          Create a Community
        </Text>
        <Text
          align="flex-start"
          size={textSizes.h9}
          style={{ marginTop: normalize(10), lineHeight: normalize(20) }}>
          Create a Community to share your interest, curate, content and more.
        </Text>
        <View style={{ marginTop: normalize(20) }}>
          <Input
            label="Name"
            value={name}
            onChangeText={(val) => setName(val)}
            placeholder="Enter a name for your community"
          />
          <View style={{ marginTop: normalize(15) }}>
            <Input
              label="Description"
              value={description}
              onChangeText={(val) => setDescription(val)}
              placeholder="Description of your community"
            />
          </View>
          <View style={{ marginTop: normalize(15) }}>
            <Input
              label="Techonlogy"
              value={techonlogy}
              onChangeText={(val) => setTechnology(val)}
              placeholder="Node Js, React Native, Python,..."
            />
          </View>
          <TouchItem
            style={{ width: normalize(150) }}
            onPress={() => {
              refRBSheet.current?.open();
            }}>
            <InviteButtonContainer>
              <Text style={{ marginRight: normalize(10) }}>Invite</Text>
              <Icon type="Entypo" name="add-user" size={normalize(25)} />
            </InviteButtonContainer>
          </TouchItem>
          <View style={{ marginTop: normalize(50) }}>
            <Button
              title="Create"
              style={{ width: normalize(150), height: normalize(45) }}
              onClick={() => {
                handleOnSubmit();
              }}
            />
          </View>
        </View>

        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          animationType={'slide'}
          customStyles={customStyles}>
          <View>
            <SearchContainer>
              <Input
                // label="Techonlogy"
                style={styles.search}
                value={input}
                onChangeText={(val) => {
                  handleFilterChange(val);
                }}
                placeholder="search by name"
              />
            </SearchContainer>
            <ScrollView showsVerticalScrollIndicator={false}>
              {filteredList.map((user, index) => {
                return (
                  <UserCards key={index}>
                    <UserInfo>
                      <ImageComponent
                        source={{
                          uri: user.profileImage,
                        }}
                        style={styles.image}
                      />
                      <View style={{ marginLeft: normalize(5) }}>
                        <Text align="flex-start">{user.name}</Text>
                      </View>
                    </UserInfo>
                    <Button
                      title={user.added == undefined ? 'Add' : 'Remove'}
                      style={{
                        ...styles.buttom,
                        backgroundColor:
                          user.added == undefined
                            ? Colors.primaryColor
                            : Colors.black,
                      }}
                      color={
                        user.added == undefined ? Colors.black : Colors.white
                      }
                      fontSize={textSizes.h10}
                      onClick={() => {
                        handleButtonClick(user, index);
                      }}
                    />
                  </UserCards>
                );
              })}
            </ScrollView>
          </View>
        </RBSheet>
      </ScrollView>
      <AlertModal ref={alertRef} />
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: ${normalize(20)}px;
  padding-top: ${normalize(0)}px;
`;

const InviteButtonContainer = styled.View`
  margin-top: ${normalize(40)}px;
  height: ${normalize(50)}px;
  background-color: ${Colors.lightGray};
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-radius: ${normalize(10)}px;
`;

const UserCards = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${normalize(10)}px;
  padding-left: ${normalize(15)}px;
  padding-right: ${normalize(10)}px;
  border-bottom-width: 1px;
  border-bottom-color: ${Colors.darkGray};
  padding-bottom: ${normalize(10)}px;
`;

const UserInfo = styled.View`
  width: 70%;
  flex-direction: row;
  align-items: center;
`;

const SearchContainer = styled.View`
  width: 100%;
  padding: ${normalize(10)}px;
  margin-bottom: ${normalize(15)}px;
`;

const styles = StyleSheet.create({
  image: {
    width: normalize(30),
    height: normalize(30),
    borderRadius: normalize(30),
  },
  buttom: {
    width: normalize(75),
    height: normalize(35),
    borderRadius: normalize(20),
  },
  search: {
    marginTop: -normalize(35),
    height: normalize(45),
    borderRadius: normalize(5),
    borderWidth: 1,
    borderColor: Colors.darkGray,
    paddingLeft: normalize(20),
  },
});

const customStyles = StyleSheet.create({
  wrapper: {
    backgroundColor: 'transparent',
  },
  draggableIcon: {
    backgroundColor: Colors.black,
  },
  container: {
    borderTopRightRadius: normalize(18),
    borderTopLeftRadius: normalize(18),
    borderColor: '#d6d6d6',
    borderWidth: normalize(1),
    height: '90%',
  },
});
