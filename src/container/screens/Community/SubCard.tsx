import React, { FC } from 'react';

import { Image, View } from 'react-native';
import styled from 'styled-components/native';
import { prop } from 'styled-tools';

import { Colors } from '../../../assets/styles/color';
import { Text, TouchItem } from '../../../components';
import { Icon } from '../../../components/Icon';
import { normalize } from '../../../lib/globals';

interface Community {
    name: string;
    secondaryImage: string;
}

interface Prop {
    data: Community;
}

export const SubCard: FC<Prop> = ({ data }) => {
    return (
        <TouchItem onPress={() => { }}>
            <Container>
                <View style={{ flexDirection: 'row' }}>
                    <CardImage source={{ uri: data.secondaryImage }} size={normalize(28)} />
                    <Text fontWeight="500" style={{ marginLeft: normalize(11) }}>
                        {data.name}
                    </Text>
                </View>
                <View>
                    <Icon
                        type="MaterialIcons"
                        color={Colors.darkGray}
                        name={'navigate-next'}
                        size={normalize(32)}
                    />
                </View>
            </Container>
        </TouchItem>
    );
};

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding-left: ${normalize(10)}px;
  padding-right: ${normalize(10)}px;
  padding-bottom: ${normalize(7)}px;
  padding-top: ${normalize(7)}px;
  border-bottom-color: ${Colors.lightGray};
  border-bottom-width: 1px;
  justify-content: space-between;
`;

const CardImage = styled(Image) <{ size: number }>`
  width: ${prop('size')}px;
  height: ${prop('size')}px;
  border-radius: ${normalize(7)}px;
`;
