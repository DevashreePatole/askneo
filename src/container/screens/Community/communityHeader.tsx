import React, { FC } from 'react';

import {
    StatusBar,
    Platform,
    View,
    StyleSheet,
} from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { Colors } from '../../../assets/styles/color';
import { ImageBack, Text, TouchItem } from '../../../components';
import Button from '../../../components/Button';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';
import { normalize, textSizes } from '../../../lib/globals';
import { RootStackParamList } from '../../navigators/stack/communityStack';


interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'CommunityDetail'>;
    data: any;
}

export const CommunityHeader: FC<Props> = ({ navigation,data }) => {
    return (
        <MainContainer>
            {Platform.OS == 'ios' && (
                <StatusBar
                    backgroundColor="#b3e6ff"
                    barStyle="dark-content"
                    hidden={true}
                />
            )}
            <View>
                <ImageBack
                    source={{
                        uri: `${data.primaryImage}`,
                    }}
                    style={{ width: '100%', height: normalize(180) }}
                    resizeMode={'cover'}>
                    <HeaderContainer>
                        <TouchItem
                            onPress={() => navigation.goBack()}
                            style={styles.floatButtom}>
                            <Icon
                                type="Entypo"
                                name="chevron-left"
                                size={normalize(25)}
                                color={Colors.white}
                            />
                        </TouchItem>
                        <TouchItem onPress={() => { }} style={styles.floatButtom}>
                            <Icon
                                type="Feather"
                                name="more-horizontal"
                                size={normalize(25)}
                                color={Colors.white}
                            />
                        </TouchItem>
                    </HeaderContainer>
                </ImageBack>

                <HeaderInfo>
                    <View>
                        <View style={{ position: 'absolute', top: -normalize(50) }}>
                            <ImageComponent
                                style={styles.primaryImage}
                                source={{
                                    uri:
                                        `${data.secondaryImage}`,
                                }}
                            />
                        </View>
                        <CommunityInfo>
                            <Text
                                size={textSizes.h7}
                                align="flex-start"
                                style={{ color: Colors.white }}
                                fontWeight="500">
                                {data.name}
                            </Text>
                            <Text
                                size={textSizes.h9}
                                align="flex-start"
                                style={styles.communityObjective}>
                                {data.description}
                            </Text>
                        </CommunityInfo>
                    </View>
                    <ButtonContainer>
                        <Button
                            title="Follow"
                            onClick={() => { }}
                            style={styles.button}
                            fontSize={textSizes.h8}
                            color={Colors.black}
                        />
                    </ButtonContainer>
                </HeaderInfo>
            </View>
        </MainContainer>
    );
};

const MainContainer = styled.View`
  background-color: ${Colors.lightGray};
`;

const HeaderInfo = styled.View`
  width: 100%;
  background-color: #585858;
  padding: ${normalize(15)}px;
  margin-top: -${normalize(10)}px;
`;

const CommunityInfo = styled.View`
  margin-top: ${normalize(40)}px;
  flex-direction: column;
  align-items: center;
`;

const HeaderContainer = styled.View`
  position: absolute;
  flex-direction: row;
  justify-content: space-between;
  top: ${normalize(25)}px;
  width: 100%;
  align-items: center;
  padding-right: ${normalize(10)}px;
  padding-left: ${normalize(10)}px;
`;

const ButtonContainer = styled.View`
  position: absolute;
  right: ${normalize(10)}px;
  top: ${normalize(12)}px;
`;

const styles = StyleSheet.create({
    primaryImage: {
        width: normalize(80),
        height: normalize(80),
        borderRadius: normalize(20),
    },
    button: {
        width: normalize(110),
        height: normalize(40),
        borderRadius: normalize(30),
    },
    floatButtom: {
        padding: normalize(5),
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 30,
    },
    communityObjective: {
        marginTop: normalize(5),
        color: Colors.white,
        lineHeight: normalize(16),
    },
});
