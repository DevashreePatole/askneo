import React, { FC, useEffect, useRef, useState } from 'react';

import {
    Animated,
    Image,
    StyleSheet,
    TouchableOpacity,
    View,
} from 'react-native';
import styled from 'styled-components/native';
import RBSheet from 'react-native-raw-bottom-sheet';
import ImageView from 'react-native-image-viewing';
import Toast from 'react-native-simple-toast';

import { Colors } from '../../../assets/styles/color';
import { Container, Text, TouchItem } from '../../../components';
import { AddComment } from '../../../components/AddComment';
import { normalize, textSizes, WINDOW } from '../../../lib/globals';
import { AnimateIcon, AnimatedIcon } from '../../../components/AnimatedIcon';
import { convertToBase64, shareData } from '../../../lib/helper';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

interface MemeProp {
    author: string;
    authorImage: string;
    memeImage: string;
    caption: string;
}

interface Props {
    meme: MemeProp;
    userMeme?: boolean;
}

export const DisplayCard: FC<Props> = ({ meme, userMeme = false }) => {
    const [likes, setLikes] = useState(2);
    const [disabled, setDisabled] = useState(false);
    const [visible, setVisible] = useState(false);
    const currentValue = new Animated.Value(1);
    const [bookMarked, setBookMark] = useState(false);
    const refRBSheet = useRef<RBSheet>(null);
    const [loading, setLoading] = useState(false);
    const [moreS, setMoreS] = useState(false);
    const [visibleImage, setVisibleImage] = useState(false);
    const [images, setImages] = useState([{ uri: meme.memeImage }]);
    const [height, setHeight] = useState(400);
    const [imageData, setImageData] = useState('');

    let backCount = 0;
    let backTimer;

    useEffect(() => {
        if (disabled == true) {
            AnimateIcon(currentValue, () => {
                setVisible(false);
            });
        }
    }, [disabled]);

    const fetchData = async () => {
        convertToBase64(`${meme.memeImage}`)
            .then((data) => {
                setImageData(data);
            })
            .catch((e) => {
                console.log(e);
            })
            .finally(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        if (loading == true) {
            fetchData();
        }
    }, [loading]);

    const getBase64Data = async () => {
        setLoading(true);
    };

    const handleLikeClick = () => {
        if (!disabled) {
            setLikes((prev) => prev + 1);
            setDisabled(true);
            setVisible(true);
        } else {
            setLikes((prev) => prev - 1);
            setDisabled(false);
            setVisible(false);
        }
    };

    const shareMeme = async () => {
        shareData(imageData, '');
    };

    const handleDoublePress = () => {
        backCount++;
        if (backCount == 2) {
            clearTimeout(backTimer);
            handleLikeClick();
        } else {
            backTimer = setTimeout(() => {
                backCount = 0;
            }, 500);
        }
    };

    useEffect(() => {
        if (loading == false && imageData != '') {
            setTimeout(() => {
                shareMeme();
            }, 500);
        }
    }, [loading, imageData]);

    useEffect(() => {
        Image.getSize(
            meme.memeImage,
            (srcWidth, srcHeight) => {
                const maxHeight = WINDOW.height;
                const maxWidth = WINDOW.width;

                const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

                const finalHeight = srcHeight * ratio;
                if (finalHeight > WINDOW.height - 100 && finalHeight > 600) {
                    setHeight(WINDOW.height - 100);
                } else {
                    setHeight(finalHeight);
                }
            },
            (error) => {
                // console.log('error:', error);
            },
        );
    }, []);

    const handleBookmark = () => {
        if (bookMarked == false) {
            Toast.show('Added To Bookmark', Toast.SHORT, [
                'UIAlertController',
            ]);
        } else {
            Toast.show('Removed From Bookmark', Toast.SHORT, [
                'UIAlertController',
            ]);
        }
        setBookMark((prevState) => !prevState);
    };

    return (
        <Card loading={loading} withKeyboard={false}>
            <HeaderContainer>
                <Row style={{ alignItems: 'center' }}>
                    <ImageComponent
                        source={{ uri: meme.authorImage }}
                        style={styles.authorImage}
                    />
                    <Text size={textSizes.h11} fontWeight="500">
                        {meme.author}
                    </Text>
                </Row>
                <TouchItem
                    onPress={() => {
                        setMoreS((prevState) => !prevState);
                    }}>
                    <Icon type="Feather" name="more-vertical" size={normalize(18)} />
                </TouchItem>
            </HeaderContainer>
            {meme.caption ? (
                <View
                    style={{
                        width: '95%',
                        paddingLeft: normalize(10),
                        paddingBottom: normalize(4),
                    }}>
                    <Text
                        size={textSizes.h10}
                        align="flex-start"
                        style={{ paddingTop: normalize(3), lineHeight: normalize(20) }}>
                        {meme.caption}
                    </Text>
                </View>
            ) : null}

            <ImageContainer>
                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                        handleDoublePress();
                    }}>
                    <ImageComponent
                        source={{ uri: meme.memeImage }}
                        style={{ width: '100%', height: height }}
                        resizeMode={'stretch'}
                    />
                </TouchableOpacity>
                <View
                    style={{
                        position: 'absolute',
                        width: normalize(45),
                        height: normalize(45),
                        top: normalize(8),
                        right: normalize(8),
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        borderRadius: normalize(50),
                    }}>
                    <TouchItem
                        onPress={() => {
                            setVisibleImage(true);
                        }}
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: '100%',
                            height: '100%',
                            borderRadius: normalize(50),
                        }}>
                        <Icon
                            name={'expand'}
                            size={normalize(22)}
                            type="FontAwesome"
                            color={Colors.white}
                        />
                    </TouchItem>
                </View>
            </ImageContainer>

            <FooterContainer>
                <ActionContainer>
                    <TouchItem onPress={handleLikeClick}>
                        {disabled ? (
                            <Icon
                                name={'heart'}
                                size={normalize(20)}
                                type="FontAwesome"
                                color={Colors.primaryColor}
                            />
                        ) : (
                            <Icon name={'heart-o'} size={normalize(20)} type="FontAwesome" />
                        )}
                    </TouchItem>
                    <TouchItem onPress={() => refRBSheet.current?.open()}>
                        <Icon name={'comment-o'} size={normalize(20)} type="FontAwesome" />
                    </TouchItem>
                    <TouchItem onPress={() => getBase64Data()}>
                        <Icon name={'share'} size={normalize(20)} type="FontAwesome" />
                    </TouchItem>
                </ActionContainer>
                <Icon
                    name={bookMarked ? 'bookmark' : 'bookmark-o'}
                    onPress={() => {
                        handleBookmark();
                    }}
                    size={normalize(22)}
                    type="FontAwesome"
                    color={bookMarked ? Colors.primaryColor : Colors.black}
                />
            </FooterContainer>

            {visible && (
                <AnimatedIcon
                    name="heart"
                    size={normalize(50)}
                    color={Colors.primaryColor}
                    currentValue={currentValue}
                />
            )}
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                animationType={'slide'}
                customStyles={{
                    wrapper: {
                        backgroundColor: 'transparent',
                    },
                    draggableIcon: {
                        backgroundColor: Colors.black,
                    },
                    container: {
                        borderTopRightRadius: normalize(20),
                        borderTopLeftRadius: normalize(20),
                        borderColor: '#d6d6d6',
                        borderWidth: 1,
                        height: '90%',
                    },
                }}>
                <AddComment />
            </RBSheet>
            {moreS && (
                <MoreContainer>
                    <TouchItem
                        onPress={() => {
                            setMoreS(false);
                        }}
                        style={{ padding: normalize(10) }}>
                        <Row>
                            <Icon
                                name={userMeme ? 'delete' : 'report'}
                                type="MaterialIcons"
                                size={normalize(16)}
                                color={Colors.black}
                                style={{ opacity: 0.7 }}
                            />
                            <Text
                                size={textSizes.h11}
                                fontWeight="500"
                                style={{ marginLeft: normalize(5), opacity: 0.7 }}>
                                {userMeme ? 'Delete' : 'Report'}
                            </Text>
                        </Row>
                    </TouchItem>
                    <UpArrow>
                        <Icon
                            type="AntDesign"
                            name="caretup"
                            size={normalize(30)}
                            color={Colors.lightGray}
                        />
                    </UpArrow>
                </MoreContainer>
            )}
            <ImageView
                images={images}
                imageIndex={0}
                visible={visibleImage}
                onRequestClose={() => setVisibleImage(false)}
            />
        </Card>
    );
};

const Card = styled(Container)`
  flex: 0;
  width: 100%;
  background-color: ${Colors.white};
  margin-bottom: ${normalize(8)}px;
  margin-top: ${normalize(10)}px;
  padding: 0;
`;

const Row = styled.View`
  flex-direction: row;
`;

const HeaderContainer = styled(Row)`
  width: 100%;
  height: ${normalize(45)}px;
  align-items: center;
  justify-content: space-between;
  padding-left: ${normalize(9)}px;
  padding-right: ${normalize(10)}px;
`;

const ImageContainer = styled.View`
  width: 100%;
  border-top-color: ${Colors.lightGray};
  border-top-width: 2px;
`;

const FooterContainer = styled(Row)`
  width: 100%;
  height: ${normalize(45)}px;
  align-items: center;
  justify-content: space-between;
  padding-left: ${normalize(10)}px;
  padding-right: ${normalize(14)}px;
`;

const ActionContainer = styled(Row)`
  align-items: center;
  width: ${normalize(95)}px;
  justify-content: space-between;
`;

const MoreContainer = styled(Row)`
  position: absolute;
  top: ${normalize(38)}px;
  background-color: ${Colors.lightGray};
  right: ${normalize(4)}px;
  align-items: center;
  justify-content: center;
  border-radius: ${normalize(4)}px;
  padding: ${normalize(18)}px;
  padding-top: ${normalize(5)}px;
  padding-bottom: ${normalize(5)}px;
`;

const UpArrow = styled.View`
  position: absolute;
  top: -${normalize(14)}px;
  right: ${normalize(0)}px;
`;

const styles = StyleSheet.create({
    authorImage: {
        width: normalize(35),
        height: normalize(35),
        marginRight: normalize(5),
        borderColor: Colors.primaryColor,
        borderWidth: 2,
        borderRadius: normalize(100),
    },
});
