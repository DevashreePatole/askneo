import React, { FC, useEffect, useRef, useState } from 'react';

import { FlatList, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { PermissionsAndroid, Platform } from 'react-native';
import CameraRoll, {
  GetPhotosParams,
  PhotoIdentifier,
} from '@react-native-community/cameraroll';

import { Container, Text, TouchItem } from '../../../components';
import AlertModal from '../../../components/AlertModal';
import { RootStackParamList } from '../../navigators/stack/drawerStack';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'SelectImage'>;
}

interface PageInfo {
  has_next_page: boolean;
  start_cursor?: string | undefined;
  end_cursor?: string | undefined;
}

export const SelectImage: FC<Props> = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<PhotoIdentifier[] | {}>([{}, {}, {}]);
  const [pageInfo, setPageInfo] = useState<PageInfo>();
  const [selectedImage, setSelectedImage] = useState<PhotoIdentifier>();
  const scrollRef = useRef<FlatList>(null);
  let alertRef = useRef();

  const getPhotos = () => {
    const PhotoObject: GetPhotosParams = {
      first: 50,
      assetType: 'Photos',
    };
    if (pageInfo?.end_cursor) {
      PhotoObject.after = pageInfo?.end_cursor;
    }

    CameraRoll.getPhotos(PhotoObject)
      .then((res) => {
        console.log('res',res)
        setPageInfo(res.page_info);
        setData((prevState) => {
          return [...prevState, ...res.edges];
        });

        navigation.setParams({ imageUri: res.edges[0].node?.image.uri });
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const askPermission = async () => {
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'Permission Explanation',
          message: 'AskNEO would like to access your photos!',
          buttonPositive: 'OK',
        },
      );
      if (result !== 'granted') {
        console.log('Access to pictures was denied');
        const warningModal = () =>
          alertRef?.current?.setOptionAndOpen(
            'error',
            'Please Provide Camera Permission to Continue',
            () => {
              navigation.goBack()}
          );
        warningModal();
        setLoading(false);

        return;
      } else {
        getPhotos();
      }
    } else {
      getPhotos();
    }
  };

  useEffect(() => {
    askPermission();
  }, []);

  const HeaderComponent = () => {
    return (
      <View>
        {selectedImage?.node || data[3]?.node ? (
          <ImageComponent
            style={{ width: '100%', height: normalize(280) }}
            source={{
              uri: selectedImage?.node?.image?.uri || data[3]?.node?.image.uri,
            }}
            resizeMode={'cover'}
          />
        ) : null}
      </View>
    );
  };

  const handleSelect = (item) => {
    setSelectedImage(item);
    navigation.setParams({ imageUri: item.node?.image.uri });

    scrollRef.current?.scrollToOffset({
      animated: true,
      offset: 0,
    });
  };

  const handleEndReached = () => {
    if (pageInfo?.has_next_page) {
      getPhotos();
    }
  };

  return (
    <ScreenContainer loading={loading}>
      <View style={{ width: '99%' }}>
        <FlatList
          ref={scrollRef}
          keyboardShouldPersistTaps={'always'}
          data={data}
          numColumns={3}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={HeaderComponent}
          stickyHeaderIndices={[1]}
          renderItem={({ item, index }) => {
            if (index == 0 || index == 1 || index == 2) {
              return (
                <StickyHeader>
                  <Text
                    size={textSizes.h8}
                    fontWeight="500"
                    style={{ color: Colors.white, marginRight: normalize(2) }}>
                    Gallary
                  </Text>
                  <Icon
                    type="Feather"
                    name="chevron-down"
                    size={normalize(18)}
                    color={'white'}
                  />
                </StickyHeader>
              );
            }

            return (
              <ImageContainer>
                <TouchItem
                  onPress={() => {
                    handleSelect(item);
                  }}>
                  <ImageComponent
                    style={{
                      width: '100%',
                      height: normalize(135),
                    }}
                    source={{ uri: item.node.image.uri }}
                    resizeMode={'cover'}
                  />
                </TouchItem>
              </ImageContainer>
            );
          }}
          onEndReached={() => handleEndReached()}
          onEndReachedThreshold={0.3}
        />
      </View>
      <AlertModal ref={alertRef} />
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  align-items: center;
  padding: 0px;
`;

const ImageContainer = styled.View`
  width: 33%;
  background-color: ${Colors.lightGray};
  border-right-width: ${normalize(2)}px;
  border-right-color: ${Colors.black};
  border-bottom-color: ${Colors.black};
  border-bottom-width: ${normalize(2)}px;
`;

const StickyHeader = styled.View`
  width: 100%;
  height: ${normalize(50)}px;
  background-color: ${Colors.black};
  flex-direction: row;
  align-items: center;
  padding-left: ${normalize(20)}px;
`;
