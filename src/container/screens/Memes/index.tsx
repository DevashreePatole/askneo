import React, { FC, useEffect, useState } from 'react';

import { FlatList } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { AddItem, Container } from '../../../components';
import { RootStackParamList } from '../../navigators/stack/drawerStack';
import { DisplayCard } from './displayCard';
import { normalize } from '../../../lib/globals';

interface Props {
  navigation: StackNavigationProp<RootStackParamList, 'Memes'>;
}

interface MemeProp {
  author: string;
  caption: string;
  authorImage: string;
  memeImage: string;
}

export const Memes: FC<Props> = ({ navigation }) => {
  const [loading, setLoading] = useState(true);
  const [finalData, setFinalData] = useState<MemeProp[]>([]);

  const _renderMemes = ({ item, index }) => {
    return <DisplayCard key={index} meme={item} />;
  };

  const [memes, setMemes] = useState<MemeProp[]>([
    {
      author: 'Akashsingh Lodh',
      caption: 'States of Programmer...😂',
      authorImage:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      memeImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpXFL1tYUxzM-0lw7uQll2axfPMFqShg4UVw&usqp=CAU',
    },
    {
      author: 'Diksha Choudhary',
      caption: 'I am Programmer.... why? 😭',
      authorImage:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      memeImage:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRiLuQtJnvjzqIwX0SXEMVOPX_W8Eo4mrq-GA&usqp=CAU',
    },
    {
      author: 'Devashree Patole',
      caption: 'Why Brain... Why???😰🥱😴',
      authorImage:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      memeImage:
        'https://www.thecoderpedia.com/wp-content/uploads/2020/06/Programming-Memes-Programmer-while-sleeping.jpg?x78269',
    },
    {
      author: 'Nilesh Chavan',
      caption: 'Utha le re Baba... 🤢 (Mere ko nahi, in charo ko uthale )😈',
      authorImage:
        'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
      memeImage: 'https://i.redd.it/0snws7y219b11.png',
    },
  ]);

  const getData = async () => {
    let FinalData = [...memes];
    try {
      const value = await AsyncStorage.getItem('AskNEO_MEME_IMAGE');
      if (value !== null) {
        const data = JSON.parse(value);
        // console.log('Data ', data);
        const allData: any[] = [];
        data.map((res) => {
          allData.push(res);
        });
        FinalData = [...allData, ...memes];
      }
      setFinalData(FinalData);

      setLoading(false);
    } catch (e) {
      // console.log('Get Data Error', e);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <ScreenContainer withKeyboard={false} loading={loading}>
      <FlatList
        data={finalData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={_renderMemes}
        showsVerticalScrollIndicator={false}
      />
      <AddItem
        navigation={navigation}
        styles={{ bottom: normalize(12), right: normalize(12) }}
        onPress={() => {
          navigation.navigate('SelectImage');
        }}
      />
    </ScreenContainer>
  );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  padding: 0;
`;
