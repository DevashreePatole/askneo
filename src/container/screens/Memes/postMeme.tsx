import React, { FC, useEffect, useRef, useState } from 'react';

import { Image, ScrollView, View } from 'react-native';
import styled from 'styled-components/native';
import { StackNavigationProp } from '@react-navigation/stack';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RouteProp } from '@react-navigation/core';

import AlertModal from '../../../components/AlertModal';
import { Container, Input, TouchItem } from '../../../components';
import { RootStackParamList } from '../../navigators/stack/drawerStack';
import Button from '../../../components/Button';
import { Colors } from '../../../assets/styles/color';
import { normalize, textSizes, WINDOW } from '../../../lib/globals';
import { Icon } from '../../../components/Icon';
import { ImageComponent } from '../../../components/Image';


interface Props {
    navigation: StackNavigationProp<RootStackParamList, 'PostMeme'>;
    route: RouteProp<RootStackParamList,'PostMeme'>;
}

export const PostMeme: FC<Props> = ({ navigation, route }) => {
    const [loading, setLoading] = useState(false);
    const [imageUri, setImageUri] = useState(route.params.imageUri);
    const [cropedData, setCropedData] = useState(route.params.imageUri);
    const [caption, setCaption] = useState('');
    const [memeImages, setMemeImages] = useState([]);
    const [width, setWidth] = useState(0);
    const [height, setHeight] = useState(0);

    let alertRef = useRef();

    console.log(route);

    const getData = async () => {
        try {
            const value = await AsyncStorage.getItem('AskNEO_MEME_IMAGE');
            if (value !== null) {
                const data = JSON.parse(value);
                setMemeImages(data);
            }
        } catch (e) {
            console.log('Get Data Error', e);
        }
    };

    const storeData = async (value) => {
        try {
            const memeData = {
                caption: caption,
                memeImage: value,
                author: 'Vartika Verma',
                authorImage:
                    'https://www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png',
            };
            let data = [memeData, ...memeImages];
            let storageData = JSON.stringify(data);
            await AsyncStorage.setItem('AskNEO_MEME_IMAGE', storageData);
            setLoading(true);
            setTimeout(() => {
                setLoading(false);
                const warningModal = () =>
                    alertRef?.current?.setOptionAndOpen(
                        'success',
                        'Meme Posted Successfully',
                        () => navigation.popToTop(),
                    );
                warningModal();
            }, 1000);
        } catch (e) {
            console.log('Set Data Error', e);
        }
    };

    const handleClick = () => {
        ImagePicker.openCropper({
            path: imageUri,
            width: 300,
            height: 400,
            mediaType: 'photo',
        })
            .then((image) => {
                setCropedData(image.path);
            })
            .catch((e) => {
                console.log('Error ', e);
            });
    };

    useEffect(() => {
        getData();

        Image.getSize(
            cropedData,
            (srcWidth, srcHeight) => {
                const maxHeight = WINDOW.height;
                const maxWidth = WINDOW.width;

                const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
                setWidth(srcWidth * ratio);
                setHeight(srcHeight * ratio);
            },
            (error) => {
                console.log('error:', error);
            },
        );
    }, [cropedData]);

    return (
        <ScreenContainer loading={loading} withKeyboard={false}>
            <View style={{ width: '100%' }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Input
                        value={caption}
                        label={'Enter a caption for your meme:'}
                        placeholder="type here.."
                        onChangeText={(val) => {
                            setCaption(val);
                        }}
                    />
                    <ImageContainer>
                        <ImageComponent
                            source={{ uri: cropedData || imageUri }}
                            style={{ width: width, height: height }}
                        />
                        <IconContainer>
                            <IconView onPress={() => handleClick()}>
                                <Icon
                                    type="MaterialCommunityIcons"
                                    name="image-edit"
                                    size={normalize(22)}
                                />
                            </IconView>
                        </IconContainer>
                    </ImageContainer>

                    <Button
                        title="Post Meme"
                        onClick={() => {
                            storeData(cropedData);
                        }}
                        style={{
                            width: normalize(190),
                            height: normalize(38),
                            marginBottom: normalize(20),
                        }}
                        fontSize={textSizes.h9}
                    />
                </ScrollView>
            </View>

            <AlertModal ref={alertRef} />
        </ScreenContainer>
    );
};

const ScreenContainer = styled(Container)`
  flex: 1;
  align-items: center;
`;

const ImageContainer = styled.View`
  width: 100%;
  margin-top: ${normalize(38)}px;
  margin-bottom: ${normalize(38)}px;
`;

const IconContainer = styled.View`
  width: ${normalize(48)}px;
  height: ${normalize(48)}px;
  position: absolute;
  right: ${normalize(2)}px;
  top: ${normalize(2)}px;
  background-color: ${Colors.lightGray};
  border-radius: ${normalize(48)}px; ;
`;

const IconView = styled(TouchItem)`
  width: ${normalize(48)}px;
  height: ${normalize(48)}px;
  border-radius: ${normalize(48)}px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
